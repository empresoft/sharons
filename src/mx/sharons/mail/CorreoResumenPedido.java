package mx.sharons.mail;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import mx.sharons.beans.CompraPastelArmado;


public class CorreoResumenPedido{
	CompraPastelArmado pastel;

	
	public CorreoResumenPedido(CompraPastelArmado pastelArmado) {
		this.pastel = pastelArmado;
	}
	
	public String getSubject(){
		StringBuffer body = new StringBuffer();	
		body.append("Resumén del pedido No: " + pastel.getIdCompraPastelArmado());
		return body.toString();
	}
	
	public String getBody(){
		StringBuffer body = new StringBuffer();		
		DecimalFormat formatter = new DecimalFormat("#,###");
		
		body.append("<html lang=\"es\">");
		body.append("<head>");
		body.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
		body.append("<style type=\"text/css\">");
		body.append("p {font-family: Verdana, Verdana, Geneva, sans-serif;font-size: 1em;color: #666666;text-align: justify;}");
		body.append("</style>");		
		body.append("</head>");  
		body.append("<body>");
		body.append("<h2> Resumén del pedido No: " + pastel.getIdCompraPastelArmado() + "</h2>");
		body.append("<h3>DESCRIPCIÓN: </h3>");
		body.append("<p>Forma de pastel: <b>" +  pastel.getDescForma() + "</b></p>");
		body.append("<p>Tamaño: <b>" + pastel.getNumPersonas() + "</b></p>");
		body.append("<p>Pisos: <b>" + pastel.getDescPisos() + "</b></p>");
		body.append("<p>Sabor: <b>" + pastel.getDescSabor() + "</b></p>");
		body.append("<p>Relleno: <b>" + pastel.getDescRelleno() + "</b></p>");
		body.append("<p>Cobertura: <b>" + pastel.getDescCobertura() + "</b></p>");
		
		List<String> coloresCobertura = pastel.getColoresCobertura();
		
    	for ( int i = 0; i < coloresCobertura.size(); i++ ) {
    		int colordePiso = i + 1;
    		body.append("<p>Color de Piso " + colordePiso + ": <b>" + coloresCobertura.get(i) + "</b></p>"); 	
    	}    
		body.append("<p>Mensaje: <b>" + pastel.getMensaje() + "</b></p>");

		body.append("<h3>ENTREGA: </h3>");
		body.append("<p>Sucursal: <b>" +  pastel.getDescSucursalEntrega() + "</b></p>");
		body.append("<p>Día: <b>" + pastel.getFechaEntrega() + "</b></p>");
		body.append("<p>Hora: <b>" + pastel.getHorario() + "</b></p>");
		body.append("<p>Observaciones: <b>" + pastel.getObservaciones() + "</b></p>");
		body.append("<p>Correo Cliente: <b>" + pastel.getCorreoCliente() + "</b></p>");
		
		body.append("<h3>EXTRAS: </h3>");
		body.append("<p><b>" + pastel.getDescExtras() + "</b></p>");
		
		int precio = pastel.getPrecioPastel();
		String precioStr = formatter.format(precio);
		
		double precioExtras = pastel.getPrecioExtras();

		double precioTotal = precio + precioExtras;
		String precioTotalStr =  formatter.format(precioTotal);
		body.append("---------------------------------------------");
		body.append("<p>PRECIO: $" + precioStr + "</p>");
		body.append("---------------------------------------------");
		body.append("<p>EXTRAS:  $" + precioExtras + "</p>");
		body.append("---------------------------------------------");
		body.append("<p>TOTAL: <b> $" + precioTotalStr + "</b></p>");
		
		body.append("</body>");
		body.append("</html>");
		
		
		return body.toString();
	}
	
	public static void main(String args[]) {
		CompraPastelArmado pastel = new CompraPastelArmado();
		pastel.setIdCompraPastelArmado(90);
		pastel.setDescForma("Cuadrada");
		pastel.setNumPersonas(10);
		pastel.setDescPisos("2");
		pastel.setDescSabor("Vainilla");
		pastel.setDescRelleno("Cremosito");
		pastel.setDescCobertura("Puntos");
		
		List<String> coloresCobertura = new ArrayList<String>();
		coloresCobertura.add("Blanco");
		
		pastel.setColoresCobertura(coloresCobertura);
		pastel.setMensaje("Test");
		
		pastel.setDescSucursalEntrega("Sucursal Colosio");
		pastel.setFechaEntrega("13/06/2022");
		pastel.setHorario("11:30 am");		
		pastel.setDescExtras("Flores Fundant, Flores");
		
		pastel.setPrecioPastel(1430);
		pastel.setPrecioExtras(0);
		
		CorreoResumenPedido correo =  new CorreoResumenPedido(pastel);
		
		System.out.println(correo.getBody());
	}
}
