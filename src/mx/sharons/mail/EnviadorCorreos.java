package mx.sharons.mail;


import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

 
public class EnviadorCorreos {
	private static final String MAIL_SMTP_PORT = "mail.smtp.port";
	private static final String PUERTO = "587";		
	
	private static final String MAIL_SMTP_HOST = "smtp.gmail.com";
	private static String CUENTA_CORREO_ORIGEN = "sharons.pasteleria.sistema@gmail.com";
	private static String USERNAME = "sharons.pasteleria.sistema@gmail.com";
	private static String PASSWORD = "vptewvpfwwfvcanm";	

	
	public static void main(String arg[]){
//		enviaCorreo("","","","","","");
		String[] sCorreoDestino = new String[3];
		
		List<String> correosDestino = new ArrayList<String>();
		correosDestino.add("ventas@sharons.mx");
		correosDestino.add("paty1400@gmail.com");
		
		EnviadorCorreos.enviarEmail("Prueba envió Resumen Pedido T1","Prueba desde cuenta origen: sharons.pasteleria.sistema@gmail.com", correosDestino);
	}
	

	public static boolean enviarEmail( String asunto, String bodyhtml, List<String> correosDestino )
	{
	    try
	    {
	        Properties props = new Properties();
	        Authenticator auth = new javax.mail.Authenticator() {
	        	public PasswordAuthentication getPasswordAuthentication() {
	        		return new PasswordAuthentication(USERNAME, PASSWORD);
	        	}
	        };
	        props.put("mail.host", MAIL_SMTP_HOST );
	        props.setProperty(MAIL_SMTP_PORT,PUERTO);
	        // descomentar la siguiente linea para usarse en modo debug
	        //props.put("mail.debug", "true");
	       
	        props.put("mail.smtp.auth", "true");
	        
	        props.setProperty("mail.smtp.starttls.enable", "true");
	        Session mailSesion = Session.getDefaultInstance(props, auth);
	        
	
	        Message msg = new MimeMessage(mailSesion);
	   
	        msg.setFrom ( new InternetAddress( CUENTA_CORREO_ORIGEN ) );
	        msg.setSubject ( asunto );
	        msg.setSentDate ( new java.util.Date() );
	        msg.setContent(bodyhtml,"text/html");
	        
	        InternetAddress address[] = new InternetAddress[correosDestino.size()];
	        
	        for (int i = 0; i<correosDestino.size(); i++) {
	            address[i] = new InternetAddress ( correosDestino.get(i) );
	            System.out.println(address[i]);
	        }
	             
	           
	        msg.setRecipients (Message.RecipientType.TO, address);
	                           
	        System.out.println ("Enviando correo, asunto: " + asunto );
	        Transport.send(msg);
	        System.out.println ("Mensaje enviado!");
	      }
	      catch( MessagingException e )
	      {
	    	  e.printStackTrace();
	          return false ;
	      }
	   
	      return true ;
	}
	
	public static boolean enviarEmail( String asunto, String bodyhtml, String correo)
	{
	    try
	    {
	        Properties props = new Properties();
	        Authenticator auth = new javax.mail.Authenticator() {
	        	public PasswordAuthentication getPasswordAuthentication() {
	        		return new PasswordAuthentication(USERNAME, PASSWORD);
	        	}
	        };
	        props.put("mail.host", MAIL_SMTP_HOST );
	        props.setProperty(MAIL_SMTP_PORT,PUERTO);
	        // descomentar la siguiente linea para usarse en modo debug
//	        props.put("mail.debug", "true");
	        props.put("mail.smtp.auth", "true");
	        props.setProperty("mail.smtp.starttls.enable", "true");
	        Session mailSesion = Session.getDefaultInstance(props, auth);
	        
	
	        Message msg = new MimeMessage(mailSesion);
	   
	        msg.setFrom ( new InternetAddress( CUENTA_CORREO_ORIGEN ) );
	        msg.setSubject ( asunto );
	        msg.setSentDate ( new java.util.Date() );
	        msg.setContent(bodyhtml,"text/html");
	        
	        InternetAddress address[] = new InternetAddress[1];
	        address[0] = new InternetAddress ( correo );
	           
	        msg.setRecipients (Message.RecipientType.TO, address);
	                           
	        System.out.println ("Enviando correo, asunto: " + asunto );
	        Transport.send(msg);
	        System.out.println ("Mensaje enviado!");
	      }
	      catch( MessagingException e )
	      {
	    	  e.printStackTrace();
	          return false ;
	      }
	   
	      return true ;
	}
}
