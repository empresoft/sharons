package mx.sharons.mail;

import mx.sharons.beans.Usuario;

public class CorreoUsuarioRegistro {
	private static final String SUBJECT = "Bienvenido a Sharons compra en l�nea.";
	Usuario usuarioBean;
	
	public CorreoUsuarioRegistro(){
	}
	
	public CorreoUsuarioRegistro(Usuario usuarioBean){
		this.usuarioBean = usuarioBean;
	}
	
	public boolean enviarCorreo() {
		return EnviadorCorreos.enviarEmail(SUBJECT, getCorreoHtml(usuarioBean), usuarioBean.getCorreo());
	}

	private String getCorreoHtml(Usuario usuarioBean){
		StringBuffer bodyHtml = new StringBuffer();
		
		StringBuilder cuerpo = new StringBuilder();
		cuerpo.append("<html lang=\"es\">");
		cuerpo.append("<head>");
		cuerpo.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">");
		cuerpo.append("<style type=\"text/css\">");
		cuerpo.append("p {font-family: Verdana, Verdana, Geneva, sans-serif;font-size: 1em;color: #666666;text-align: justify;}");
		cuerpo.append("</style>");		
		cuerpo.append("</head>");  
		cuerpo.append("<body>");
		cuerpo.append("<p><b>Hola,  ");
		cuerpo.append( usuarioBean.getNombre() );
		cuerpo.append(":</b></p>");
		cuerpo.append("<p> Te has registrado en el portal de venta en l�nea de Sharons Pasteler�a.</p>");
        cuerpo.append("<p> Tu usuario es: </p>");
        cuerpo.append( usuarioBean.getCorreo() );
        cuerpo.append("</p><p> Tu contrase�a es: ");
        cuerpo.append( usuarioBean.getPassword() );
        cuerpo.append("</p>Visita y compra pasteles en l�nea en: ");
        cuerpo.append("<br><br>");
        cuerpo.append("<p>Visita y compra pasteles en l�nea en: ");
        cuerpo.append("<a href=\"http://www.sharons.mx\"> http://www.sharons.mx </a></p>");        
        cuerpo.append("<br><br>");
		cuerpo.append("<p>Gracias,</p>");
		cuerpo.append("<p>Sharons ventas.</p>");
		cuerpo.append("<br><br>");
		cuerpo.append("<div style=\"font-size:12px;\">");
		cuerpo.append("<p>Cualquier duda o comentario por favor envia un correo a: ");
		cuerpo.append("<a href=\"mailto:contacto@sharons.mx\">contacto@sharons.mx</a>");
		cuerpo.append(" o comun�cate al tel&eacute;fono: 01 (81) 2164 7240.");
		cuerpo.append("<p>Plataforma desarrollada por <a href=\"http://www.empresoft.com.mx\">empresoft</a>. </p>");
		cuerpo.append("</div>");
		cuerpo.append("</body>");
		cuerpo.append("</html>");
		return bodyHtml.toString();
	}	
}
