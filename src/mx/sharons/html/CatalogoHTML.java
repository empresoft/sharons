package mx.sharons.html;

import java.util.ArrayList;

import mx.sharons.beans.Catalog;
import mx.sharons.dao.CatalogDao;

public class CatalogoHTML implements CustomTag{
	private String tableName;
	private String id;
	private String cssClass;
	private String html;
	private boolean isRequired;
	private boolean hasPrecio;
	private ArrayList<Catalog> options;
	private final String CATALOG_SEPARATOR = "|";
	
	CatalogoHTML(){
	}
	
	public CatalogoHTML(String tableName, String id, String cssClass, boolean isRequired, boolean hasPrecio){
		this.tableName = tableName;
		this.id = id;
		this.cssClass = cssClass;
		this.isRequired = isRequired;
		this.hasPrecio = hasPrecio;
	}
	
	@Override
	public void build(){
		this.populateOptions();
		StringBuilder line = new StringBuilder();
		
		// Header
		line.append("<select id=\"");
		line.append(this.id);
		line.append("\" class=\"");
		line.append(this.cssClass);
		line.append("\"");
		
		if (this.isRequired) {
			line.append(" required");			
		}
		line.append(">");
		
		// add first option by default:
		line.append("<option value=\"0\">");
		line.append("Selecciona ");
		line.append(tableName);
		line.append("</option>");

		// body
		for (Catalog catalog: options){
			line.append("<option value=\"");
			line.append(catalog.getPrecio());
			line.append("\">");
			line.append(catalog.getDescription());
			line.append("</option>");
		}
		// end tag
		line.append("</select>");
		this.html = line.toString();
	}
	
	private void populateOptions(){
		CatalogDao catalog = new CatalogDao();
		options = catalog.getCatalog(this.tableName, this.hasPrecio);
	}
	
	public String getTag(){
		this.build();
		return html;
	}
}
