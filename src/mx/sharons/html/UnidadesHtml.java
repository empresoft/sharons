package mx.sharons.html;

import java.util.ArrayList;

import mx.sharons.beans.Unidad;
import mx.sharons.dao.UnidadesDao;

public class UnidadesHtml {
	
	public static String getSelectUnidades(){
		UnidadesDao unidadesDao = new UnidadesDao();
		StringBuilder html = new StringBuilder();
		
		ArrayList<Unidad> listaUnidad = (ArrayList<Unidad>) unidadesDao.buscarTodos();
		
		html.append("<select id=\"idUnidad\" class=\"select_sharons\" >");
		html.append("<option value=\"0\">Selecciona una unidad</option>");
		for(Unidad unidad: listaUnidad){
			html.append("<option value=\"");
			html.append(unidad.getIdUnidad());
			html.append("\">");
			html.append(unidad.getDescripcion());
			html.append("</option>");
		}
		html.append("</select>");		
		
		return html.toString();
	}
}
