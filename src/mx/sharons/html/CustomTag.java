package mx.sharons.html;

public interface CustomTag {
	public void build();
	public String getTag();
}
