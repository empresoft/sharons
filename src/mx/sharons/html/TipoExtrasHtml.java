package mx.sharons.html;

import java.util.ArrayList;

import mx.sharons.beans.Tipo;
import mx.sharons.beans.Unidad;
import mx.sharons.dao.TiposExtraDao;

public class TipoExtrasHtml {
	
	public static String getSelectTipos(){
		TiposExtraDao tiposExtraDao = new TiposExtraDao();
		StringBuilder html = new StringBuilder();
		
		ArrayList<Tipo> listaTipos = (ArrayList<Tipo>) tiposExtraDao.buscarTodos();
		
		html.append("<select id=\"idTipo\" class=\"select_sharons\" >");
		html.append("<option value=\"0\">Selecciona un tipo</option>");
		for(Tipo tipo: listaTipos){
			html.append("<option value=\"");
			html.append(tipo.getIdTipo());
			html.append("\">");
			html.append(tipo.getDescripcion());
			html.append("</option>");
		}
		html.append("</select>");		
		
		return html.toString();
	}	
}
