package mx.sharons.html;

import java.util.ArrayList;

import mx.sharons.beans.Extra;
import mx.sharons.dao.ExtraDao;

public class ExtraHtml {
	
	private static final int ADICIONALES = 3;
	private static final int FLORES_FONDANT = 1;
	private static final int FLORES_NATURALES = 2;

	public static String getListaFloresFondant(){
		ExtraDao extraDao = new ExtraDao();
		
		ArrayList<Extra> listaFloresFondant = extraDao.buscarPorIdTipo(FLORES_FONDANT);
		
		StringBuilder html = new StringBuilder();
		
		for( Extra extra : listaFloresFondant ){
			buildDescription(html, extra);
		}
		return html.toString();	
	}
	public static String getListaFloresNaturales(){
		ExtraDao extraDao = new ExtraDao();
		
		ArrayList<Extra> listaFloresFondant = extraDao.buscarPorIdTipo(FLORES_NATURALES);
		
		StringBuilder html = new StringBuilder();
		
		for( Extra extra : listaFloresFondant ){
			buildDescription(html, extra);
		}
		return html.toString();		
	}
	
	public static String getListaAdicionales(){
		ExtraDao extraDao = new ExtraDao();
		
		ArrayList<Extra> listaAdicionales = extraDao.buscarPorIdTipo(ADICIONALES);
		
		StringBuilder html = new StringBuilder();
		
		for( Extra extra : listaAdicionales ){
			buildDescription(html, extra);
		}
		return html.toString();				
	}
	
	public static String getListaFloresFondantConUnidades(){
		ExtraDao extraDao = new ExtraDao();
		
		ArrayList<Extra> listaFloresFondant = extraDao.buscarPorIdTipo(FLORES_FONDANT);
		
		StringBuilder html = new StringBuilder();
		for( Extra extra : listaFloresFondant ){
			html.append("<div class=\"mediabox\">");
			html.append("<label class=\"decorado-extra\">");
			html.append("<input name=\"decorado_flores_fondant\" value=\"Trébol fondant\" id=\"trebol_fondant\">");
			html.append("<img src=\"decorado/");
			html.append(extra.getImagen());
			html.append("\">");
			html.append("<p>");
			html.append(extra.getTitulo());
			html.append("<br><span class=\"costo_adi\">");
			html.append(extra.getUnidad().getDescripcion().replace('s', ' ') );
			html.append(" $");
			html.append(extra.getCosto());
			html.append(" pesos</span></label>");
			
			buildSelectUnidades(html, extra);			
			
			buildSelectColores(html, extra);
			
			html.append("</div>");
		}
		return html.toString();
	}
	
	
	public static String getListaFloresNaturalesConUnidades(){
		ExtraDao extraDao = new ExtraDao();
		
		ArrayList<Extra> listaFloresFondant = extraDao.buscarPorIdTipo(FLORES_NATURALES);
		
		StringBuilder html = new StringBuilder();
		for( Extra extra : listaFloresFondant ){
			html.append("<div class=\"mediabox\">");
			html.append("<label class=\"decorado-extra\">");
			html.append("<input name=\"decorado_flores_fondant\" value=\"Trébol fondant\" id=\"trebol_fondant\">");
			html.append("<img src=\"decorado/");
			html.append(extra.getImagen());
			html.append("\">");
			html.append("<p>");
			html.append(extra.getTitulo());
			html.append("<br><span class=\"costo_adi\">");
			html.append(extra.getUnidad().getDescripcion().replace('s', ' ') );
			html.append(" $");
			html.append(extra.getCosto());
			html.append(" pesos</span></label>");
			
			buildSelectUnidades(html, extra);			
			
			buildSelectColores(html, extra);
			
			html.append("</div>");
		}
		return html.toString();		
	}
	
	public static String getListaAdicionalesConUnidades(){
		ExtraDao extraDao = new ExtraDao();
		
		ArrayList<Extra> listaFloresFondant = extraDao.buscarPorIdTipo(ADICIONALES);
		
		StringBuilder html = new StringBuilder();
		for( Extra extra : listaFloresFondant ){
			html.append("<div class=\"mediabox\">");
			html.append("<label class=\"decorado-extra\">");
			html.append("<input name=\"decorado_flores_fondant\" value=\"Trébol fondant\" id=\"trebol_fondant\">");
			html.append("<img src=\"decorado/");
			html.append(extra.getImagen());
			html.append("\">");
			html.append("<p>");
			html.append(extra.getTitulo());
			html.append("<br><span class=\"costo_adi\">");
			html.append(extra.getUnidad().getDescripcion().replace('s', ' ') );
			html.append(" $");
			html.append(extra.getCosto());
			html.append(" pesos</span></label>");
			
			buildSelectUnidades(html, extra);			
			
			buildSelectColores(html, extra);
			
			html.append("</div>");
		}
		return html.toString();				
	}


	private static void buildSelectColores(StringBuilder html, Extra extra) {
		html.append("<select id=\"idColor_" + extra.getIdExtra() + "\"   class=\"decorado\" required>");
		html.append("<option value=\"\">Color</option>");
		html.append("<option value=\"Blanco\">Blanco</option>");
		html.append("<option value=\"Rojo\">Rojo</option>");
		html.append("<option value=\"Amarillo\">Amarillo</option>");
		html.append("<option value=\"Rosa\">Rosa</option>");
		html.append("<option value=\"Naranja\">Naranja</option>");
		html.append("<option value=\"Lila\">Lila</option>");
		html.append("<option value=\"Morado\">Morado</option>");
		html.append("<option value=\"Verde\">Verde</option>");
		html.append("<option value=\"Azul\">Azul</option>");
		html.append("<option value=\"Turquesa\">Turquesa</option>");
		html.append("<option value=\"Dorado\">Dorado</option>");
		html.append("<option value=\"Plateado\">Plateado</option>");
		html.append("</select>");
	}

	private static void buildSelectUnidades(StringBuilder html, Extra extra) {
		html.append("<select id=\"idExtra_" + extra.getIdExtra() + "\" class=\"decorado\" required>");
		html.append("<option value=\"0\">"); 
		html.append(extra.getUnidad().getDescripcion() );
		html.append("</option>");
		int noUnidades = extra.getNoUnidades();
		int idUnidad = extra.getUnidad().getIdUnidad();
		
		for ( int i = 1; i <= noUnidades; i ++)   {
			// agregar media decena o docena			
			if ( i == 1 && (idUnidad == 1 || idUnidad == 2)) {
				html.append("<option value=\"");
				html.append(0.5);
				html.append("\">");
				html.append(0.5);
				html.append("</option>");				
			} 
			
			if ( noUnidades == 50 ){
				i = i + 9;
			}else if( i == 11 || i == 16 ) {
				i = i + 4;
			} 
			html.append("<option value=\"");
			html.append(i);
			html.append("\">");
			html.append(i);
			html.append("</option>");
		}
		html.append("</select>");
	}
		
	private static void buildDescription(StringBuilder html, Extra extra) {
		html.append("<div class=\"mediabox\">");
		html.append("<label class=\"decorado-extra\">");
		html.append("<a onclick=\"editar(");
		html.append(extra.getIdExtra());
		html.append(")\">");
		html.append("<div class=\"img_");
		html.append(extra.getIdExtra());
		html.append("\">");
		html.append("<img src=\"decorado/");
		html.append(extra.getImagen());
		html.append("\">");
		html.append("</div>");
		
		html.append("<p><span id=\"titulo_");html.append(extra.getIdExtra());html.append("\">");
		html.append(extra.getTitulo());
		html.append("</span></p>");
		html.append("</a>");
		
		html.append("<span class=\"costo_adi\"");
		html.append(extra.getUnidad().getDescripcion().replace('s', ' ') );
		html.append("$<span id=\"costo_");html.append(extra.getIdExtra());html.append("\" class=\"costo_adi\">");
		html.append("");
		html.append(extra.getCosto());
		html.append("</span> pesos<br>");
		html.append("</span>");
		
		html.append("<span class=\"costo_adi\">Hasta</span> ");
		html.append("<span id=\"no_unidades_");html.append(extra.getIdExtra());html.append("\" class=\"costo_adi\">");
		html.append(extra.getNoUnidades() );
		html.append("</span><br>");			
		
		html.append("<span id=\"unidad_");html.append(extra.getIdExtra());html.append("\" class=\"costo_adi\">");
		html.append(extra.getUnidad().getDescripcion() );
		html.append("</span><br>");
		
		html.append("<p hidden><span id=\"idTipo_");html.append(extra.getIdExtra());html.append("\">");
		html.append(extra.getTipo().getIdTipo() );
		html.append("</span></p>");
		
		html.append("<p hidden><span id=\"idUnidad_");html.append(extra.getIdExtra());html.append("\">");
		html.append(extra.getUnidad().getIdUnidad()  );
		html.append("</span></p>");

		html.append("<span class=\"button\" onclick=\"eliminar(");
		html.append(extra.getIdExtra());
		html.append(")\" id=\"btn_guardar\">Eliminar<span class=\"icon-times\"></span></span>");
		
		html.append("</label>");
		html.append("</div>");		
	}
}
