package mx.sharons.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.sharons.beans.Catalog;
import mx.sharons.beans.Tipo;
import mx.sharons.db.FabricaDeConexiones;
import mx.sharons.db.SQLDaoImpl;

public class CatalogDao extends SQLDaoImpl{
	public ArrayList<Catalog> getCatalog(String tableName, boolean hasPrecio){
    	ArrayList<Catalog> catalogs = new ArrayList<Catalog>();
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        // Execute SQL query
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM " + tableName);
	     
        rSet = instruccion.executeQuery(sql.toString());
        
	    while ( rSet.next() ){
	    	Catalog catalog = new Catalog(rSet.getInt("id_" + tableName),
	    			rSet.getString("descripcion"));
	    	
	    	if ( hasPrecio ){
	    		catalog.setPrecio(rSet.getInt("precio"));
	    	}
	    	catalogs.add(catalog);
	    }  
	    
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
    	
		return catalogs;		
	}
}
