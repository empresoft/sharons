package mx.sharons.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import mx.sharons.beans.PastelPredisenado;
import mx.sharons.db.SQLDaoImpl;

public class PastelPredisenadoDao extends SQLDaoImpl{
	private final char ESTATUS_INACTIVO = 'I';
	private final char ESTATUS_ACTIVO = 'A';
	/**
	 * Regresa los pasteles predisenados en una lista, solo los que sean igual al tipo
	 * pasado como parametro.
	 * 1.- Bautizo, 2.- Bodas y XV años, 3.- Cumpleaños, 4.- Infantil
	 * @param idTipoPastel
	 * @return
	 */
	public ArrayList<PastelPredisenado> buscarTodosPorTipo(int idTipoPastel){
		ArrayList<PastelPredisenado> pastelesEncontrados = new ArrayList<PastelPredisenado>();
		try {
	        // Open a connection
	        instruccion = conseguirInstruccion();
			// crear query
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT * FROM pastel_predisenado WHERE id_tipo_pastel = ");
			query.append(idTipoPastel);
			query.append(" AND estatus = '");
			query.append(ESTATUS_ACTIVO);
			query.append("'");
			// System.out.println("IN " + query.toString());
			rSet = instruccion.executeQuery(query.toString());
			
			while( rSet.next() ){
				PastelPredisenado pastelPredisenado = new PastelPredisenado();
				pastelPredisenado.setIdPastelPredisenado(rSet.getInt("id_pastel_predisenado"));
				pastelPredisenado.setIdTipoPastel(idTipoPastel);
				pastelPredisenado.setNombre(rSet.getString("nombre"));
				pastelPredisenado.setDescripcion(rSet.getString("descripcion"));
				pastelPredisenado.setPrecioPorcion(rSet.getInt("precio_porcion"));
				pastelPredisenado.setSabor(rSet.getString("sabor"));
				pastelPredisenado.setImagen(rSet.getString("imagen"));
				pastelPredisenado.setObservaciones(rSet.getString("observaciones"));
				
				pastelesEncontrados.add(pastelPredisenado);
			}
		} catch(SQLException sqle){
			System.out.println("Ocurrió un error en PastelPredisenadoDao.buscarTodosPastelesPorTipo");
			sqle.printStackTrace();
		}
		return pastelesEncontrados;
	}
	
	public PastelPredisenado buscarPorId(int idPastelPredisenado ){
		PastelPredisenado pastelPredisenado = new PastelPredisenado();
		try {
	        // Open a connection
	        instruccion = conseguirInstruccion();
			// crear query
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT * FROM pastel_predisenado WHERE id_pastel_predisenado = ");
			query.append( idPastelPredisenado );
			// System.out.println(query.toString());
			rSet = instruccion.executeQuery(query.toString());
			
			while( rSet.next() ){
				pastelPredisenado.setIdPastelPredisenado(rSet.getInt("id_pastel_predisenado"));
				pastelPredisenado.setIdTipoPastel( rSet.getInt("id_tipo_pastel") );
				pastelPredisenado.setNombre(rSet.getString("nombre"));
				pastelPredisenado.setDescripcion(rSet.getString("descripcion"));
				pastelPredisenado.setPrecioPorcion(rSet.getInt("precio_porcion"));
				pastelPredisenado.setSabor(rSet.getString("sabor"));
				pastelPredisenado.setImagen(rSet.getString("imagen"));
				pastelPredisenado.setObservaciones(rSet.getString("observaciones"));
				pastelPredisenado.setAdicional(rSet.getInt("adicional"));
			}			
		} catch(SQLException sqle) {
			System.out.println("Ocurrió un error en PastelPredisenadoDao.buscarPorId");
			sqle.printStackTrace();
			
		}
		return pastelPredisenado;
	}
	
	/**
	 * Cambia de estatus a inactivo un pastel predisenado
	 * @param idPastelPredisenado
	 * @return
	 * TO-DO Use ? wild cards for safe query and avoid sql injection
	 */
	public int delete(int idPastelPredisenado) {
		int result = 0;
		try {
			instruccion = conseguirInstruccion();
			StringBuilder query = new StringBuilder();
			query.append("UPDATE pastel_predisenado SET estatus = '");
			query.append(ESTATUS_INACTIVO);
			query.append("' WHERE id_pastel_predisenado = ");
			query.append(idPastelPredisenado);
			result = instruccion.executeUpdate(query.toString());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	public int update(PastelPredisenado pastel) {
		int result = 0;
		try {
			instruccion = conseguirInstruccion();
			StringBuilder query = new StringBuilder();
			query.append("UPDATE pastel_predisenado SET nombre = '");
			query.append(pastel.getNombre());
			query.append("', descripcion = '");
			query.append(pastel.getDescripcion());
			query.append("', precio_porcion =  ");
			query.append(pastel.getPrecioPorcion());
			query.append(", adicional =  ");
			query.append(pastel.getAdicional());
			query.append(", sabor = '");
			query.append(pastel.getSabor());
			query.append("', observaciones = '");
			query.append(pastel.getObservaciones());
			query.append("', imagen = '");
			query.append(pastel.getImagen());
			query.append("' WHERE id_pastel_predisenado = ");
			query.append(pastel.getIdPastelPredisenado());
			result = instruccion.executeUpdate(query.toString());
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
