package mx.sharons.dao;

import java.sql.SQLException;
import java.sql.Statement;

import mx.sharons.beans.CompraPastelPredisenado;
import mx.sharons.db.FabricaDeConexiones;
import mx.sharons.db.SQLDaoImpl;
import mx.sharons.utils.Constantes;

public class CompraPastelPredisenadoDao extends SQLDaoImpl {
	
	public int guardar( CompraPastelPredisenado compra ){
		return this.guardar(compra, Constantes.ACTIVO);
	}
	
	public int guardar( CompraPastelPredisenado compra, String status ){
    	int idCompra = 0;
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        // Execute SQL query
        String sql = 
        		"INSERT INTO compra_pastel_predisenado"+
        		"(id_pastel_predisenado," +
        		"precio," +
        		"id_sucursal_entrega, " +
        		"fecha_entrega,"+
        		"mensaje, " +
        		"hora_entrega,"+
        		"observaciones, " +
        		"num_personas, " +
        		"estatus) " +
        		"VALUES" +
        		"("+ compra.getIdPastelPredisenado()  +
        		","+ compra.getPrecio() +
        		","+ compra.getIdSucursalEntrega() +
        		",'"+ compra.getFechaEntrega() +
        		"','"+ compra.getMensaje() +
        		"','"+ compra.getHoraEntrega() +
        		"','"+ compra.getObservaciones() +
        		"',"+ compra.getNumPersonas() +
        		",'"+ status +
	    	    "');";
	     
        System.out.println(sql);
	     idCompra = instruccion.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
	     
         if (idCompra > 0){
        	 rSet = instruccion.getGeneratedKeys();
        	 if (rSet.next()){
        		 idCompra = rSet.getInt(1);
                 compra.setIdPastelPredisenado(idCompra);
                 System.out.println("Se guardo una compra " + idCompra);
             } 
        }	     
	      
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return idCompra;
	}
}
