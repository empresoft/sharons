package mx.sharons.dao;

import java.util.List;

import mx.sharons.beans.CompraExtras;
import mx.sharons.db.SQLDaoImpl;

public class CompraExtrasDao extends SQLDaoImpl {
	// TO-DO hacer método que devuelva la suma del precio de todos los extras
	
	public double calcularPrecioExtras( List<CompraExtras> compraExtrasList ){
		ExtraDao extraDao = new ExtraDao();
		double costo = 0;
		double costoTotal = 0;
		double costoCompra = 0;
		
		for(CompraExtras compra: compraExtrasList){
			costo = extraDao.getCostoById(compra.getIdExtra());
			costoCompra = costo * compra.getCantidad();
			costoTotal = costoTotal + costoCompra;
		}
		
		return costoTotal;
	}
	// TO-DO hacer método que devuelva una descripción de los artículos
	/*// se movio por que esta construyendo código html, no pertenece a la capa de dao
	public String getDescripcionExtras( List<CompraExtras> compraExtrasList ){
		StringBuffer descripcion = new StringBuffer();
		ExtraDao extraDao = new ExtraDao();
		String descExtra = "";
		
		for( CompraExtras compra: compraExtrasList ){
			descExtra = extraDao.getDescpripcionById(compra.getIdExtra());
			
			descripcion.append(compra.getCantidad());
			descripcion.append(" ");
			descripcion.append(compra.getDescUnidad());
			descripcion.append(" ");
			descripcion.append(descExtra);
			descripcion.append(". ");
			
		}
		
		return descripcion.toString();
	}*/
}
