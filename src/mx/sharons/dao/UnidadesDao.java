package mx.sharons.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.sharons.beans.Unidad;
import mx.sharons.db.FabricaDeConexiones;
import mx.sharons.db.SQLDaoImpl;

public class UnidadesDao extends SQLDaoImpl{
	
	public List<Unidad> buscarTodos(){
    	ArrayList<Unidad> listaUnidad = new ArrayList<Unidad>();
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        // Execute SQL query
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM unidad");
	     
        rSet = instruccion.executeQuery(sql.toString());
        
	    while ( rSet.next() ){
	    	Unidad unidad = new Unidad();
	    	
	    	unidad.setIdUnidad(rSet.getInt("id_unidad"));
	    	unidad.setDescripcion(rSet.getString("descripcion"));
	    	
	    	listaUnidad.add(unidad);
	    }  
	    
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return listaUnidad;		
	}
}
