package mx.sharons.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mx.sharons.beans.Tipo;
import mx.sharons.db.FabricaDeConexiones;
import mx.sharons.db.SQLDaoImpl;

public class TiposExtraDao extends SQLDaoImpl{
	public List<Tipo> buscarTodos(){
    	ArrayList<Tipo> listaTipoExtra = new ArrayList<Tipo>();
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        // Execute SQL query
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM tipo_extra");
	     
        rSet = instruccion.executeQuery(sql.toString());
        
	    while ( rSet.next() ){
	    	Tipo TipoExtra = new Tipo();
	    	
	    	TipoExtra.setIdTipo(rSet.getInt("id_tipo"));
	    	TipoExtra.setDescripcion(rSet.getString("descripcion"));
	    	
	    	listaTipoExtra.add(TipoExtra);
	    }  
	    
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return listaTipoExtra;		
	}
}
