package mx.sharons.dao;

import java.sql.SQLException;

import mx.sharons.db.SQLDaoImpl;

public class TipoPastelDao extends SQLDaoImpl {
	
	public String getDescPorIdTipo( int idTipoPastel ){
		String descripcion = "";
		try {
	        // Open a connection
	        instruccion = conseguirInstruccion();
			// crear query
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT * FROM tipo_pastel WHERE id_tipo_pastel = ");
			query.append( idTipoPastel );
			// System.out.println(query.toString());
			rSet = instruccion.executeQuery(query.toString());
			
			if ( rSet.next() ){
				descripcion = rSet.getString("descripcion");
			}			
		} catch(SQLException sqle) {
			System.out.println("Ocurrió un error en TipoPastelDao.getDescPorIdTipo");
			sqle.printStackTrace();
			
		}
		return descripcion;
	}
}
