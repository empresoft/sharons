package mx.sharons.dao;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mx.sharons.beans.CompraPastelArmado;
import mx.sharons.db.FabricaDeConexiones;
import mx.sharons.db.SQLDaoImpl;
import mx.sharons.utils.Constantes;

public class CompraPastelArmadoDao extends SQLDaoImpl {
	public int guardar(CompraPastelArmado pastelBean){
    	int idCompra = 0;
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        // Execute SQL query
        String sql = 
        		"INSERT INTO compra_pastel_armado"+
        		"(id_tipo_pastel," +
        		"id_forma," +
        		"num_personas," +
        		"id_piso, " +
        		"id_sabor,"+
        		"id_relleno, " +
        		"id_cobertura,"+
        		"id_textura, " +
        		"mensaje, " +
        		"observaciones, " +
        		"id_sucursal_entrega, " +
        		"fecha_entrega, " +
        		"hora_entrega, " +
        		"precio_extras," +
        		"precio_pastel," +
        		"desc_extras,"+
        		"correo_cliente,"+        		
        		"estatus) " +
        		"VALUES" +
        		"(" + pastelBean.getIdTipoPastel() +
        		"," + pastelBean.getIdForma() +
        		"," + pastelBean.getNumPersonas()+
        		"," + pastelBean.getIdPiso() +
        		"," + pastelBean.getIdSabor() +
        		"," + pastelBean.getIdRelleno() +
        		"," + pastelBean.getIdCobertura() +
        		"," + pastelBean.getIdTextura() +
        		",'" + pastelBean.getMensaje() +
        		"','" + pastelBean.getObservaciones() +
        		"'," + pastelBean.getIdSucursalEntrega() +
        		",'" + pastelBean.getFechaEntrega() +
        		"','" + pastelBean.getHorario() +
        		"'," + pastelBean.getPrecioExtras() +
        		"," + pastelBean.getPrecioPastel() +
        		",'" + pastelBean.getDescExtras() + 
        		"','" + pastelBean.getCorreoCliente() +         		
        		"','" + Constantes.PENDIENTE +
	    	    "');";
        
	     //System.out.println(sql);
	     int resultInsert = instruccion.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
	     //System.out.println("resultInsert " + resultInsert);
	     
         if ( resultInsert > 0){
        	 rSet = instruccion.getGeneratedKeys();
        	 if ( rSet.next() ){
        		 idCompra = rSet.getInt(1);
                 //System.out.println("Id Compra: " + idCompra);
                 pastelBean.setIdCompraPastelArmado(idCompra);
                 int resultGuardadoColores = guardarColorCoberturaPorPiso(pastelBean);
                 //System.out.println("Result Guardado colores: " + resultGuardadoColores );
                 // TO-DO: Guardar Extras
             } 
        } else {
        	// TO-DO Manage the error
        }     
	      
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
    	//System.out.println("IdCompra3:" + idCompra);
		return idCompra;
	}
	
	public int guardarColorCoberturaPorPiso(CompraPastelArmado pastelBean){
    	int result = 0;
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        List<String> idsColoresCobertura = pastelBean.getIdsColoresCobertura();
        
        int piso = 1;
        for (String idColorCobertura : idsColoresCobertura ){
	        String sql = 
	        		"INSERT INTO color_cobertura_por_piso"+
	        		"(id_compra," +
	        		"id_piso," +
	        		"id_color_cobertura) " +
	        		"VALUES" +
	        		"("+ pastelBean.getIdCompraPastelArmado() + 
	        		","+ piso +
	        		","+ idColorCobertura +
		    	    ");";
		     //System.out.println("getIdCompraPastelArmado" +  pastelBean.getIdCompraPastelArmado());
	         //System.out.println(sql);
		     result = instruccion.executeUpdate(sql);
		     piso++;
	     }	     
	      
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return result;
	}	
	
	public int modificar(CompraPastelArmado pastelBean){
		return 0;
	}
	
	public int eliminar(CompraPastelArmado pastelBean){
		return 0;
	}
	
	public CompraPastelArmado buscarPorId(String idPastel){
		CompraPastelArmado compra = new CompraPastelArmado();
		try {
	        // Open a connection
	        instruccion = conseguirInstruccion();
			// crear query
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT p.id_compra_pastel_armado, tp.descripcion as tipo_pastel, ");
			query.append("f.descripcion as forma, p.num_personas,p.id_piso, p.desc_extras,");
			query.append("s.descripcion as sabor, p.precio_pastel, p.precio_extras,");
			query.append("r.descripcion as relleno, c.descripcion as cobertura,");
			query.append("t.descripcion as textura, p.mensaje, p.observaciones,");
			query.append("su.descripcion as sucursal, p.fecha_entrega, p.hora_entrega");
			query.append(" FROM sharons.compra_pastel_armado as p, tipo_pastel as tp, forma as f,");
			query.append("sabor as s, relleno as r, cobertura c, textura t, sucursales as su");
			query.append(" where p.id_compra_pastel_armado = " + idPastel);
			query.append(" AND p.id_tipo_pastel = tp.id_tipo_pastel");
			query.append(" AND p.id_forma = f.id_forma");
			query.append(" AND p.id_sabor = s.id_sabor");
			query.append(" AND p.id_relleno = r.id_relleno");
			query.append(" AND p.id_cobertura = c.id_cobertura");
			query.append(" AND p.id_textura = t.id_textura");
			query.append(" AND p.id_sucursal_entrega = su.id_sucursal");
			System.out.println(query.toString());
			rSet = instruccion.executeQuery(query.toString());

			if ( rSet.next() ){
				compra.setIdCompraPastelArmado(rSet.getInt("id_compra_pastel_armado"));
				compra.setDescTipoPastel(rSet.getString("tipo_pastel"));
				compra.setDescForma(rSet.getString("forma"));
				compra.setIdPiso(rSet.getInt("id_piso"));
				compra.setDescSabor(rSet.getString("sabor"));
				compra.setDescRelleno(rSet.getString("relleno"));
				compra.setDescCobertura(rSet.getString("cobertura"));
				compra.setDescTextura(rSet.getString("textura"));
				compra.setNumPersonas(Integer.parseInt(rSet.getString("num_personas")));
				compra.setMensaje(rSet.getString("mensaje"));
				compra.setObservaciones(rSet.getString("observaciones"));		
				compra.setDescSucursalEntrega(rSet.getString("sucursal"));
				compra.setFechaEntrega(rSet.getString("fecha_entrega"));
				compra.setHorario(rSet.getString("hora_entrega"));
				compra.setPrecioPastel(rSet.getInt("precio_pastel"));
				compra.setPrecioExtras(rSet.getInt("precio_extras"));
				compra.setDescExtras(rSet.getString("desc_extras"));
				List<String> coloresCobertura = this.buscarPorIdColorPorPiso(idPastel);
				
				compra.setColoresCobertura(coloresCobertura);

			}			
		} catch(SQLException sqle) {
			System.out.println("Ocurrió un error en TipoPastelDao.getDescPorIdTipo");
			sqle.printStackTrace();
			
		}		
		return compra;
	}
	
	public List<String> buscarPorIdColorPorPiso(String idPastel){
		List<String> listColorPiso = new ArrayList<String>();
		try {
	        // Open a connection
	        instruccion = conseguirInstruccion();
			// crear query
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT cp.id_compra, cp.id_piso, c.descripcion color_cobertura");
			query.append(" FROM sharons.color_cobertura_por_piso as cp, color_cobertura as c");
			query.append(" WHERE cp.id_compra =");
			query.append( idPastel );
			query.append(" AND cp.id_color_cobertura = c.id_color_cobertura;");

			// System.out.println(query.toString());
			rSet = instruccion.executeQuery(query.toString());
			
			
			while ( rSet.next() ){
				listColorPiso.add(rSet.getString("color_cobertura"));
			}			
		} catch(SQLException sqle) {
			System.out.println("Ocurrió un error en TipoPastelDao.getDescPorIdTipo");
			sqle.printStackTrace();
			
		}
		System.out.println("listColorPiso: " +listColorPiso);
		return listColorPiso;
	}
}
