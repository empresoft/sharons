package mx.sharons.dao;

import java.sql.SQLException;
import java.util.ArrayList;

import mx.sharons.beans.Extra;
import mx.sharons.beans.Tipo;
import mx.sharons.beans.Unidad;
import mx.sharons.db.FabricaDeConexiones;
import mx.sharons.db.SQLDaoImpl;
import mx.sharons.utils.Constantes;

public class ExtraDao extends SQLDaoImpl{
	
	public ArrayList<Extra> buscarPorIdTipo(int idTipo){
    	ArrayList<Extra> listaExtra = new ArrayList<Extra>();
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        // Execute SQL query
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT e.id_extra, e.titulo, e.id_tipo, t.descripcion as tipo_descripcion, ");
        sql.append("e.costo, e.id_unidad, u.descripcion as unidad_descripcion, ");
        sql.append("e.no_unidades, e.imagen, e.estatus ");
        sql.append("FROM extra e, unidad u, tipo_extra t ");
        sql.append("WHERE e.id_tipo =");
        sql.append(idTipo);
        sql.append(" AND e.id_tipo = t.id_tipo ");
        sql.append("AND e.id_unidad = u.id_unidad ");
        sql.append("AND e.estatus = 'A' ");
        sql.append("ORDER BY e.id_extra ");
	     
        // System.out.println(sql);
	    rSet = instruccion.executeQuery(sql.toString());
	     
	    while ( rSet.next() ){
	    	Extra extra = new Extra();
	    	
	    	extra.setIdExtra(rSet.getInt("id_extra"));
	    	extra.setTitulo(rSet.getString("titulo"));
	    	
	    	Tipo tipoExtra = new Tipo();
	    	tipoExtra.setIdTipo(rSet.getInt("id_tipo"));
	    	tipoExtra.setDescripcion(rSet.getString("tipo_descripcion"));
	    	extra.setTipo(tipoExtra);
	    	
	    	Unidad unidad = new Unidad();
	    	unidad.setIdUnidad(rSet.getInt("id_unidad"));
	    	unidad.setDescripcion(rSet.getString("unidad_descripcion"));
	    	extra.setUnidad(unidad);
	    	
	    	extra.setCosto(rSet.getInt("costo"));
	    	extra.setEstatus(rSet.getString("estatus"));
	    	extra.setImagen(rSet.getString("imagen"));
	    	extra.setNoUnidades(rSet.getInt("no_unidades"));
	    	
	    	listaExtra.add(extra);
	    }  
	    
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return listaExtra;
	}
	
	public int saveOrUpdate(Extra extra){
    	int result = 0;
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	int idExtra = extra.getIdExtra();
    	String sql = "";
    	
        if ( idExtra > 0 ){
        	sql = buildUpdateQuery(extra);
        } else {
        	sql = buildSaveQuery(extra);
        }
	     
        System.out.println(sql);
	    result = instruccion.executeUpdate( sql );
	     
	      
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return result;
	}

	private String buildSaveQuery(Extra extra) {
		StringBuilder qry = new StringBuilder();
		qry.append("INSERT INTO extra");
		qry.append("(titulo,");
		qry.append("id_tipo,");
		qry.append("costo,");
		qry.append("id_unidad,");
		qry.append("no_unidades,");
		qry.append("imagen,");
		qry.append("estatus) ");
		qry.append("VALUES");
		qry.append("('");
		qry.append(extra.getTitulo());
		qry.append("',");
		qry.append(extra.getTipo().getIdTipo());
		qry.append(",");
		qry.append(extra.getCosto());
		qry.append(",");
		qry.append(extra.getUnidad().getIdUnidad());
		qry.append(",");
		qry.append(extra.getNoUnidades());
		qry.append(",'");
		qry.append(extra.getImagen());
		qry.append("','");
		qry.append(Constantes.ACTIVO);
		qry.append("');");

		return qry.toString();
	}	
	
	private String buildUpdateQuery(Extra extra) {
		StringBuilder qry = new StringBuilder();
		qry.append("UPDATE extra");
		qry.append(" set titulo='");
		qry.append(extra.getTitulo());
		qry.append("', id_tipo=");
		qry.append(extra.getTipo().getIdTipo());
		qry.append(", costo =");
		qry.append(extra.getCosto());
		qry.append(", id_unidad = ");
		qry.append(extra.getUnidad().getIdUnidad());
		qry.append(", no_unidades = ");
		qry.append(extra.getNoUnidades());
		qry.append(", imagen = '");
		qry.append(extra.getImagen());
		qry.append("' WHERE id_extra = ");
		qry.append(extra.getIdExtra());
		
		return qry.toString();
	}

	public int delete(int idExtra) {
    	int result = 0;
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
		StringBuilder qry = new StringBuilder();
		qry.append("UPDATE extra");
		qry.append(" set estatus='");
		qry.append("E");
		qry.append("' WHERE id_extra = ");
		qry.append(idExtra);
	     
        System.out.println(qry);
	    result = instruccion.executeUpdate( qry.toString() );
	     
	      
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return result;			
	}	
	
	public int getCostoById(int idExtra){
		int costo = 0;
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        // Execute SQL query
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT costo FROM sharons.extra where id_extra= ");
        sql.append(idExtra);
	     
        // System.out.println(sql);
	    rSet = instruccion.executeQuery(sql.toString());
	     
	    if ( rSet.next() ){
	    	costo = rSet.getInt("costo");
	    }  
	    
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return costo;
	}
	
	public String getDescpripcionById(int idExtra){
		String titulo = "";
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        // Execute SQL query
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT titulo FROM sharons.extra where id_extra= ");
        sql.append(idExtra);
	     
        // System.out.println(sql);
	    rSet = instruccion.executeQuery(sql.toString());
	     
	    if ( rSet.next() ){
	    	titulo = rSet.getString("titulo");
	    }  
	    
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return titulo;
	}
}
