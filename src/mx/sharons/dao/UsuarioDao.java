package mx.sharons.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import mx.sharons.beans.Usuario;
import mx.sharons.db.FabricaDeConexiones;
import mx.sharons.db.SQLDaoImpl;

public class UsuarioDao extends SQLDaoImpl{

	public int guardar(Usuario usuarioBean) {
    	int result = 0;
    	
    	try {

        // Open a connection
        instruccion = conseguirInstruccion();
    	
        // Execute SQL query
        String sql = 
        		"INSERT INTO usuario"+
        		"(id_usuario, " +
        		"nombre," +
        		"apellidos," +
        		"correo, " +
        		"telefono,"+
        		"direccion, " +
        		"password) "+
        		"VALUES" +
        		"(0"+ 
        		",'"+ usuarioBean.getNombre() +
        		"','"+ usuarioBean.getApellidos() +
        		"','"+ usuarioBean.getCorreo() +
        		"','"+ usuarioBean.getTelefono() +
        		"','"+ usuarioBean.getDireccion() +
        		"','"+ usuarioBean.getPassword() +
	    	    "');";
	     
        System.out.println(sql);
	     result = instruccion.executeUpdate(sql);
	     
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
		return result;		
	}

	public Usuario login(String correo, String password) {
    	Usuario usuarioBean = new Usuario();
    	try {
    		instruccion = conseguirInstruccion();
        
	    	String sql = 
	     		"SELECT * FROM noticia "+
		   	    " WHERE correo=' " + correo +
		   	    " ' AND password='" + password;
		     
	        System.out.println(sql);
	        ResultSet rSet = instruccion.executeQuery(sql);
		    
		    while( rSet.next() ){
		    	usuarioBean.setId(rSet.getInt("id"));
		    	usuarioBean.setNombre(rSet.getString("nombre"));
		    	usuarioBean.setApellidos(rSet.getString("appelidos"));
		    	usuarioBean.setCorreo(rSet.getString("correo"));
		    	usuarioBean.setDireccion(rSet.getString("direccion"));
		    	usuarioBean.setPassword(rSet.getString("password"));
		    	usuarioBean.setEstatus(rSet.getString("estatus"));
		    	usuarioBean.setTelefono(rSet.getString("telefono"));
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
    	
		return usuarioBean;
	}

	public Usuario buscarPorCorreo(String correo) {
    	Usuario usuarioBean = new Usuario();
    	try {
    		instruccion = conseguirInstruccion();
        
	    	String sql = 
	     		"SELECT * FROM noticia " +
		   	    " WHERE correo='" + correo + "';";
		     
	        System.out.println(sql);
	        ResultSet rSet = instruccion.executeQuery(sql);
		    
		    while( rSet.next() ){
		    	usuarioBean.setId(rSet.getInt("id"));
		    	usuarioBean.setNombre(rSet.getString("nombre"));
		    	usuarioBean.setApellidos(rSet.getString("appelidos"));
		    	usuarioBean.setCorreo(rSet.getString("correo"));
		    	usuarioBean.setDireccion(rSet.getString("direccion"));
		    	usuarioBean.setPassword(rSet.getString("password"));
		    	usuarioBean.setEstatus(rSet.getString("estatus"));
		    	usuarioBean.setTelefono(rSet.getString("telefono"));
		    }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally{
			FabricaDeConexiones.cerrar( instruccion );
	        FabricaDeConexiones.cerrar( conexion );	
		}
    	
		return usuarioBean;
	}

}
