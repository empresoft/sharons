package mx.sharons.utils;

public class PayUSignGenerator {
	final String API_KEY = "2bc7agV76vsZLrrFRDJ6Rxoxs0";
	final String MERCHANT_ID = "608912";
	final String CURRENCY = "MXN";
	
	private String reference; 
	private double amount;
	
	public PayUSignGenerator(String reference, double amount){
		this.reference = reference;
		this.amount = amount;
	}
	
	public String getSign(){
		return API_KEY + "~" + MERCHANT_ID +  "~" + reference +  "~" + amount + "~" + CURRENCY;
	}
	
	public String getEncryptedSign(){
		return EncryptorMD5.encriptaEnMD5(this.getSign());
	}
	
    public static void main(String args[])
    {
    	PayUSignGenerator generator = new PayUSignGenerator("PBXV10", 700.00);
    	
        System.out.println("Signature: '"+ generator.getEncryptedSign() +"'");
    }
}
