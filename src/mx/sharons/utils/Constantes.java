package mx.sharons.utils;

public class Constantes {
	public static final String ACTIVO = "A";
	public static final String PENDIENTE = "P"; 
	public static final int PRECIO_BASE_PASTEL_BODAS_XV = 450;
	public static final int PRECIO_UNIDAD_FLORES_NATURALES = 50;
	public static final int PRECIO_UNIDAD_FLORES_FONDANT = 60;
	
}
