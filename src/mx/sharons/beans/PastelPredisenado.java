package mx.sharons.beans;

public class PastelPredisenado {
	int idPastelPredisenado;
	int idTipoPastel;
	String nombre;
	String descripcion;
	int precioPorcion;
	int adicional;
	String sabor;
	String observaciones;
	String imagen;
	
	public int getIdPastelPredisenado() {
		return idPastelPredisenado;
	}
	public void setIdPastelPredisenado(int idPastelPredisenado) {
		this.idPastelPredisenado = idPastelPredisenado;
	}
	public int getIdTipoPastel() {
		return idTipoPastel;
	}
	public void setIdTipoPastel(int idTipoPastel) {
		this.idTipoPastel = idTipoPastel;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getPrecioPorcion() {
		return precioPorcion;
	}
	public void setPrecioPorcion(int precioPorcion) {
		this.precioPorcion = precioPorcion;
	}
	public int getAdicional() {
		return adicional;
	}
	public void setAdicional(int adicional) {
		this.adicional = adicional;
	}
	public String getSabor() {
		return sabor;
	}
	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
}
