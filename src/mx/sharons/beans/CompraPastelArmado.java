package mx.sharons.beans;

import java.util.ArrayList;
import java.util.List;

public class CompraPastelArmado {
	public int idCompraPastelArmado;
	public int idTipoPastel;
	public int idForma;
	public int idPiso;
	public int idSabor;
	public int idRelleno;
	public int numPersonas;
	public int idCobertura;
	public int idTextura;
	public String mensaje;
	public String observaciones;
	public String descTipoPastel;
	public String descForma;
	public String descTamano;
	public String descSabor;
	public String descRelleno;
	public String descCobertura;
	public String descPisos;
	public String descTextura;
	public List<String> coloresCobertura;
	public List<String> idsColoresCobertura;
	public int idSucursalEntrega;
	public String descSucursalEntrega;
	public int precioPastel;
	public double precioExtras;
	public String horario;
	public String fechaEntrega;
	public ArrayList<CompraExtras> compraExtrasList; 
	public String descExtras;
	private String correoCliente;
	
	public CompraPastelArmado(){	
	}
	
	public int getIdCompraPastelArmado() {
		return idCompraPastelArmado;
	}
	public void setIdCompraPastelArmado(int idCompraPastelArmado) {
		this.idCompraPastelArmado = idCompraPastelArmado;
	}
	public void setCompraExtrasList(ArrayList<CompraExtras> compraExtrasList) {
		this.compraExtrasList = compraExtrasList;
	}
	public void setDescExtras(String descExtras) {
		this.descExtras = descExtras;
	}
	public int getIdTipoPastel() {
		return idTipoPastel;
	}
	public void setIdTipoPastel(int idTipoPastel) {
		this.idTipoPastel = idTipoPastel;
	}
	public int getIdForma() {
		return idForma;
	}
	public void setIdForma(int idForma) {
		this.idForma = idForma;
	}
	public int getIdSabor() {
		return idSabor;
	}
	public void setIdSabor(int idSabor) {
		this.idSabor = idSabor;
	}
	public int getIdRelleno() {
		return idRelleno;
	}
	public void setIdRelleno(int idRelleno) {
		this.idRelleno = idRelleno;
	}
	public int getNumPersonas() {
		return numPersonas;
	}
	public void setNumPersonas(int numPersonas) {
		this.numPersonas = numPersonas;
	}	
	public int getIdCobertura() {
		return idCobertura;
	}
	public void setIdCobertura(int idCobertura) {
		this.idCobertura = idCobertura;
	}
	public int getIdTextura() {
		return idTextura;
	}
	public void setIdTextura(int idTextura) {
		this.idTextura = idTextura;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	public String getDescTipoPastel() {
		return descTipoPastel;
	}
	public void setDescTipoPastel(String descTipoPastel) {
		this.descTipoPastel = descTipoPastel;
	}
	public String getDescForma() {
		return descForma;
	}
	public void setDescForma(String descForma) {
		this.descForma = descForma;
	}
	public String getDescTamano() {
		return descTamano;
	}
	public void setDescTamano(String descTamano) {
		this.descTamano = descTamano;
	}
	public String getDescSabor() {
		return descSabor;
	}
	public void setDescSabor(String descSabor) {
		this.descSabor = descSabor;
	}
	public String getDescRelleno() {
		return descRelleno;
	}
	public void setDescRelleno(String descRelleno) {
		this.descRelleno = descRelleno;
	}
	public String getDescCobertura() {
		return descCobertura;
	}
	public void setDescCobertura(String descCobertura) {
		this.descCobertura = descCobertura;
	}
	public int getIdPiso() {
		return idPiso;
	}
	public void setIdPiso(int idPiso) {
		this.idPiso = idPiso;
	}
	public String getDescPisos() {
		return descPisos;
	}
	public void setDescPisos(String descPisos) {
		this.descPisos = descPisos;
	}
	public String getDescTextura() {
		return descTextura;
	}
	public void setDescTextura(String descTextura) {
		this.descTextura = descTextura;
	}
	public List<String> getColoresCobertura() {
		return coloresCobertura;
	}
	public void setColoresCobertura(List<String> coloresCobertura) {
		this.coloresCobertura = coloresCobertura;
	}
	public int getIdSucursalEntrega() {
		return idSucursalEntrega;
	}
	public void setIdSucursalEntrega(int idSucursalEntrega) {
		this.idSucursalEntrega = idSucursalEntrega;
	}
	public String getDescSucursalEntrega() {
		return descSucursalEntrega;
	}
	public void setDescSucursalEntrega(String descSucursalEntrega) {
		this.descSucursalEntrega = descSucursalEntrega;
	}
	public List<String> getIdsColoresCobertura() {
		return idsColoresCobertura;
	}
	public void setIdsColoresCobertura(List<String> idsColoresCobertura) {
		this.idsColoresCobertura = idsColoresCobertura;
	}

	public int getPrecioPastel() {
		return precioPastel;
	}

	public void setPrecioPastel(int precioPastel) {
		this.precioPastel = precioPastel;
	}

	public double getPrecioExtras() {
		return precioExtras;
	}
	public void setPrecioExtras(double precioExtras) {
		this.precioExtras = precioExtras;
	}
	
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public ArrayList<CompraExtras> getCompraExtrasList() {
		return compraExtrasList;
	}
	public void setExtras(ArrayList<CompraExtras> compraExtrasList) {
		this.compraExtrasList = compraExtrasList;
	}
	public String getDescExtras() {
		return descExtras;
	}
	public void setResumenExtras(String descExtras) {
		this.descExtras = descExtras;
	}

	public String getCorreoCliente() {
		return correoCliente;
	}
	
	public void setCorreoCliente(String correoCliente) {
		this.correoCliente = correoCliente;
	}
	
	
}
