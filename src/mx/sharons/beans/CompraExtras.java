package mx.sharons.beans;

public class CompraExtras {
	int idExtra;
	String color;
	double cantidad;
	String descUnidad;
	
	public int getIdExtra() {
		return idExtra;
	}
	public void setIdExtra(int idExtra) {
		this.idExtra = idExtra;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	public double getCantidad() {
		return cantidad;
	}
	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}
	public String getDescUnidad() {
		return descUnidad;
	}
	public void setDescUnidad(String descUnidad) {
		this.descUnidad = descUnidad;
	}
	@Override
	public String toString(){
		return "idExtra: " + idExtra + " cantidad: " + cantidad +
				" descUnidad: " + descUnidad +
				" color: " + color;
	}
}
