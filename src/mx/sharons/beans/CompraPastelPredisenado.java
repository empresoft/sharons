package mx.sharons.beans;

public class CompraPastelPredisenado {
	
	int idCompraPastelPredisenado;
	int idPastelPredisenado;
	int numPersonas;
	String mensaje;
	int idSucursalEntrega;
	String sucursalEntrega;
	String fechaEntrega;
	String horaEntrega;
	String observaciones;
	Double precio;
	String imagen;
	
	public int getIdCompraPastelPredisenado() {
		return idCompraPastelPredisenado;
	}
	public void setIdCompraPastelPredisenado(int idCompraPastelPredisenado) {
		this.idCompraPastelPredisenado = idCompraPastelPredisenado;
	}
	public int getNumPersonas() {
		return numPersonas;
	}
	public void setNumPersonas(int numPersonas) {
		this.numPersonas = numPersonas;
	}
	
	public int getIdPastelPredisenado() {
		return idPastelPredisenado;
	}
	public void setIdPastelPredisenado(int idPastelPredisenado) {
		this.idPastelPredisenado = idPastelPredisenado;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public int getIdSucursalEntrega() {
		return idSucursalEntrega;
	}
	public void setIdSucursalEntrega(int idSucursalEntrega) {
		this.idSucursalEntrega = idSucursalEntrega;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public String getImagen() {
		return imagen;
	}
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	public String getSucursalEntrega() {
		return sucursalEntrega;
	}
	public void setSucursalEntrega(String sucursalEntrega) {
		this.sucursalEntrega = sucursalEntrega;
	}
	public String getFechaEntrega() {
		return fechaEntrega;
	}
	public void setFechaEntrega(String fechaEntrega) {
		this.fechaEntrega = fechaEntrega;
	}
	public String getHoraEntrega() {
		return horaEntrega;
	}
	public void setHoraEntrega(String horaEntrega) {
		this.horaEntrega = horaEntrega;
	}
}
