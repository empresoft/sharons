package mx.sharons.db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

            
public class FabricaDeConexiones {
    private static FabricaDeConexiones ref = new FabricaDeConexiones();
    // JDBC driver name and database URL
    public static final String JDBC_DRIVER="com.mysql.cj.jdbc.Driver";  
    //public static final String DB_URL="jdbc:mysql://127.0.0.1:3306/sharons?serverTimezone=UTC";
    public static final String DB_URL="jdbc:mysql://db-clientes.cvfhwrbgz5jh.us-west-1.rds.amazonaws.com:3306/sharons?serverTimezone=UTC&autoReconnect=true";
    

    //  Database credentials
    public static final String USER = "root";
    public static final String PASS = "admin123";
    
    private FabricaDeConexiones() {
        try{
            DriverManager.registerDriver( new com.mysql.cj.jdbc.Driver() );
        } catch( SQLException excepcionSql){
            //System.out.println("ERROR: Excepcion al leer el controlador");
            excepcionSql.printStackTrace();
        }
    }
    
    public static Connection conseguirConexion() throws SQLException {
        return DriverManager.getConnection(
        		DB_URL, USER, PASS);
    }
    
    // Metodo sobrecargado que recibe un parametro ResultSet
    public static void cerrar(ResultSet rs) {
        try{
        	if ( rs != null){
        		rs.close();
        	}
        } catch (Exception ignored){
        	ignored.printStackTrace();
        }
    }
    
    // Metodo sobrecargado que recibe un parametro Statement
    public static void cerrar(Statement stmt) {
        try{
        	if ( stmt != null){
        		stmt.close();       
        	}
        } catch (Exception ignored){
        	ignored.printStackTrace();
        }
    }
    
    // Metodo sobrecargado que recibe un parametro Connection
    public static void cerrar(Connection conn) {
        try{
        	if ( conn != null){
        		conn.close();
        	}
        } catch (Exception ignored){
        	ignored.printStackTrace();
        }
    }

    public static void cerrarRecursosDB(ResultSet rSet, Statement instruccion, Connection conexion) {
        cerrar(rSet);
        cerrar(instruccion);
        cerrar(conexion);
    }

    public static Statement conseguirInstruccion(Connection conexion) throws SQLException {
        return conexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
    }

}
