-- MySQL dump 10.13  Distrib 5.5.19, for Linux (x86_64)
--
-- Host: localhost    Database: sharons
-- ------------------------------------------------------
-- Server version	5.5.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cobertura`
--

DROP TABLE IF EXISTS `cobertura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cobertura` (
  `id_cobertura` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `precio` int(4) DEFAULT NULL,
  PRIMARY KEY (`id_cobertura`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cobertura`
--

LOCK TABLES `cobertura` WRITE;
/*!40000 ALTER TABLE `cobertura` DISABLE KEYS */;
INSERT INTO `cobertura` VALUES (1,'Chantilly ',0),(2,'Chantilly de chocolate',0),(3,'Crema natural',8),(4,'Fondant alto',55),(5,'Fondant inicial',35),(6,'Fondant medio',45),(7,'Queso crema',8);
/*!40000 ALTER TABLE `cobertura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color_cobertura`
--

DROP TABLE IF EXISTS `color_cobertura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color_cobertura` (
  `id_color_cobertura` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_color_cobertura`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color_cobertura`
--

LOCK TABLES `color_cobertura` WRITE;
/*!40000 ALTER TABLE `color_cobertura` DISABLE KEYS */;
INSERT INTO `color_cobertura` VALUES (1,'Amarillo'),(2,'Amarillo Pastel'),(3,'Arena'),(4,'Azul Pastel'),(5,'Azul Rey'),(6,'Azul Turquesa'),(7,'Beige'),(8,'Blanco'),(9,'Cafe'),(10,'Coral'),(11,'Dorado'),(12,'Lila'),(13,'Morado'),(14,'Naranja'),(15,'Negro'),(16,'Perla'),(17,'Plateado'),(18,'Rojo'),(19,'Rosa Fiusha'),(20,'Rosa Pastel'),(21,'Verde aqua'),(22,'Verde Bandera'),(23,'Verde Jade'),(24,'Verde Limón');
/*!40000 ALTER TABLE `color_cobertura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color_cobertura_por_piso`
--

DROP TABLE IF EXISTS `color_cobertura_por_piso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color_cobertura_por_piso` (
  `id_compra` int(11) NOT NULL,
  `id_piso` int(11) NOT NULL,
  `id_color_cobertura` int(11) NOT NULL,
  PRIMARY KEY (`id_compra`,`id_piso`,`id_color_cobertura`),
  KEY `fk_id_piso_idx` (`id_piso`),
  KEY `fk_id_color_cobertura_idx` (`id_color_cobertura`),
  CONSTRAINT `fk_id_compra_pastel_armado` FOREIGN KEY (`id_compra`) REFERENCES `compra_pastel_armado` (`id_compra_pastel_armado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color_cobertura_por_piso`
--

LOCK TABLES `color_cobertura_por_piso` WRITE;
/*!40000 ALTER TABLE `color_cobertura_por_piso` DISABLE KEYS */;
INSERT INTO `color_cobertura_por_piso` VALUES (7,1,1),(12,1,4),(13,1,4),(14,1,4),(20,1,4),(21,1,1),(22,1,11),(23,1,4),(24,1,4),(25,1,4),(26,1,4),(27,1,4),(28,1,4),(30,1,4),(31,1,4),(32,1,4),(33,1,2),(34,1,4),(35,1,2),(36,1,4),(37,1,4),(38,1,4),(39,1,1),(40,1,1),(41,1,1),(42,1,1),(43,1,1),(44,1,2),(45,1,1),(46,1,1),(47,1,4),(48,1,1),(49,1,1),(50,1,4),(51,1,1),(52,1,1),(56,1,4),(57,1,1),(58,1,3),(59,1,3),(60,1,1),(61,1,7),(62,1,4),(63,1,4),(64,1,2),(65,1,1),(66,1,4),(67,1,4),(68,1,4),(69,1,7),(70,1,1),(71,1,1),(72,1,1),(73,1,4),(50,2,2),(51,2,2),(52,2,3),(57,2,20),(64,2,20),(66,2,2),(69,2,2),(71,2,4),(72,2,4),(73,2,4),(57,3,23),(69,3,10),(71,3,5);
/*!40000 ALTER TABLE `color_cobertura_por_piso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compra_pastel_armado`
--

DROP TABLE IF EXISTS `compra_pastel_armado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compra_pastel_armado` (
  `id_compra_pastel_armado` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_pastel` int(11) DEFAULT NULL,
  `id_forma` int(11) DEFAULT NULL,
  `num_personas` int(11) DEFAULT NULL,
  `id_piso` int(11) DEFAULT NULL,
  `id_sabor` int(11) DEFAULT NULL,
  `id_relleno` int(11) DEFAULT NULL,
  `id_cobertura` int(11) DEFAULT NULL,
  `id_textura` int(11) DEFAULT NULL,
  `mensaje` varchar(100) DEFAULT NULL,
  `observaciones` varchar(5000) DEFAULT NULL,
  `id_sucursal_entrega` int(11) DEFAULT NULL,
  `fecha_entrega` varchar(45) DEFAULT NULL,
  `hora_entrega` varchar(45) DEFAULT NULL,
  `precio_extras` decimal(6,2) DEFAULT NULL,
  `precio_pastel` decimal(6,2) DEFAULT NULL,
  `desc_extras` varchar(500) DEFAULT NULL,
  `estatus` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_compra_pastel_armado`),
  KEY `fk_id_forma_idx` (`id_forma`),
  KEY `fk_id_sabor_idx` (`id_sabor`),
  KEY `fk_id_relleno_idx` (`id_relleno`),
  KEY `fk_id_cobertura_idx` (`id_cobertura`),
  KEY `fk_id_textura_idx` (`id_textura`),
  KEY `fk_id_mensaje_idx` (`mensaje`),
  KEY `fk_id_tipo_pastel_idx` (`id_tipo_pastel`),
  KEY `fk_id_sucursal_idx` (`id_sucursal_entrega`),
  CONSTRAINT `fk_id_cobertura` FOREIGN KEY (`id_cobertura`) REFERENCES `cobertura` (`id_cobertura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_forma` FOREIGN KEY (`id_forma`) REFERENCES `forma` (`id_forma`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_relleno` FOREIGN KEY (`id_relleno`) REFERENCES `relleno` (`id_relleno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_sabor` FOREIGN KEY (`id_sabor`) REFERENCES `sabor` (`id_sabor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_sucursal` FOREIGN KEY (`id_sucursal_entrega`) REFERENCES `sucursales` (`id_sucursal`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_textura` FOREIGN KEY (`id_textura`) REFERENCES `textura` (`id_textura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_tipo_pastel` FOREIGN KEY (`id_tipo_pastel`) REFERENCES `tipo_pastel` (`id_tipo_pastel`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra_pastel_armado`
--

LOCK TABLES `compra_pastel_armado` WRITE;
/*!40000 ALTER TABLE `compra_pastel_armado` DISABLE KEYS */;
INSERT INTO `compra_pastel_armado` VALUES (5,1,1,2,2,3,2,1,3,'qwerty',NULL,1,NULL,NULL,NULL,NULL,NULL,'A'),(7,1,2,2,1,6,1,2,1,'qwert',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'A'),(12,1,2,50,1,6,1,3,6,'qwer',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'P'),(13,1,2,50,1,5,1,2,3,'234r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'P'),(14,1,2,50,1,5,1,2,3,'234r',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'P'),(15,1,2,50,1,5,1,2,3,'qwert',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'P'),(16,1,2,50,1,6,1,2,1,'qwed',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'P'),(17,1,2,10,2,6,1,1,1,'test',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'P'),(18,1,1,10,1,1,7,1,5,'msj',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'P'),(19,1,1,10,1,5,1,1,1,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(20,1,1,10,1,5,6,1,1,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(21,1,2,10,1,6,1,1,6,'mensaje','obsr',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(22,1,2,10,1,6,1,2,2,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(23,1,2,10,1,7,1,1,6,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(24,1,1,10,1,6,1,2,1,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(25,1,2,10,1,6,1,1,6,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(26,1,2,10,3,6,1,1,6,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(27,1,2,10,1,5,1,1,6,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(28,1,2,10,1,6,1,3,6,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(30,1,2,10,1,6,1,1,6,'qwrqw','qweqw',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(31,1,2,10,1,6,1,3,2,'qweqe','qwerty',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(32,1,2,10,1,5,1,3,2,'qweqe','qwerty',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(33,1,2,10,1,1,1,1,6,'msj','obs',NULL,NULL,NULL,NULL,NULL,NULL,'P'),(34,1,2,10,1,6,1,1,6,'','qwert',4,'14/08/2019','12:30 pm',NULL,NULL,NULL,'P'),(35,1,2,10,1,6,1,2,1,'msj','obs',4,'14/08/2019','12:30 pm',NULL,NULL,NULL,'P'),(36,1,1,10,1,6,1,3,6,'qwe','obs',3,'14/08/2019','12:30 pm',NULL,0.00,NULL,'P'),(37,1,2,10,1,5,1,1,1,'qwert','qwer',1,'14/08/2019','12:30 pm',NULL,495.00,NULL,'P'),(38,1,2,10,1,5,1,1,1,'qwert','qwer',1,'14/08/2019','12:30 pm',NULL,495.00,NULL,'P'),(39,1,2,10,1,4,6,1,6,'msj','obs',1,'14/08/2019','12:30 pm',130.00,393.00,NULL,'P'),(40,1,1,10,1,1,1,1,6,'qwer','obs',1,'14/08/2019','10:30 am',49.00,360.00,NULL,'P'),(41,1,2,10,1,1,2,1,1,'qwert','qqwert',1,'14/08/2019','12:30 pm',0.00,350.00,NULL,'P'),(42,1,1,10,1,1,1,1,1,'qwer','obs',3,'14/08/2019','12:30 pm',0.00,360.00,NULL,'P'),(43,1,1,10,1,6,1,1,2,'qwer','qwer',1,'14/08/2019','12:30 pm',195.00,450.00,NULL,'P'),(44,1,2,10,1,5,1,1,6,'wert','qwe',3,'14/08/2019','12:30 pm',0.00,430.00,NULL,'P'),(45,1,1,10,1,1,6,1,6,'','',1,'14/08/2019','12:30 pm',0.00,373.00,NULL,'P'),(46,1,1,10,1,1,2,1,6,'','',3,'14/08/2019','11:30 am',0.00,350.00,NULL,'P'),(47,1,1,10,1,1,3,1,1,'qwer','qwe',1,'14/08/2019','12:30 pm',0.00,350.00,NULL,'P'),(48,1,1,10,1,3,3,1,2,'qwert','qwert',1,'14/08/2019','12:30 pm',0.00,370.00,NULL,'P'),(49,1,1,10,1,6,1,1,5,'','',1,'14/08/2019','11:30 am',0.00,450.00,NULL,'P'),(50,1,1,10,2,1,1,2,5,'qwer','qwer',1,'14/08/2019','11:30 am',0.00,440.00,NULL,'P'),(51,1,1,10,2,4,4,2,2,'123','1233e',1,'14/08/2019','12:30 pm',163.00,470.00,NULL,'P'),(52,1,1,10,2,5,1,2,6,'qwer','obs',1,'14/08/2019','12:30 pm',300.00,510.00,NULL,'P'),(53,1,1,10,2,3,1,2,5,'123','1234',4,'14/08/2019','10:30 am',70.00,460.00,NULL,'P'),(54,1,1,10,1,3,2,1,2,'123','1234',4,'14/08/2019','10:30 am',0.00,370.00,NULL,'P'),(55,1,1,10,1,5,1,1,2,'qwer','qwert',3,'14/08/2019','12:30 pm',0.00,430.00,NULL,'P'),(56,1,1,10,1,6,1,3,5,'','',3,'14/08/2019','12:30 pm',0.00,450.00,NULL,'P'),(57,1,1,10,3,4,5,2,2,'','',1,'14/08/2019','12:30 pm',0.00,540.00,NULL,'P'),(58,1,2,10,1,1,2,2,2,'mensaje','obs',1,'15/08/2019','9:30 am',100.00,350.00,'1 extra(s) de TRÉBOL 4.5CM. 1 extra(s) de ROSA CHICA. ','P'),(59,1,1,10,1,1,5,1,4,'','',1,'15/08/2019','9:30 am',200.00,360.00,'2 Docenas TRÉBOL 4.5CM. 2 Piezas ROSA CHICA. ','P'),(60,1,1,10,1,6,1,3,5,'qwe','qwe',1,'15/08/2019','9:30 am',135.00,450.00,'-1 Docenas TRÉBOL 4.5CM.-2 Piezas ROSA CHICA.','P'),(61,1,1,10,1,1,1,1,2,'mensaje','obs',1,'15/08/2019','9:30 am',135.00,360.00,'-1 Docenas TRÉBOL 4.5CM. -2 Piezas ROSA CHICA. ','P'),(62,1,1,10,1,6,1,1,2,'','',1,'23/08/2019','7:30 am',0.00,450.00,'','P'),(63,1,1,10,1,13,1,1,2,'qwe','qwe',1,'23/08/2019','7:30 am',0.00,410.00,'','P'),(64,1,2,24,2,1,2,1,2,'qwer','qwer',1,'23/08/2019','7:30 am',65.00,808.00,'-1 Docenas TRÉBOL 4.5CM. ','P'),(65,1,1,11,1,1,3,4,2,'','',1,'23/08/2019','7:30 am',0.00,762.00,'','P'),(66,1,1,10,2,4,6,4,2,'','',1,'23/08/2019','7:30 am',80.00,823.00,'-1 Piezas OBLEA IMPRESA ACETATO. ','P'),(67,1,2,20,1,4,2,4,6,'sdfsdfsdfsdfsdf','sdfsdfsdfsdf',1,'23/08/2019','12:30 pm',525.00,1360.00,'-15 Piezas ROSA CHICA. ','P'),(68,1,1,10,1,6,1,1,7,'','',1,'29/08/2019','7:30 am',0.00,450.00,'','P'),(69,1,2,20,3,5,1,1,2,'hkhikhj','kjbhkhjkjhkj',1,'30/09/2019','10:30 am',350.00,860.00,'-1 Docenas ROSAS. ','P'),(70,1,2,100,1,1,5,1,2,'jkhkh','khjhkjhkh',1,'30/09/2019','11:30 am',130.00,3280.00,'-2 Docenas TRÉBOL 4.5CM. ','P'),(71,1,1,10,0,6,6,5,6,'','',1,'30/08/2019','7:30 am',0.00,940.00,'','P'),(72,1,2,10,0,1,2,2,4,'','',1,'06/10/2019','7:30 am',200.00,510.00,'-2 Docenas Estrella 5 cm. ','P'),(73,1,1,10,0,2,2,1,1,'','',1,'10/10/2019','7:30 am',0.00,590.00,'','P');
/*!40000 ALTER TABLE `compra_pastel_armado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `compra_pastel_predisenado`
--

DROP TABLE IF EXISTS `compra_pastel_predisenado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compra_pastel_predisenado` (
  `id_compra_pastel_predisenado` int(11) NOT NULL AUTO_INCREMENT,
  `id_pastel_predisenado` int(11) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `id_sucursal_entrega` varchar(45) DEFAULT NULL,
  `fecha_entrega` varchar(45) DEFAULT NULL,
  `mensaje` varchar(145) DEFAULT NULL,
  `hora_entrega` varchar(45) DEFAULT NULL,
  `observaciones` varchar(145) DEFAULT NULL,
  `num_personas` int(11) DEFAULT NULL,
  `estatus` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_compra_pastel_predisenado`),
  KEY `fk_pastel_predisenado_idx` (`id_pastel_predisenado`),
  CONSTRAINT `fk_pastel_predisenado` FOREIGN KEY (`id_pastel_predisenado`) REFERENCES `pastel_predisenado` (`id_pastel_predisenado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compra_pastel_predisenado`
--

LOCK TABLES `compra_pastel_predisenado` WRITE;
/*!40000 ALTER TABLE `compra_pastel_predisenado` DISABLE KEYS */;
INSERT INTO `compra_pastel_predisenado` VALUES (3,1,2850,'1','29/03/2018','MEnsaje','7:30 am','qwerty',50,'P'),(4,1,0,'1','29/03/2018','qwe','12:30 pm','qwer',10,'P'),(5,1,2850,'3','29/03/2018','123','12:30 pm','qwer',50,'P'),(6,1,2850,'3','29/03/2018','qwer','12:30 pm','OBS',50,'P'),(7,1,2850,'3','29/03/2018','MENSAJE','11:30 am','OBSERVACIONES',50,'P'),(8,1,0,'3','29/03/2018','MENSAJE','11:30 am','OBSERVACIONES',50,'P'),(9,1,2850,'4','29/03/2018','Mensaje','12:30 pm','OBS',50,'P'),(10,1,2850,'1','29/03/2018','Mensaje tex','12:30 pm','obs',50,'P'),(11,1,2850,'3','29/03/2018','MSJ','12:30 pm','OBS',50,'P'),(12,1,3018,'3','29/03/2018','MSJ','12:30 pm','OBS',54,'P'),(13,5,2390,'1','29/03/2018','msj','12:30 pm','obs',50,'P'),(14,5,2390,'3','29/03/2018','msj','12:30 pm','obs',50,'P'),(15,1,2850,'1','30/03/2018','Mensaje','12:30 pm','obs',50,'P'),(16,1,2850,'3','30/03/2018','12345','12:30 pm','qwert',50,'P'),(17,28,1940,'1','01/01/2020','','12:30 pm','',55,'P'),(18,28,0,'1','30/03/2018','dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd','12:30 pm','dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd',55,'P'),(19,28,0,'1','30/03/2018','dd ddddd ddddddddddddddd dddddddddddddddddd dddddddddddddd ddddddddddd ddddddddddddddddddddddddddddd','12:30 pm','dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd',55,'P'),(20,28,0,'1','30/03/2018','dd ddddd ddddddddddddddd dddddddddddddddddd dddddddddddddd ddddddddddd ddddddddddddddddddddddddddddd','12:30 pm','dd dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd',55,'P'),(21,1,2850,'3','04/04/2018','Mensaje','11:30 am','wert',50,'P');
/*!40000 ALTER TABLE `compra_pastel_predisenado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `decorado`
--

DROP TABLE IF EXISTS `decorado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `decorado` (
  `id_decorado` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_decorado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `decorado`
--

LOCK TABLES `decorado` WRITE;
/*!40000 ALTER TABLE `decorado` DISABLE KEYS */;
INSERT INTO `decorado` VALUES (1,'Adicionales'),(2,'Chocolates'),(3,'Flores'),(4,'Listones'),(5,'Peluches'),(6,'Velas');
/*!40000 ALTER TABLE `decorado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entidad`
--

DROP TABLE IF EXISTS `entidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entidad` (
  `cve_ent` int(11) NOT NULL,
  `nom_ent` varchar(45) DEFAULT NULL,
  `nom_abr` varchar(45) DEFAULT NULL,
  `id_pais` varchar(2) DEFAULT NULL,
  `num_reg` int(4) DEFAULT NULL,
  `id_estatus` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`cve_ent`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entidad`
--

LOCK TABLES `entidad` WRITE;
/*!40000 ALTER TABLE `entidad` DISABLE KEYS */;
INSERT INTO `entidad` VALUES (1,'Aguascalientes','Ags.','MX',-3,NULL),(2,'Baja California','BC','MX',0,NULL),(3,'Baja California Sur','BCS','MX',0,NULL),(4,'Campeche','Camp.','MX',0,NULL),(5,'Coahuila de Zaragoza','Coah.','MX',2,NULL),(6,'Colima','Col.','MX',0,NULL),(7,'Chiapas','Chis.','MX',-1,NULL),(8,'Chihuahua','Chih.','MX',0,NULL),(9,'Ciudad de México','DF','MX',18,NULL),(10,'Durango','Dgo.','MX',0,NULL),(11,'Guanajuato','Gto.','MX',2,NULL),(12,'Guerrero','Gro.','MX',0,NULL),(13,'Hidalgo','Hgo.','MX',0,NULL),(14,'Jalisco','Jal.','MX',3,NULL),(15,'Estado de México','Mex.','MX',1,NULL),(16,'Michoacán de Ocampo','Mich.','MX',0,NULL),(17,'Morelos','Mor.','MX',-1,NULL),(18,'Nayarit','Nay.','MX',0,NULL),(19,'Nuevo León','NL','MX',76,'1'),(20,'Oaxaca','Oax.','MX',0,NULL),(21,'Puebla','Pue.','MX',1,NULL),(22,'Querétaro','Qro.','MX',8,NULL),(23,'Quintana Roo','Q. Roo','MX',1,NULL),(24,'San Luis Potosí','SLP','MX',2,NULL),(25,'Sinaloa','Sin.','MX',0,NULL),(26,'Sonora','Son.','MX',0,NULL),(27,'Tabasco','Tab.','MX',0,NULL),(28,'Tamaulipas','Tamps.','MX',0,NULL),(29,'Tlaxcala','Tlax.','MX',0,NULL),(30,'Veracruz','Ver.','MX',1,NULL),(31,'Yucatán','Yuc.','MX',0,NULL),(32,'Zacatecas','Zac.','MX',0,NULL);
/*!40000 ALTER TABLE `entidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estatus_pedido`
--

DROP TABLE IF EXISTS `estatus_pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estatus_pedido` (
  `id_estatus` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_estatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estatus_pedido`
--

LOCK TABLES `estatus_pedido` WRITE;
/*!40000 ALTER TABLE `estatus_pedido` DISABLE KEYS */;
INSERT INTO `estatus_pedido` VALUES (1,'Capturado'),(2,'Confirmado'),(3,'Liquidado'),(4,'Pago parcial');
/*!40000 ALTER TABLE `estatus_pedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `extra`
--

DROP TABLE IF EXISTS `extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extra` (
  `id_extra` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(145) DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `costo` int(11) DEFAULT NULL,
  `id_unidad` int(11) DEFAULT NULL,
  `no_unidades` int(11) DEFAULT NULL,
  `imagen` varchar(150) DEFAULT NULL,
  `estatus` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_extra`),
  KEY `fk_id_tipo_extra_idx` (`id_tipo`),
  KEY `fk_id_unidad_idx` (`id_unidad`),
  CONSTRAINT `fk_id_tipo_extra` FOREIGN KEY (`id_tipo`) REFERENCES `tipo_extra` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_id_unidad` FOREIGN KEY (`id_unidad`) REFERENCES `unidad` (`id_unidad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extra`
--

LOCK TABLES `extra` WRITE;
/*!40000 ALTER TABLE `extra` DISABLE KEYS */;
INSERT INTO `extra` VALUES (1,'TRÉBOL 4.5CM',1,65,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_01.jpg','A'),(2,'ASTROMELÍA',2,120,1,50,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_01.jpg','A'),(3,'OBLEA IMPRESA ACETATO',3,80,3,1,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_02.jpg','A'),(4,'ROSA CHICA',1,35,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_02.jpg','A'),(5,'CASABLANCA',2,140,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_02.jpg','A'),(6,'Test',1,123,1,12,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_03.jpg','E'),(7,'ROSA CON HOJAS 2',1,49,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_03.jpg','E'),(8,'ACAPULCO',2,140,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_03.jpg','A'),(9,'ROSA CON HOJAS',1,49,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_03.jpg','A'),(10,'ALCATRAZ 5CM',1,35,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_04.jpg','A'),(11,'CEBOLLÍN 5CM',1,300,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_05.jpg','A'),(12,'ESTRELLA MINI',1,100,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_06.jpg','A'),(13,'SURTIDAS 2 A 5CM',1,100,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_07.jpg','A'),(14,'LISIANTHUS',1,20,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_08.jpg','A'),(15,'Estrella 5 cm',1,100,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_09.jpg','A'),(16,'TERESITA 15CM',1,120,3,120,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_10.jpg','A'),(17,'ONDULADA 15CM',1,150,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_11.jpg','A'),(18,'LILI CON HOJAS 15CM',1,180,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_11.jpg','A'),(19,'LILI 15CM',1,150,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_13.jpg','A'),(20,'ORQUÍDEA 15CM',1,180,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_14.jpg','A'),(21,'PEONÍA 25CM',1,250,3,250,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_15.jpg','A'),(22,'ORQUIDEA SINGAPUR 20CM',1,300,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_16.jpg','A'),(23,'ROSA GRANDE',1,75,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_17.jpg','A'),(24,'FLOR DE AGUAS 5CM',1,10,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_18.jpg','A'),(25,'CAMPANILLA CHINA',1,100,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_19.jpg','A'),(26,'MIMOSA',1,90,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_20.jpg','A'),(27,'JACINTO',1,120,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_21.jpg','A'),(28,'FLOR LÁTEX CHICA 5CM',1,45,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_22.jpg','A'),(29,'FLOR LÁTEX CHICA (5CM) CON BROCHE',1,120,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_23.jpg','A'),(30,'FLOR LÁTEX CHICA (5CM) CON BROCHE',1,130,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_24.jpg','A'),(31,'FLOR LÁTEX GRANDE 15CM',1,70,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_25.jpg','A'),(32,'LILI',2,90,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_04.jpg','A'),(33,'ROSA',2,80,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_05.jpg','A'),(34,'ROSAS',2,350,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_06.jpg','A'),(35,'GERBERA',2,70,3,70,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_07.jpg','A'),(36,'GIPSOFILA',2,200,6,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_08.jpg','A'),(37,'TULIPÁN',2,650,4,50,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_09.jpg','A'),(38,'ORQUÍDEA',2,520,3,20,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_10.jpg','A'),(39,'CORSAGE DE 3 FLORES',2,280,5,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_11.jpg','A'),(40,'CORSAGE DE 5 FLORES',2,380,5,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_12.jpg','A'),(41,'CORSAGE DE 7 FLORES',2,420,5,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_13.jpg','A'),(42,'CORSAGE DE 14 FLORES',2,480,5,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_14.jpg','A'),(43,'CORSAGE DE 24 FLORES',2,580,5,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-flores-naturales_15.jpg','A'),(44,'OBLEA COMESTIBLE',3,150,3,1,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_03.jpg','A'),(45,'AGREGAR ALTURA',3,80,3,3,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_04.jpg','A'),(46,'PERLA COMESTIBLE',3,1,3,40,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_05.jpg','A'),(47,'BROCHE BRILLANTE',3,91,3,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_06.jpg','A'),(48,'FRESAS CON CHOCOLATE',3,144,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_07.jpg','A'),(49,'PLUMA LARGA',3,230,3,15,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_08.jpg','A'),(50,'MARIPOSA SINTÉTICA',3,45,3,15,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_09.jpg','A'),(51,'ESTRELLA CON PIPETA',3,80,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_10.jpg','A'),(52,'CORSAGE ESTRELLAS Y CONCHAS',3,480,3,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_11.jpg','A'),(53,'CONCHAS Y CARACOLES FONDANT',3,350,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_12.jpg','A'),(54,'CONCHAS Y CARACOLES NATURALES',3,750,2,10,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-adicionales_13.jpg','A'),(55,'lisantus',3,80,3,100,'sharons-pasteleria-panaderia-cafeteria-acapulco-decorado-fondant_01.jpg','E');
/*!40000 ALTER TABLE `extra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forma`
--

DROP TABLE IF EXISTS `forma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `forma` (
  `id_forma` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_forma`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forma`
--

LOCK TABLES `forma` WRITE;
/*!40000 ALTER TABLE `forma` DISABLE KEYS */;
INSERT INTO `forma` VALUES (1,'Cuadrada'),(2,'Circular');
/*!40000 ALTER TABLE `forma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mensaje`
--

DROP TABLE IF EXISTS `mensaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mensaje` (
  `id_mensaje` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_mensaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mensaje`
--

LOCK TABLES `mensaje` WRITE;
/*!40000 ALTER TABLE `mensaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `mensaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pastel_predisenado`
--

DROP TABLE IF EXISTS `pastel_predisenado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pastel_predisenado` (
  `id_pastel_predisenado` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_pastel` int(11) DEFAULT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `precio_porcion` int(11) DEFAULT NULL,
  `adicional` int(11) DEFAULT NULL,
  `sabor` varchar(255) DEFAULT NULL,
  `observaciones` varchar(255) DEFAULT '',
  `imagen` varchar(155) DEFAULT NULL,
  `estatus` varchar(2) DEFAULT 'A',
  PRIMARY KEY (`id_pastel_predisenado`),
  KEY `fk_id_tipo_pastel_idx` (`id_tipo_pastel`),
  CONSTRAINT `fk_tipo_pastel_predisenado` FOREIGN KEY (`id_tipo_pastel`) REFERENCES `tipo_pastel` (`id_tipo_pastel`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pastel_predisenado`
--

LOCK TABLES `pastel_predisenado` WRITE;
/*!40000 ALTER TABLE `pastel_predisenado` DISABLE KEYS */;
INSERT INTO `pastel_predisenado` VALUES (1,1,'CHOCOLATE EN VERANO','Este pastel desnudo, tu y yo celebrando nuestro gran día',42,750,'Chocolate, Vainilla','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_01.jpg','A'),(2,1,'AMOR EN LA PLAYA','Consolidar nuestro matrimonio a la orilla del mar es tan delicioso como este pastel de chantilly',32,850,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.',' ','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_02.jpg','A'),(3,1,'LLUVIA FLORAL','Entra a la etapa mas bella de tu vida con este delicioso pastel de chantilly',32,740,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.',' ','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_03.jpg','A'),(4,1,'LOVE OF SEA (BX67)','En el mar la vida es mas sabrosa, pero con un pastel de chantilly lo es aun mas',32,1080,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_04.jpg','A'),(5,1,'LOVE DOOR','Atrévete a soñar en tu gran día con un rico pastel de chantilly decorado con flores naturales',32,790,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_05.jpg','A'),(6,1,'SUEÑOS','Endulza tu gran momento con este delicioso pastel de fondant',55,850,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_06.jpg','A'),(7,1,'ESTRELLA DE AMOR','No existe nada mejor que compartir un momento especial con un rico pastel de chantilly y con la persona que amas',32,880,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_07.jpg','A'),(8,1,'AMOR CRISTALIZADO (BX50)','Este gran instante permanecerá en nuestras mentes, compartamoslo con un exquisito pastel de chantilly',32,610,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_08.jpg','A'),(9,1,'SUEÑO DE PASIÓN','Lo único que quiero es compartir mi vida y este delicioso pastel de chantilly contigo',32,820,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_09.jpg','A'),(10,1,'AMOR EN FLORES','Tengo ganas de 3 cosas verte, abrazarte y de compartir contigo este delicioso pastel de chantilly',32,970,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_10.jpg','A'),(11,1,'MAREA DE AMOR','Yo prometo que en nuestro gran día, este sera nuestro pastel',32,1025,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_11.jpg','A'),(12,1,'SI ACEPTO','Lo nuestro fue tan inesperado que nos encontramos sin buscarnos, así como encontramos este delicioso pastel de chantilly perfecto para nuestro gran día',32,580,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_12.jpg','A'),(13,1,'ROSAS IMPERIALES (BX 11)','Nuestro amor no tiene por que ser perfecto, pero nuestro pastel de bodas si',32,1820,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','No incluye novios','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_13.jpg','A'),(14,1,'NOCHE GLAMUROSA','Celebra la noche mas glamurosa con este delicioso pastel de chantilly',32,1040,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_14.jpg','A'),(15,1,'PASEO DE AMOR','Pase lo que pase solo tengo ojos para ti y este delicioso pastel de chantilly',32,280,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_15.jpg','A'),(16,1,'VELO ROSA','Las mejores veladas se viven con un delicioso pastel de chantilly cerca del mar',32,520,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_16.jpg','A'),(17,1,'VELO AZUL','Las mejores veladas se viven con un delicioso  pastel de chantilly cerca del mar',32,520,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_17.jpg','A'),(18,1,'DULCES DESTELLOS','Este encantador pastel y yo, seremos las estrellas de esta noche',32,340,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_18.jpg','A'),(19,1,'DETALLES DE AMOR','Cada detalle de este gran día debe de ser tan perfecto como este delicioso pastel de crema batida',32,970,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_19.jpg','A'),(20,1,' VELADA CON MARIPOSAS ','Las mariposas en mi pastel de chantilly son el complemento perfecto para mi gran evento',32,770,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_20.jpg','A'),(21,1,'DESTELLO DE LUNA','Celebro el inicio de una gran etapa con este riquísimo pastel de chantilly en 4 pisos',32,990,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_21.jpg','A'),(22,1,'TERNURA DE ROSAS','Ven, que sera una fiesta inolvidable gracias a este delicioso pastel de chantilly en 3 pisos',32,800,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_22.jpg','A'),(23,1,'ELEGANCIA Y GLAMOUR','Mi suculento pastel de chantilly en 4 pisos, combina perfecto con la elegancia y glamour de mi evento',32,620,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_23.jpg','A'),(24,1,'NOCHE DE PASIÓN','Los detalles rojos de mi pastel de chantilly son la combinación perfecta de nuestra pasión',32,1300,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','No incluye  novios','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_24.jpg','A'),(25,1,'CRISTALES DE AMOR','En el amor debe haber respeto, pero no falso orgullo, cristalicemos nuestro  amor con este delicioso pastel de chantilly.',32,1500,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_25.jpg','A'),(26,1,'SUEÑO DE AMOR','Es una bendición encontrar a la pareja perfecta y el pastel de fondant perfecto para unir nuestras vidas',55,800,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_26.jpg','A'),(27,1,'RESPLANDOR PLATEADO','La historia mas bonita que el destino escribio en mi vida, tu y este delicioso pastel de chantilly',55,350,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_27.jpg','A'),(28,1,'JARDÍN DE SUEÑOS','En un jardín de sueños la vida te espera con los brazos abiertos para que la disfrutes al igual que este riquísimo pastel de chantilly',32,180,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_28.jpg','A'),(29,1,'ROMANCE ELEGANTE','No hay mal tiempo si es contigo y este delicioso pastel de chantilly',32,420,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_29.jpg','A'),(30,1,'DULCE ATARDECER','No soy egoísta pero a ti y a este exquisito pastel de chantilly no los comparto con nadie',32,690,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_30.jpg','A'),(31,1,'VIDA DE CARNAVAL','Vivamos juntos la grandiosa fiesta de carnaval con este delicioso pastel de chantilly',32,1170,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_31.jpg','A'),(32,1,'SUEÑOS DORADOS','De todas las maneras que hay para ser feliz la que mas me gusta es estar contigo compartiendo este rico pastel de chantilly',32,450,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_32.jpg','A'),(33,1,'ILUSIÓN DE AMOR','Necesitaba tener un momento especial  y te escogí a ti y este hermoso pastel de chantilly',32,1080,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_33.jpg','A'),(34,1,'DULCE Y  SUTIL DETALLE','La persona con la que quiero compartir mi vida se merece lo mejor y lo mejor es este exquisito pastel de chantilly',32,1350,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_34.jpg','A'),(35,1,'CASCADA AZUL','Para completar mi felicidad solo falta unir nuestras vidas con este hermoso pastel de chantilly',32,2820,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_35.jpg','A'),(36,1,'ETERNO AMOR','Mi corazón es perfecto por que tu estas dentro de el y este  pastel  de  chantilly es perfecto por que  simboliza nuestro amor',32,340,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_36.jpg','A'),(37,1,'AMOR DORADO','Este rico pastel de chantilly  iluminara tu evento  mas que las estrellas',32,720,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_37.jpg','A'),(38,1,'ROJO TENTACIÓN','La verdadera felicidad es disfrutar el presente junto a ti y este suculento pastel desnudo',42,980,'Chocolate, Vainilla','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_38.jpg','A'),(39,1,'BRILLOS DE AMOR','Esperaría cien vidas  para comerme este delicioso pastel de chantilly y caminar contigo',32,620,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_39.jpg','A'),(40,1,'JUNTOS POR SIEMPRE','Mereces a alguien que te regale sonrisas, tiempo, alegrías, besos y este delicado y exquisito  pastel de chantilly',32,3500,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_40.jpg','A'),(41,1,'BRISA DE AMOR','Una buena relación debe de tener dos cosas: un amor que no termine y este delicioso pastel de chantilly en su boda.',32,450,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_41.jpg','A'),(42,1,'AMOR PROFUNDO','Elige a la persona que haga mas bonito tu mundo y el pastel de chantiily de tus sueños',32,490,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_42.jpg','A'),(43,1,'SIEMPRE TE AMARE','En el sendero de la vida te encontré y nunca me arrepentiré de amarte así como no me arrepentiré de escoger este magnifico pastel de fondant para nuestra boda',55,1400,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','No incluye novios','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_43.jpg','A'),(44,1,'NOCHE DE ESTRELLAS','En esta etapa brillaras mas que una estrella si decides compartir en tu evento este delicioso pastel de fondant  ',55,480,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_44.jpg','A'),(45,1,'LINDO ATARDECER','Los atardeceres son mejor si los comparto contigo y con este delicioso pastel de chantilly',32,510,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_bodas_y_xv_predisenado_sharons_45.jpg','A'),(46,2,'KITTI PRIMAVERA','Delicioso pastel en fondant para la princesa de la casa',55,500,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_01.jpg','A'),(47,2,'BAUTIZO EN PETALOS','Pastel decorado en chantilly elaborado especialmente para mi bautizo en 3 pisos cubierto en pétalos de fondant con bellas flores no comestibles',32,780,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_02.jpg','A'),(48,2,'EL MEJOR REGALO','Riquísimo pastel de chantilly con un moño comestible elaborado para recibir a la reina del hogar',32,250,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_03.jpg','A'),(49,2,'EN HORA BUENA','Exquisito pastel en chantilly arribando un tren para la llegada de una gran bendición',32,440,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_04.jpg','A'),(50,2,'SUBLIME COMUNIÓN','Tarta decorada con flores comestibles para celebrar mi bautizo',32,280,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_05.jpg','A'),(51,2,'NIÑA O NIÑO','Rica tarta sorpresa para mi baby shower',55,280,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_06.jpg','A'),(52,2,'ZOOLÓGICO','Hermoso pastel en 3 pisos decorado con animalitos del zoológico para celebrar un año mas de tu pequeño',32,1000,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_07.jpg','A'),(53,2,'BABY BEAR','Dale la bienvenida al príncipe de la casa con este exquisito pastel de fondant',55,590,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_08.jpg','A'),(54,2,'DULCE LLEGADA','Celebra tu baby shower de una manera original con un pastel de chantilly decorado con fondant',32,850,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_09.jpg','A'),(55,2,'BIENVENIDA PRINCESA','Festeja la sublime llegada de una princesa con un pastel de crema chantilly con decoración de fondant',32,650,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_10.jpg','A'),(56,2,'DULCE SORPRESA','Comparte la emoción de saber que será con esta rica tarta de chantilly y corazones de fondant',32,260,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_11.jpg','A'),(57,2,'MINE CRAFT','Apetecible pastel de chantilly especial para celebrar tu gran día',32,980,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','No incluye vela de primera comunión, uvas ni espigas','previsualizacion_pastel_infantil_predisenado_sharons_12.jpg','A'),(58,2,'REGALO MICKEY','Celebremos mi gran día junto a mickey mouse con este exquisito pastel  de chantilly ',32,499,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','No incluye vela de mickey y minie','previsualizacion_pastel_infantil_predisenado_sharons_13.jpg','A'),(59,2,'FELICIDADES PRINCESA','Rico pastel de chantilly para festejar un año mas de tu princesa',32,100,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_14.jpg','A'),(60,2,'TWILIGHT SPARKLE','Apetitoso pastel de fondant para la gran ocasión',55,450,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_15.jpg','A'),(61,2,'AUTOPISTA CARS','Acompáñame a la gran carrera de cars en mi cumpleaños con este riquísimo pastel de fondant',55,350,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_16.jpg','A'),(62,2,'PARTY PEPPA','Vivamos una fiesta excepcional junto a peppa y este pastel elaborado en chantilly',32,410,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_17.jpg','A'),(63,2,'HADA PEPPA','Cumple tu deseo de cumpleaños junto a peppa con un pastel de chantilly decorado con fondant',32,250,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_18.jpg','A'),(64,2,'TORRE PAW PATROL','Celebrando mi cumpleaños con la patrulla canina y un rico pastel de chantilly en tres pisos',32,1220,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_19.jpg','A'),(65,2,'NOCHE DE HADAS','Delicado pastel en chantilly para un ocasión especial ',32,380,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','No incluye muñeca','previsualizacion_pastel_infantil_predisenado_sharons_20.jpg','A'),(66,2,'BABY PONY','Vivamos un mundo de fantasía con este delicioso pastel en dos pisos',32,440,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_21.jpg','A'),(67,2,'BAJO EL MAR','Con este rico pastel de chantilly en dos pisos vive una gran aventura bajo el mar',32,600,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_22.jpg','A'),(68,2,'CUENTO DE HADAS','Una gran princesa merece una gran fiesta celebra con este apetitoso pastel de crema batida en 3 pisos decorado con fondant',32,840,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_23.jpg','A'),(69,2,'AVENTURA CONGELADA','Vive una aventura en el hielo con frozen y este hermoso pastel de fondant en 3 pisos',55,650,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_24.jpg','A'),(70,2,'PARTY RAINBOW ROCKS','Vive una fiesta sensacional con rainbow rocks y este pastel elaborado en chantilly',32,550,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_25.jpg','A'),(71,2,'BLANCA NIEVES','Un gran día se celebra con un suculento pastel de chantilly',32,250,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_26.jpg','A'),(72,2,'EL GRAN VAQUERO','Con una aventura de vaqueros y este delicioso pastel de fondant, tu y yo viviremos un gran cumpleaños',55,550,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_27.jpg','A'),(73,2,'SUEÑO ENCANTADO','Mi princesa quedara mas que encantada con este riquísimo pastel de chantilly',32,480,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_28.jpg','A'),(74,2,'ELSA','Elsa y yo tendremos un sensacional día con este pastel de chantilly',32,80,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_29.jpg','A'),(75,2,'PARTY MINIONS','Tu fiesta sera la mejor con los minions y este pastel de fondant',55,900,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_30.jpg','A'),(76,2,'SIRENITA','Sumerge tus emociones en este delicioso pastel de chantilly',32,80,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_31.jpg','A'),(77,2,'BOB ESPONJA','Viviremos una gran aventura bajo el mar con bob esponja y este rico pastel de chantilly con decoración de fondant',32,1360,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_32.jpg','A'),(78,2,'MINION','Un pastel de chantilly en dos pisos de minion es lo que necesito para tener la mejor fiesta',32,610,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_33.jpg','A'),(79,2,'CONGRATS','Yo celebro mis triunfos con un pastel especial de fondant',55,450,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_34.jpg','A'),(80,2,'CIRCO','Mi fiesta sera la mejor función con este delicioso pastel de fondant',55,350,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_35.jpg','A'),(81,2,'PRINCESA MINIE','Celebra mi primer año de vida con un riquísimo pastel en 3 pisos de chantilly',32,490,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','No incluye vela de número','previsualizacion_pastel_infantil_predisenado_sharons_36.jpg','A'),(82,2,'AVENTURA CARS','Exquisito pastel de fondant especialmente elaborado para festejar mi cumpleaños',55,530,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_37.jpg','A'),(83,2,'BATMAN','Vive un cumpleaños excepcional junto al caballero negro y un delicioso pastel de fondant',55,550,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_38.jpg','A'),(84,2,'MASCARA TIKI','Riquísimo pastel de chantilly decorado con fondant, por que lo que se hace con gusto se disfruta sin culpa.',32,650,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_39.jpg','A'),(85,2,'PRINCESA SOFIA','Los niños son asombrosos, son divinos, son un regalo, ven y disfruta con ellos este adorable pastel de chantilly',32,120,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_40.jpg','A'),(86,2,'SUPERMAN','Con este delicioso pastel de chantilly conocerás el universo junto a superman',32,500,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_41.jpg','A'),(87,2,'HULK','Con este riquísimo pastel de fondant hasta hulk celebraría contigo',55,500,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_42.jpg','A'),(88,2,'TORRE SOFÍA','Yo consiento a mi princesa con un pastel de chantilly en dos pisos',32,570,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_43.jpg','A'),(89,2,'UNICORNIO','Vivamos una fiesta única con unicornios con este rico pastel de chantilly',32,450,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_44.jpg','A'),(90,2,'AVENTURA EN ALTA MAR (NN1)','Vive una aventura en alta mar con este delicioso pastel de fondant',55,450,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_45.jpg','A'),(91,2,'VESTIDO BLANCA NIEVES','Mi princesa y este delicioso pastel de chantilly serán la sensación de la fiesta',32,550,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_46.jpg','A'),(92,2,'PARTY POCOYO','Vive la mejor fiesta de tu vida junto a pocoyo y este pastel de fondant',55,650,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_47.jpg','A'),(93,2,'CARRERA CARS','Tu puedes llegar a la cima, celebra la victoria con este rico pastel de fondant',55,650,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','No incluye vela de cars ni copa','previsualizacion_pastel_infantil_predisenado_sharons_48.jpg','A'),(94,2,'DOCTORA JUGUETES','Mis papis me aman tanto que me consentirán con este delicioso pastel de chantilly',32,150,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','No incluye ningun muñeco','previsualizacion_pastel_infantil_predisenado_sharons_49.jpg','A'),(95,2,'LOGO ZELDA','Revive la magia de los videojuegos con este delicioso pastel de chantilly',32,280,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_infantil_predisenado_sharons_50.jpg','A'),(96,2,'BALLENA','En las profundidades del océano todo es posible, con este rico pastel de fondant',55,450,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_51.jpg','A'),(97,2,'TIBURÓN','No hay nada mas emocionante que las aventuras bajo el mar, disfrútala con un delicioso pastel de fondant',32,550,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_infantil_predisenado_sharons_52.jpg','A'),(98,3,'HERMOSA PRINCESA','Demuéstrale al mundo la maravillosa mujer que eres compartiendo en tu gran día este delicioso pastel de chantilly',32,590,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_01.jpg','A'),(99,3,'DULCE DETALLE','Que todos tus sueños y anhelos se hagan realidad con un dulce detalle como este delicado y rico  pastel de fondant',55,560,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_02.jpg','A'),(100,3,'FELIZ GRADUACIÓN','Un logro mas en tu vida merece ser festejado con un delicioso pastel de chantilly.',32,420,'Mocka, 3 leches chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_03.jpg','A'),(101,3,'TERNURA','Tu decides cuando sera la ocasión y este bello pastel de fondant la hará especial.',55,350,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_04.jpg','A'),(102,3,'MANANTIAL DE DESEOS','Tu decides cuando sera la ocasión y este bello pastel de fondant la hará especial.',32,460,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_05.jpg','A'),(103,3,'GELATINA FRUTAL','No te conformes con poco  lucha por que te mereces este delicioso pastel de chantilly',28,280,'Uva y leche, fresa y leche, limon y leche','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_06.jpg','A'),(104,3,'GRAN RECUERDO','Deseo recordar este gran momento con un delicioso pastel de chantilly',32,800,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_07.jpg','A'),(105,3,'BELLA ÉPOCA','Los mejores años de mi vida aun no han pasado y  los disfruto con este riquísimo pastel de fonfant',55,680,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_08.jpg','A'),(106,3,'PRECIOSO REGALO','La mejor parte de una fiesta son los regalos y mi preferido es este riquísimo pastel de chantilly',32,420,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_09.jpg','A'),(107,3,'PRIMAVERA','La primavera es mi estación favorita para compartir un colorido pastel de fondant.',55,650,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_10.jpg','A'),(108,3,'ESCUDO CHIVAS','Yo festejo los triunfos de mi equipo con un rico pastel de chantilly',32,250,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_11.jpg','A'),(109,3,'FASHION CAKE','Mi evento será aún mas elegante con este delicioso pastel de fondant',55,650,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_12.jpg','A'),(110,3,'VAQUITA','Los animales son seres vivos excepcionales, tanto como esta tarta de fondant con carita de vaquita',55,250,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_13.jpg','A'),(111,3,'SUEÑO DE ROSAS','La manera mas original de regalar flores es en un delicioso pastel de chantilly',32,250,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_14.jpg','A'),(112,3,'BUHO DEL BOSQUE (TO7)','Vive el mas sublime de los momentos con este hermoso pastel de fondant',55,450,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_15.jpg','A'),(113,3,'GRADUACIÓN AZUL','El gran día de mi graduación se celebra con un delicioso pastel de chantilly',32,250,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_16.jpg','A'),(114,3,'DOCTOR CAKE','Un doctor salva vidas, demuéstrale tu agradecimiento con este exquisito pastel de fondant',55,420,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_17.jpg','A'),(115,3,'GRADUACIÓN FIUSHA','El gran día de mi graduación se celebra con un delicioso pastel de chantilly',32,250,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_18.jpg','A'),(116,3,'CÉLULA','Delicioso pastel de chantilly con detalles de fondant que simulan una célula',32,180,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_19.jpg','A'),(117,3,'DULCE PIANO','Motiva a esa persona a seguir su  pasión con este delicioso pastel de fondant en forma de piano',55,250,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_20.jpg','A'),(118,3,'HOMERO','El buen sentido del humor nunca debe pasar de moda, regala un divertidisimo pero muy rico pastel de fonfant de homero',55,500,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_21.jpg','A'),(119,3,'CONSTRUYENDO RECUERDOS','Un arquitecto puede construir una casa, déjanos ayudarte a construirle una gran sorpresa con este delicioso pastel de chantilly en dos pisos',32,530,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_22.jpg','A'),(120,3,'EL ENCANTO DEL BÚHO','Una mujer maravillosa cumple sus metas, nosotros le celebramos a la nuestra con un delicioso pastel de chantilly',32,510,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_23.jpg','A'),(121,3,'FLORES DE GRADUACIÓN','Todo sacrificio tiene su recompensa, felicitalo con una deliciosa plancha de chantilly',32,480,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_24.jpg','A'),(122,3,'DULCES 17','No importa que tan difícil sea la etapa, nosotros la endulzamos con un delicioso pastel de chantilly en dos pisos',32,760,'Mocka, 3l chocolate, 3 leches vainilla, Oreo, Supremo.','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_25.jpg','A'),(123,3,'DULCE MELODÍA','La música es la vida emocional de la mayoría de la gente, comparte esa emoción con un exquisito pastel de fondant',55,250,'Mocka, Chocolate, New York, Oreo, Zarzamora, Supremo, Delicias','','previsualizacion_pastel_toda_ocasion_predisenado_sharons_26.jpg','A');
/*!40000 ALTER TABLE `pastel_predisenado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piso`
--

DROP TABLE IF EXISTS `piso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piso` (
  `id_piso` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `precio` int(4) DEFAULT NULL,
  PRIMARY KEY (`id_piso`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piso`
--

LOCK TABLES `piso` WRITE;
/*!40000 ALTER TABLE `piso` DISABLE KEYS */;
INSERT INTO `piso` VALUES (1,'Piso 1',80),(2,'Piso 2',160),(3,'Piso 3',240);
/*!40000 ALTER TABLE `piso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relleno`
--

DROP TABLE IF EXISTS `relleno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relleno` (
  `id_relleno` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `precio` int(4) DEFAULT NULL,
  PRIMARY KEY (`id_relleno`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relleno`
--

LOCK TABLES `relleno` WRITE;
/*!40000 ALTER TABLE `relleno` DISABLE KEYS */;
INSERT INTO `relleno` VALUES (1,'Sin Relleno',0),(2,'Almendra',8),(3,'Cerezas',5),(4,'Chispas de chocolate',5),(5,'Chocolate',5),(6,'Crema de cajeta',5),(7,'Crema de queso',8),(8,'Crema pastelera',5),(9,'De Linea',0),(10,'Durazno',5),(11,'Fresas',8),(12,'Frutas',8),(13,'Mermelada de chabacano',5),(14,'Mermelada de fresa',5),(15,'Nuez',10),(16,'Nutella',10);
/*!40000 ALTER TABLE `relleno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sabor`
--

DROP TABLE IF EXISTS `sabor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sabor` (
  `id_sabor` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `precio` int(4) DEFAULT NULL,
  PRIMARY KEY (`id_sabor`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sabor`
--

LOCK TABLES `sabor` WRITE;
/*!40000 ALTER TABLE `sabor` DISABLE KEYS */;
INSERT INTO `sabor` VALUES (1,'3 Leches',27),(2,'Cajeta 3 leches',35),(3,'Cajeta Vainilla',38),(4,'Capuchino',35),(5,'Chocolate 3 leches',31),(6,'Coco 3 leches',30),(7,'Cookies and cream',38),(8,'Crema de limon',35),(9,'Crema de whisky',38),(10,'Delicias',35),(11,'Doble chocolate',46),(12,'Domino',35),(13,'Elote',33),(14,'Mocka',28),(15,'New York',36),(16,'Piña colada',31),(17,'Queso brownie',38),(18,'Queso fresa',35),(19,'Queso Light  0% Azucar',42),(20,'Queso nutella',36),(21,'Queso oreo',37),(22,'Queso tortuga',37),(23,'Queso zarzamora',37),(24,'Rompope',35),(25,'Supremo',33),(26,'Tiramisu',32),(27,'Triple chocolate',48),(28,'Vainilla',25),(29,'Zanahoria',32);
/*!40000 ALTER TABLE `sabor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sucursales`
--

DROP TABLE IF EXISTS `sucursales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sucursales` (
  `id_sucursal` int(11) NOT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_sucursal`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sucursales`
--

LOCK TABLES `sucursales` WRITE;
/*!40000 ALTER TABLE `sucursales` DISABLE KEYS */;
INSERT INTO `sucursales` VALUES (1,'ACA - MAGALLANES      Martín Alonso Pinzón, Col. Magallanes No. 28, Local 6, CP 39670'),(2,'ACA - RENA               Boulevard Vicente Guerrero Saldaña, Manzana 1, Lote 3, Local B2, Cd. Renacimiento, CP 39715'),(3,'ACA - CONSTY          Av. Constituyentes No. 90, Interior B, Col. Vista Alegre, CP 39560'),(4,'ACA - COLOSIO          Cd. Luis Donaldo Colosio, Calle Obsidiana #1, CP 39907'),(5,'ACA - HAMACAS          Av. Costera Miguel Alemán 239, Col. Centro, CP 39300'),(6,'CDMX - SINATEL          Av. Sinatel No. 72, Ampliación Sinatel, CP 09470, Iztapalapa');
/*!40000 ALTER TABLE `sucursales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tamano`
--

DROP TABLE IF EXISTS `tamano`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tamano` (
  `id_tamano` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_tamano`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tamano`
--

LOCK TABLES `tamano` WRITE;
/*!40000 ALTER TABLE `tamano` DISABLE KEYS */;
INSERT INTO `tamano` VALUES (1,'10 a 20 personas'),(2,'30 a 40 personas'),(3,'50 o más personas');
/*!40000 ALTER TABLE `tamano` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `textura`
--

DROP TABLE IF EXISTS `textura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `textura` (
  `id_textura` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `precio` int(4) DEFAULT NULL,
  PRIMARY KEY (`id_textura`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `textura`
--

LOCK TABLES `textura` WRITE;
/*!40000 ALTER TABLE `textura` DISABLE KEYS */;
INSERT INTO `textura` VALUES (1,'Canasta',0),(2,'Rosetas',0),(3,'Garigoleado',0),(4,'Grecas',0),(5,'Liso',0),(6,'Perlas',0),(7,'Puntos',0),(8,'Rombos',0),(9,'Rombos con perlas',0);
/*!40000 ALTER TABLE `textura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_extra`
--

DROP TABLE IF EXISTS `tipo_extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_extra` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_extra`
--

LOCK TABLES `tipo_extra` WRITE;
/*!40000 ALTER TABLE `tipo_extra` DISABLE KEYS */;
INSERT INTO `tipo_extra` VALUES (1,'Flores Fondant'),(2,'Flores Naturales'),(3,'Adicionales');
/*!40000 ALTER TABLE `tipo_extra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_pastel`
--

DROP TABLE IF EXISTS `tipo_pastel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_pastel` (
  `id_tipo_pastel` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_pastel`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_pastel`
--

LOCK TABLES `tipo_pastel` WRITE;
/*!40000 ALTER TABLE `tipo_pastel` DISABLE KEYS */;
INSERT INTO `tipo_pastel` VALUES (1,'Pasteles de bodas y XV años'),(2,'Pasteles Infantiles'),(3,'Pasteles para toda ocasión');
/*!40000 ALTER TABLE `tipo_pastel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidad`
--

DROP TABLE IF EXISTS `unidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidad` (
  `id_unidad` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_unidad`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidad`
--

LOCK TABLES `unidad` WRITE;
/*!40000 ALTER TABLE `unidad` DISABLE KEYS */;
INSERT INTO `unidad` VALUES (1,'Decenas'),(2,'Docenas'),(3,'Piezas'),(4,'Paquetes'),(5,'Corsages'),(6,'Manojos');
/*!40000 ALTER TABLE `unidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `direccion` varchar(80) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-19 16:45:45
