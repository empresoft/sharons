package mx.sharons.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * Clase con implementacion basica para un DAO de SQL.
 * Created by Edmundo on 05/06/2015.
 */
public abstract class SQLDaoImpl {
    protected Statement  instruccion;
    protected Connection conexion;
    protected ResultSet rSet;

    public Statement conseguirInstruccion() throws SQLException {
        conexion = FabricaDeConexiones.conseguirConexion();
        return conexion.createStatement( ResultSet.TYPE_SCROLL_SENSITIVE,
                                         ResultSet.CONCUR_UPDATABLE );
    }

    public void cerrarRecursosDB() {
        FabricaDeConexiones.cerrar( rSet );
        FabricaDeConexiones.cerrar( instruccion );
        FabricaDeConexiones.cerrar( conexion );
    }
}
