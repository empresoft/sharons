package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.sharons.beans.Usuario;
import mx.sharons.dao.UsuarioDao;
import mx.sharons.mail.CorreoRestablecerPassword;
import mx.sharons.utils.Constantes;

/**
 * Servlet implementation class RegistrarUsuarioServlet
 */
public class RestablecerPasswordServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RestablecerPasswordServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("RegistrarUsuarioServlet");
		PrintWriter out = response.getWriter();
		
		String correo = request.getParameter("correo");
		
		UsuarioDao usuarioDao = new UsuarioDao();
		Usuario usuario = usuarioDao.buscarPorCorreo(correo);
		HttpSession session = request.getSession(true);
		
		if ( usuario != null & usuario.getEstatus().equals( Constantes.ACTIVO ) ){
			// enviar password a correo
			CorreoRestablecerPassword correoRestablecerPassword = new CorreoRestablecerPassword(usuario);
			
			if ( correoRestablecerPassword.enviarCorreo() ){
				out.print("ERROR_ENVIO_CORREO");	
			} else {
				out.print("EXITO");
			}
			
			out.print("EXITO");
		} else  {
			out.print("ERROR");
		}
	}

	
}
