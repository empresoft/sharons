package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.sharons.beans.CompraPastelArmado;
import mx.sharons.dao.CompraPastelArmadoDao;

public class GuardarPastelPredisenadoServlet extends HttpServlet  {
	
	private static final long serialVersionUID = 1L;

	public GuardarPastelPredisenadoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		System.out.println("Guardar Pastel");
		PrintWriter out = response.getWriter();
		int isSaved = 0;
		
		if ( session.getAttribute("pastelBean") != null ) {
			System.out.println("Guardando pastel");
			CompraPastelArmado pastelBean = (CompraPastelArmado) session.getAttribute("pastelBean");
			CompraPastelArmadoDao pastelDao = new CompraPastelArmadoDao();
			isSaved = pastelDao.guardar(pastelBean);
		}
		
		// redirigir a resumen pedido con js
		out.print(isSaved);
	}
	
}
