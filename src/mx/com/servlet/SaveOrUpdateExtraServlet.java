package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.sharons.beans.Extra;
import mx.sharons.beans.Tipo;
import mx.sharons.beans.Unidad;
import mx.sharons.dao.ExtraDao;

public class SaveOrUpdateExtraServlet extends HttpServlet  {
		
	private static final long serialVersionUID = -4564483965285635294L;

		public SaveOrUpdateExtraServlet() {
	        super();
	    }

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			System.out.println("SaveOrUpdateExtraServlet");
			PrintWriter out = response.getWriter();
			int isSaved = 0;
			
			Extra extra = new Extra();
			Tipo tipo = new Tipo();
			Unidad unidad = new Unidad();
			
			int idExtra= request.getParameter("idExtra") != null && !request.getParameter("idExtra").equals("") ? Integer.parseInt(request.getParameter("idExtra")):0;
			
			extra.setIdExtra(idExtra);
			extra.setTitulo(request.getParameter("titulo"));
			
			tipo.setIdTipo( Integer.parseInt( request.getParameter("idTipo")) );
			extra.setTipo(tipo);
			extra.setNoUnidades( Integer.parseInt( request.getParameter("noUnidades")) );
			
			unidad.setIdUnidad( Integer.parseInt( request.getParameter("idUnidad"))  );
			extra.setUnidad( unidad );
			 
			extra.setImagen( request.getParameter("imagen") );
			extra.setCosto( Integer.parseInt( request.getParameter("costo")) );
			
			ExtraDao extraDao = new ExtraDao();
			
			extraDao.saveOrUpdate(extra);
			
			out.print(isSaved);
		}
}
