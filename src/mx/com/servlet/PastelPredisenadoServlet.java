package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.sharons.beans.CompraPastelPredisenado;
import mx.sharons.dao.CompraPastelPredisenadoDao;
import mx.sharons.utils.Constantes;

public class PastelPredisenadoServlet extends HttpServlet  {
	
	private static final long serialVersionUID = 1L;
	
	public PastelPredisenadoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		PrintWriter out = response.getWriter();
		
		// Crear bean
		CompraPastelPredisenado compra = new CompraPastelPredisenado();
		compra.setIdPastelPredisenado(Integer.parseInt(request.getParameter("idPastelPredisenado")));
		compra.setNumPersonas( Integer.parseInt(request.getParameter("numPersonas")));
		compra.setMensaje(request.getParameter("mensaje"));
		compra.setIdSucursalEntrega(Integer.parseInt(request.getParameter("idSucursalEntrega")));
		compra.setSucursalEntrega(request.getParameter("descSucursalEntrega"));
		compra.setFechaEntrega(request.getParameter("descFechaEntrega"));
		compra.setHoraEntrega(request.getParameter("descListaHorario"));
		compra.setObservaciones(request.getParameter("observaciones"));
		compra.setPrecio(Double.parseDouble(request.getParameter("precioTotal")));
		compra.setImagen(request.getParameter("imgPastel"));
		
		// persisitir datos 
		CompraPastelPredisenadoDao compraDao = new CompraPastelPredisenadoDao();
		int idCompra = compraDao.guardar(compra, Constantes.PENDIENTE);
		
		if (idCompra > 0 ){
			// subir a sesion
			System.out.println("Se guardo una compra pendiente");
			compra.setIdCompraPastelPredisenado(idCompra);
			session.setAttribute("compraPPBean", compra);
			System.out.println("Se subió la compra a la sesión");
			// redirigir a resumen pedido con js
			out.print("Success");
		} else {
			out.print("Fail");
		}
		
	}
	
}
