package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.sharons.beans.CompraPastelArmado;
import mx.sharons.dao.CompraPastelArmadoDao;

public class BuscarPastelArmadoServlet extends HttpServlet  {
	
	private static final long serialVersionUID = 1L;

	public BuscarPastelArmadoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		System.out.println("Buscar Pastel Armado Servlet");
		PrintWriter out = response.getWriter();
		String idPedido = request.getParameter("idPedido");
		System.out.println("idPedido" + idPedido);
		
		CompraPastelArmadoDao dao = new CompraPastelArmadoDao();
		if ( idPedido != null ) {
			CompraPastelArmado compraBean = dao.buscarPorId(idPedido);
			session.setAttribute("compraBean", compraBean);
			idPedido = String.valueOf(compraBean.getIdCompraPastelArmado());
			//System.out.println("Se subió la compra a la sesión, con id: " + idPedido);
		} else {
			idPedido = "";
		}
		
		out.print(idPedido);
		
	}
	
	private List<String> getColoresCobertura(String coloresCobertura) {
		List<String> coloresPorPiso = Arrays.asList(coloresCobertura.split(","));
		return coloresPorPiso;
	}

	private int getIdFormaSafe(HttpServletRequest request, String idForma){
		int id = 0;
		if ( request.getParameter("idForma") != null ){
			id = Integer.parseInt(request.getParameter("idForma") );
		}
		return id;
	}
}
