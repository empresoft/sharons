package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.sharons.beans.CompraPastelArmado;
import mx.sharons.dao.CompraPastelArmadoDao;
import mx.sharons.mail.CorreoResumenPedido;
import mx.sharons.mail.EnviadorCorreos;

public class EnviarCorreoPedidoServlet extends HttpServlet  {
	
	private static final long serialVersionUID = 1L;

	public EnviarCorreoPedidoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		System.out.println("Enviar Correo Pedido Servlet");
		PrintWriter out = response.getWriter();
		int idCompra = 0;
		int result = 0;
		try {
			if ( session.getAttribute("pastelArmadoBean") != null ) {
				System.out.println("Se encontro objeto en sesión");
				CompraPastelArmado pastelArmadoBean = (CompraPastelArmado) session.getAttribute("pastelArmadoBean");
				CorreoResumenPedido correo =  new CorreoResumenPedido(pastelArmadoBean);
				
				List<String> correosDestino = new ArrayList<String>();
				correosDestino.add("ventas@sharons.mx");
				
				if (pastelArmadoBean.getCorreoCliente() != null && pastelArmadoBean.getCorreoCliente() != ""){
					correosDestino.add( pastelArmadoBean.getCorreoCliente());					
				}
				
				EnviadorCorreos.enviarEmail(correo.getSubject(), correo.getBody(), correosDestino );
				
				result = 1;
			} else {
				result = -1;
				System.out.println("No se encontró objeto en sesión");		
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Hubó un error al enviar el correo");
			result = -2;
		}
		// validar en jsp resultado
		out.print(result);
	}
	
}
