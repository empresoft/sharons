package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.sharons.beans.Usuario;
import mx.sharons.dao.UsuarioDao;
import mx.sharons.mail.CorreoUsuarioRegistro;
import mx.sharons.mail.EnviadorCorreos;
import mx.sharons.utils.Constantes;

/**
 * Servlet implementation class RegistrarUsuarioServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("RegistrarUsuarioServlet");
		PrintWriter out = response.getWriter();
		
		String correo = request.getParameter("correo");
		String password  = request.getParameter("password");
		
		UsuarioDao usuarioDao = new UsuarioDao();
		Usuario usuario = usuarioDao.login(correo, password);
		HttpSession session = request.getSession(true);
		
		if ( usuario != null & usuario.getEstatus().equals( Constantes.ACTIVO ) ){
			// sube usuario a la sesi�n
			session.setAttribute("usuarioBean", usuario);
			out.print("EXITO");
		} else  {
			out.print("ERROR");
		}
	}

	
}
