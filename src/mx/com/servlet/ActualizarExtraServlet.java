package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.sharons.beans.Extra;
import mx.sharons.beans.Tipo;
import mx.sharons.beans.Unidad;
import mx.sharons.dao.ExtraDao;

public class ActualizarExtraServlet extends HttpServlet  {
		
	private static final long serialVersionUID = 5350583254405424165L;

		public ActualizarExtraServlet() {
	        super();
	    }

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			System.out.println("GuardarExtraServlet");
			PrintWriter out = response.getWriter();
			int isSaved = 0;
			
			Extra extra = new Extra();
			Tipo tipo = new Tipo();
			Unidad unidad = new Unidad();
			
			extra.setTitulo(request.getParameter("titulo"));
			
			tipo.setIdTipo( Integer.parseInt( request.getParameter("idTipo")) );
			extra.setTipo(tipo);
			extra.setNoUnidades( Integer.parseInt( request.getParameter("noUnidades")) );
			
			unidad.setIdUnidad( Integer.parseInt( request.getParameter("idUnidad"))  );
			extra.setUnidad( unidad );
			 
			extra.setImagen( request.getParameter("imagen") );
			extra.setCosto( Integer.parseInt( request.getParameter("costo")) );
			
			ExtraDao extraDao = new ExtraDao();
			
			extraDao.saveOrUpdate(extra);
			
			out.print(isSaved);
		}
}
