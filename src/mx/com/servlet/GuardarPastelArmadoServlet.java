package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.sharons.beans.CompraPastelArmado;
import mx.sharons.dao.CompraPastelArmadoDao;

public class GuardarPastelArmadoServlet extends HttpServlet  {
	
	private static final long serialVersionUID = 1L;

	public GuardarPastelArmadoServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		System.out.println("Guardar Pastel Servlet");
		PrintWriter out = response.getWriter();
		int idCompra = 0;
		
		if ( session.getAttribute("pastelBean") != null ) {
			System.out.println("Se encontro objeto en sesión");
			CompraPastelArmado pastelBean = (CompraPastelArmado) session.getAttribute("pastelBean");
			CompraPastelArmadoDao pastelDao = new CompraPastelArmadoDao();
			//System.out.println("IdCompra5:" + idCompra);
			idCompra = pastelDao.guardar(pastelBean);
			//System.out.println("IdCompra4:" + idCompra);
		} else {
			System.out.println("No se encontró objeto en sesión");		
		}
		
		System.out.println("IdCompra Final:" + idCompra);
		// validar en jsp resultado
		// out.print(idCompra);
	}
	
	private List<String> getColoresCobertura(String coloresCobertura) {
		List<String> coloresPorPiso = Arrays.asList(coloresCobertura.split(","));
		return coloresPorPiso;
	}

	private int getIdFormaSafe(HttpServletRequest request, String idForma){
		int id = 0;
		if ( request.getParameter("idForma") != null ){
			id = Integer.parseInt(request.getParameter("idForma") );
		}
		return id;
	}
}
