package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.sharons.beans.CompraExtras;
import mx.sharons.beans.CompraPastelArmado;
import mx.sharons.dao.CompraExtrasDao;
import mx.sharons.dao.CompraPastelArmadoDao;
import mx.sharons.dao.ExtraDao;

import org.json.JSONArray;
import org.json.JSONObject;

public class ExtrasPastelServlet extends HttpServlet  {
	
	private static final long serialVersionUID = 1L;
	CompraExtrasDao compraDao = new CompraExtrasDao();
	
	public ExtrasPastelServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		PrintWriter out = response.getWriter();
		CompraPastelArmadoDao pastelDao = new CompraPastelArmadoDao();
		CompraPastelArmado compra = (CompraPastelArmado) session.getAttribute("pastelArmadoBean");
		
		if ( compra != null ) {
			String extrasJSON = request.getParameter("extras");
			ArrayList<CompraExtras> compraExtrasList = parseExtras(extrasJSON);
			
			compra.setExtras(compraExtrasList);
		    compra.setPrecioExtras(getTotalPrecioExtras(compraExtrasList));
		    compra.setResumenExtras(getDescripcion(compraExtrasList));
		    
		    // se tiene que persistir para generar el id único
			System.out.println("Guardando pastel...");
			
			int idCompra = pastelDao.guardar(compra);
			
			if ( idCompra > 0 ){
				compra.setIdCompraPastelArmado(idCompra);
				session.setAttribute("pastelArmadoBean", compra);
			} else {
				// No seguir con la compra
				session.setAttribute("pastelArmadoBean", null);
				// TO-DO: Manejar el error
			}
		} else {
			// TO-DO: Manejar el error 
		} 
		
		
		// redirigir a resumen pedido con js
		out.print("Success");
	}

	private String getDescripcion(ArrayList<CompraExtras> compraExtrasList) {
		StringBuffer descripcion = new StringBuffer();
		ExtraDao extraDao = new ExtraDao();
		String descExtra = "";
		
		for( CompraExtras compra: compraExtrasList ){
			descExtra = extraDao.getDescpripcionById(compra.getIdExtra());
			descripcion.append("-");			
			descripcion.append(compra.getCantidad());
			descripcion.append(" ");
			descripcion.append(compra.getDescUnidad());
			descripcion.append(" ");
			descripcion.append(descExtra);
			descripcion.append(". ");
			
		}
		
		return descripcion.toString();		
	}

	private double getTotalPrecioExtras( ArrayList<CompraExtras> compraExtrasList ) {
		return compraDao.calcularPrecioExtras(compraExtrasList);
	}
	
	private ArrayList<CompraExtras> parseExtras(String extrasJSON){
		JSONArray jsonArray = new JSONArray(extrasJSON);
		ArrayList<CompraExtras> compraExtrasList = new ArrayList<CompraExtras>();
		
	    for (int i=0; i<jsonArray.length();i++){
	        JSONObject obj = (JSONObject) jsonArray.get(i);

	        CompraExtras comprasExtras = new CompraExtras();
	        comprasExtras.setIdExtra(obj.getInt("idExtra"));
	        comprasExtras.setCantidad(obj.getDouble("cantidad"));
	        comprasExtras.setDescUnidad(obj.getString("descUnidad"));
	        comprasExtras.setColor(obj.getString("color"));
	        
	        compraExtrasList.add(comprasExtras);
	    }
	    
	    return compraExtrasList;
	}
	
	
	public int validateParameter( String val ){
		return val != null ? Integer.valueOf( val ): 0;
	}
}
