package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import mx.sharons.beans.CompraPastelArmado;
import mx.sharons.utils.Constantes;

public class ArmaPastelServlet extends HttpServlet  {
	
	private static final long serialVersionUID = 1L;
	
	
	public ArmaPastelServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		System.out.println("Arma Pastel");
		PrintWriter out = response.getWriter();
		
		int numPersonas = getIdFormaSafe(request,"num_personas");
		String descTamano = request.getParameter("desc_tamano");
		int idForma = getIdFormaSafe(request,"id_forma");
		String descForma = request.getParameter("desc_forma");
		int idPiso = getIdFormaSafe(request,"id_piso");
		String descPisos = request.getParameter("desc_pisos");
		int idCobertura = getIdFormaSafe(request,"id_cobertura");
		String descCobertura = request.getParameter("desc_cobertura");
		int idSabor = getIdFormaSafe(request,"id_sabor");
		String descSabor = request.getParameter("desc_sabor");
		int idRelleno = getIdFormaSafe(request,"id_relleno");
		String descRelleno = request.getParameter("desc_relleno");
		String mensaje = request.getParameter("mensaje");
		String observaciones = request.getParameter("observaciones");
		int idTextura = getIdFormaSafe(request,"id_textura");
		String descTextura = request.getParameter("desc_textura");
		int idSucursalEntrega = getIdFormaSafe(request,"id_sucursal_entrega");
		String descSucursalEntrega = request.getParameter("desc_sucursal_entrega");
		String descListaHorario = request.getParameter("desc_lista_horario");
		
		String idsColoresCobertura = request.getParameter("ids_colores_cobertura");
		List<String> idsColoresCoberturaPorPiso = getIdsColoresCobertura(idsColoresCobertura);
		String descColoresCobertura = request.getParameter("desc_colores_cobertura");
		List<String> coloresCoberturaPorPiso = getColoresCobertura(descColoresCobertura);
		
		String descFechaEntrega = request.getParameter("desc_fecha_entrega");
		String correoCliente = request.getParameter("correo_cliente");
		int precio = Integer.parseInt(request.getParameter("precio_total").trim());
		int precioExtras = 0;
		// Id's
		/*
		System.out.println("NumPersonas: " + numPersonas);
		System.out.println("idForma: " + idForma);
		System.out.println("idNiveles: " + idPiso);
		System.out.println("idCobertura: " + idCobertura);
		System.out.println("idSabor: " + idSabor);
		System.out.println("idRelleno: " + idRelleno);
		System.out.println("idTextura: " + idTextura);
		System.out.println("idSucrusalEntrega: " + idSucursalEntrega);
		System.out.println("idsColoresCobertura: " + idsColoresCobertura);
		System.out.println("mensaje: " + mensaje);
		System.out.println("observaciones: " + observaciones);
		*/
		System.out.println("Correo Cliente: " + correoCliente);
		// descripciones
		/*
		System.out.println("descForma: " + descForma);
		System.out.println("descTamano: " + descTamano);
		System.out.println("descNiveles: " + idPiso);
		System.out.println("descCobertura: " + descCobertura);
		System.out.println("descSabor: " + descSabor);
		System.out.println("descRelleno: " + descRelleno);
		System.out.println("descTextura: " + descTextura);
		System.out.println("idsColoresCoberturaPorPiso: " + idsColoresCoberturaPorPiso);
		System.out.println("coloresCoberturaPorPiso: " + coloresCoberturaPorPiso);
		System.out.println("descSucursalEntrega: " + descSucursalEntrega);
		System.out.println("descListaHorario: " + descListaHorario);
		System.out.println("descFechaEntrega: " + descFechaEntrega);
		System.out.println("mensaje: " + mensaje);
		*/
		// System.out.println("precioTotal: " + precioTotal);
		CompraPastelArmado pastel = new CompraPastelArmado();
		pastel.setNumPersonas(numPersonas);
		// TO-DO: Modificar por el tipo de pastel correcto 
		// 1 BODAS
		pastel.setIdTipoPastel(1);
		// Id's
		pastel.setIdForma(idForma);
		pastel.setIdPiso(idPiso);
		pastel.setIdCobertura(idCobertura);
		pastel.setIdSabor(idSabor);
		pastel.setIdRelleno(idRelleno);
		pastel.setMensaje(mensaje);
		pastel.setObservaciones(observaciones);
		pastel.setIdTextura(idTextura);
		pastel.setIdSucursalEntrega(idSucursalEntrega);
		// Descripciones
		pastel.setDescForma(descForma);
		pastel.setDescTamano(descTamano);
		pastel.setDescPisos(descPisos);
		pastel.setDescCobertura(descCobertura);
		pastel.setDescSabor(descSabor);
		pastel.setDescRelleno(descRelleno);
		pastel.setDescTextura(descTextura);
		pastel.setMensaje(mensaje);
		pastel.setColoresCobertura(coloresCoberturaPorPiso);
		pastel.setIdsColoresCobertura(idsColoresCoberturaPorPiso);
		pastel.setDescSucursalEntrega(descSucursalEntrega);
		pastel.setHorario(descListaHorario);
		pastel.setFechaEntrega(descFechaEntrega);
		pastel.setCorreoCliente(correoCliente);
		
		// Calcular precio
		pastel.setPrecioPastel(precio);
		pastel.setPrecioExtras(precioExtras);
		System.out.println("Subir pastel a la sesión");
		// subir a sesion
		session.setAttribute("pastelArmadoBean", pastel);
		
		// redirigir a resumen pedido con js
		out.print("Success");
	}
	
	private List<String> getIdsColoresCobertura(String idsColoresCobertura) {
		List<String> coloresPorPiso = Arrays.asList(idsColoresCobertura.split(","));
		return coloresPorPiso;
	}

	private List<String> getColoresCobertura(String coloresCobertura) {
		List<String> coloresPorPiso = Arrays.asList(coloresCobertura.split(","));
		return coloresPorPiso;
	}

	private int getIdFormaSafe(HttpServletRequest request, String idForma){
		int id = 0;
		if ( request.getParameter(idForma) != null ){
			id = Integer.parseInt(request.getParameter(idForma) );
		}
		return id;
	}
	
	private int calculaPrecio(){
		return Constantes.PRECIO_BASE_PASTEL_BODAS_XV;
	}
}
