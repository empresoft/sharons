package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.sharons.dao.ExtraDao;

public class EliminarExtraServlet extends HttpServlet  {
		
	private static final long serialVersionUID = -4564483965285635294L;

		public EliminarExtraServlet() {
	        super();
	    }

		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			System.out.println("EliminarExtraServlet");
			PrintWriter out = response.getWriter();
			int isSaved = 0;
			
			int idExtra= Integer.parseInt(request.getParameter("idExtra"));
			
			ExtraDao extraDao = new ExtraDao();
			
			extraDao.delete(idExtra);
			
			out.print(isSaved);
		}
}
