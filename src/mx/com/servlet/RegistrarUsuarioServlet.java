package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.sharons.beans.Usuario;
import mx.sharons.dao.UsuarioDao;
import mx.sharons.mail.CorreoUsuarioRegistro;
import mx.sharons.mail.EnviadorCorreos;

/**
 * Servlet implementation class RegistrarUsuarioServlet
 */
public class RegistrarUsuarioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public RegistrarUsuarioServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("RegistrarUsuarioServlet");
		PrintWriter out = response.getWriter();
		
		String nombre = request.getParameter("nombre");
		String apellidos = request.getParameter("apellidos");
		String telefono = request.getParameter("telefono");
		String direccion = request.getParameter("direccion");
		String correo = request.getParameter("correo");
		String password  = request.getParameter("password");
		
		Usuario usuarioBean = new Usuario();
		usuarioBean.setNombre(nombre);
		usuarioBean.setApellidos(apellidos);
		usuarioBean.setTelefono(telefono);
		usuarioBean.setDireccion(direccion);
		usuarioBean.setCorreo(correo);
		usuarioBean.setPassword(password);
		
		UsuarioDao usuarioDao = new UsuarioDao();
		int result = usuarioDao.guardar(usuarioBean);
		
		if ( result > 0 ){
			// Envio de correo de confirmación
			CorreoUsuarioRegistro correoUsuarioRegistro = new CorreoUsuarioRegistro(usuarioBean);
			if ( correoUsuarioRegistro.enviarCorreo() ){
				out.print("ERROR_ENVIO_CORREO");	
			} else {
				out.print("EXITO");
			}
		} else  {
			out.print("ERROR");
		}
	}

	
}
