package mx.com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.sharons.beans.PastelPredisenado;
import mx.sharons.dao.PastelPredisenadoDao;

/**
 * Servlet implementation class EditarPastelPredisenadoServlet
 */
public class EditarPastelPredisenadoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditarPastelPredisenadoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("EditarPastelPredisenadoServlet");
		PrintWriter out = response.getWriter();
		PastelPredisenadoDao pastelDao = new PastelPredisenadoDao();
		PastelPredisenado pastelPredisenado = new PastelPredisenado();
		
		// tomar valores de request y llenar el objeto a persistir
		try {
			pastelPredisenado.setIdPastelPredisenado( Integer.parseInt(request.getParameter("idPastel")) );
			pastelPredisenado.setNombre( request.getParameter("nombre"));
			pastelPredisenado.setDescripcion( request.getParameter("descripcion"));
			pastelPredisenado.setAdicional( Integer.parseInt(request.getParameter("precioBase")) );
			pastelPredisenado.setPrecioPorcion( Integer.parseInt(request.getParameter("precioPorcion")) );
			pastelPredisenado.setSabor( request.getParameter("sabores"));
			pastelPredisenado.setObservaciones( request.getParameter("observaciones"));
			pastelPredisenado.setImagen( request.getParameter("imagen"));
		
		} catch (NumberFormatException ne){
			System.out.println("Hubo un error al convertir un número en EditarPastelPredisenadoServlet");
		}
		
		int result = pastelDao.update(pastelPredisenado);
		
		out.print(result);		
	}

}