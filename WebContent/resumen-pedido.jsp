<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="mx.sharons.beans.CompraPastelArmado" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="mx.sharons.utils.PayUSignGenerator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="ie8 oldie">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Pastelerías Sharons</title>
<script src="js/jquery-1.9.0.min.js"></script>
<link href="css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/reticula.css" rel="stylesheet" type="text/css">
<link href="css/estilos.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" href="imagenes/icono.ico">

<script src="js/respond.min.js"></script>
       <!-- Menu -->
<link rel="stylesheet" href="css/responsive-nav.css">
<link rel="stylesheet" href="css/styles.css">
<script src="js/responsive-nav.js"></script>
<link rel="stylesheet" type="text/css" href="css/color.css" />
   
</head>
<body>
<!--facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--end facebook-->

<div class="gridContainer clearfix">
  <div id="header">
    <div id="logo"><a href="index.html"><img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_01.png" class="big">
    <img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_02.png" class="small"></a></div>
    
    <div id="menu">
  <nav class="nav-collapse">
  <ul>
  	<li ><a href="index.html">INICIO</a></li>
    <li><a href="cotizador.jsp">COTIZADOR</a></li>
    <li ><a href="buscar-pedido.jsp">CONSULTA</a></li>
    <li><a href="ayuda.html">AYUDA</a></li>
      </ul>
  </nav></div>
    
  </div><!--END HEADER-->
  
  <%
  	if ( session.getAttribute("pastelArmadoBean") != null ){
    		CompraPastelArmado compra = (CompraPastelArmado) session.getAttribute("pastelArmadoBean");
    		
    		List<String> coloresCobertura = compra.getColoresCobertura();
    		String descSucursalEntrega = compra.getDescSucursalEntrega();
    		
    		DecimalFormat formatter = new DecimalFormat("#,###");
    		double precio = compra.getPrecioPastel();
    		String precioStr = formatter.format(precio);
    		
    		double precioExtras = compra.getPrecioExtras();
    		String fechaEntrega = compra.getFechaEntrega();
    		String horaEntrega = compra.getHorario();
    		double precioTotal = precio + precioExtras;
    		String precioTotalStr =  formatter.format(precioTotal);
    		
    		String extrasDesc = compra.getDescExtras() != null && 
    				!compra.getDescExtras().equals("") ? compra.getDescExtras() : "Sin extras";
    				
    		//
  			String reference = "ARM-" + compra.getIdCompraPastelArmado();
  			PayUSignGenerator encryptor = new PayUSignGenerator(reference, precioTotal); 
  		
  %>
  <div id="titulo-centro"><span class="secciones">Resumén pedido #: 
  		<u><%= compra.getIdCompraPastelArmado() %></u></span>
  <hr><br></div>
  
  <div id="resumen">
    <div id="resumen-descripcion">
  
  	
  	<div class="map" id="resumen-titulo1">DESCRIPCIÓN</div>
	  <div id="resumen-descripcion-info">
	   <span class="pedido">
	    Forma de pastel: <b><%= compra.getDescForma() %></b><br><br>
	    Tamaño: <b><%= compra.getNumPersonas() %> personas</b><br><br>
	    Pisos: <b><%= compra.getDescPisos() %></b><br><br>
	    Sabor: <b><%= compra.getDescSabor() %></b><br><br>
	    Relleno: <b><%=  compra.getDescRelleno() %></b><br><br>
	    Cobertura: <b><%= compra.getDescCobertura() %></b><br><br>
	    Textura: <b><%= compra.getDescTextura() %></b><br><br>
	    <%
	    	for ( int i = 0; i < coloresCobertura.size(); i++ ) {
	    	%>
		    Color piso <%= i+1 %>: <b><%= coloresCobertura.get(i) %></b><br><br>
	    	<%
	    	}
	    %>
	    Mensaje: <b><%= compra.getMensaje() %></b>
	    </span>
  	  </div>
    </div>
    
    <div id="resumen-entrega">
    <div class="map" id="resumen-titulo2">ENTREGA</div>
    
    <div id="resumen-entrega-info">
    <span class="pedido">
     Sucursal: <b><%= descSucursalEntrega %></b><br><br>
     Día: <b><%= fechaEntrega %></b><br><br>
     Hora:  <b><%= horaEntrega %></b><br><br>
     Observaciones:  <b><%= compra.getObservaciones() %></b><br><br>
     Correo Cliente:  <b><%= compra.getCorreoCliente() %></b><br><br>
     </span>
    </div>
    
    </div>

    <div id="resumen-precio">
     <div class="map" id="resumen-titulo2">EXTRAS</div>
     <span class="pedido"><b><%= extrasDesc %></b></span><br><br>
	</div>    
    
    <div id="resumen-precio-info">
	    <div id="resumen-extras"><span class="map">PRECIO</span><br></div>
	    
	    <div id="extras-precio"><span class="nombre">$<%= precioStr %> pesos </span></div>
	    <div id="resumen-extras"><span class="map">EXTRAS</span><br></div>
	    
	    <div id="extras-precio"><span class="nombre">$<%= precioExtras %> pesos </span></div>
	    
	    <div class="map" id="resumen-total">TOTAL</div>
	    <div id="total-precio"><span class="titulos">$<%= precioTotalStr %> pesos </span></div>
        <br><br>
        <!-- 
		<button id="pagoEfectivo"  class="button" > Pago Efectivo</button>
		<button id="pagoParcial"  class="button" > Liquidación Parcial</button>
		<button id="pago TDC"  class="button" > Pago Tarjeta</button>
         -->
		<button id="enviarCorreo"  class="button" > Enviar Correo</button>
		<span id="mensajeInfo" class="mensaje_info"></span>		
        <span id="mensajeExito" class="mensaje_exito"></span>
	 	<span id="mensajeError" class="error_requerido"></span>
	 	
	</div>
	
    </div>
    <br>
  
  <%
  	} else {
  		%>
  		<div id="titulo-centro">
  		<p class="error_requerido">Se venció el tiempo de su sesión.</p>
  		</div>
  		<%
  	}
  %>    
  </div><!--END RESUMEN-->
  
  
<div class="pie" id="footer">
  <hr color="#e28f26">
<span class="map">inicio</span> | <span class="map">pastelería</span> | <span class="map">cafetería</span> | <span class="map">eventos</span> | <span class="map">contacto</span><br>
Todos los derechos reservados Sharon's ® <br>Sitio web desarrollado por Ome Diseño y Empresoft
</div>

</div>

    <!-- Menu -->
<script>
      var navigation = responsiveNav(".nav-collapse", {
        animate: true,                    // Boolean: Use CSS3 transitions, true or false
        transition: 284,                  // Integer: Speed of the transition, in milliseconds
        label: "Menu",                    // String: Label for the navigation toggle
        insert: "after",                  // String: Insert the toggle before or after the navigation
        customToggle: "",                 // Selector: Specify the ID of a custom toggle
        closeOnNavClick: false,           // Boolean: Close the navigation when one of the links are clicked
        openPos: "relative",              // String: Position of the opened nav, relative or static
        navClass: "nav-collapse",         // String: Default CSS class. If changed, you need to edit the CSS too!
        navActiveClass: "js-nav-active",  // String: Class that is added to <html> element when nav is active
        jsClass: "js",                    // String: 'JS enabled' class which is added to <html> element
        init: function(){},               // Function: Init callback
        open: function(){},               // Function: Open callback
        close: function(){}               // Function: Close callback
      });
    </script>
    
     <!--EFFECT -->
 <script>
			// For Demo purposes only (show hover effect on mobile devices)
			[].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
				el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			} );

$('#guardarPastel').click ( 
	function() {
		var precio = 450;
		var extras = 110;
		var total = precio + extras;
	    $.ajax({
	        type: "POST",
	        url: "GuardarPastelArmadoServlet",
	        data: {
	        	total:total
	        },
	        success: function(data){
	        	var counter = 0;
	        	counter++;
	        	console.log('Counter:' + counter +  'Data' + data);
	        	if (data > 0 ){
	        		alert("Guardado");
	        	} else {
	        		alert("Fallo al guardar");
	        	}
	        },
	        done: function() {
	        	console.log("Don");
	        }
	    });
	}
);

$('#enviarCorreo').click ( 
		function() {
			console.log("Llamar Resumen Pedido Servlet");
			$('#mensajeInfo').text("Enviando correo ...");
			
		    $.ajax({
		        type: "POST",
		        url: "EnviarCorreoPedidoServlet",
		        success: function(response){
		        	$('#mensajeInfo').text("");
		        	console.log('Response code: ' + response);
		        	if ( response == 1) {
		        		console.log("Correo enviado");
		        		$('#mensajeInfo').text("");
		        		$('#mensajeExito').text("El correo se envió exitosamente.");
		        	} else if ( response == -1) {
		        		console.log("No hay datos en sesión");
		        		$('#mensajeError').text("Error al enviar el correo: no hay datos en sesión.");
		        	} else if ( response == -2) {
		        		console.log("Error con los datos de envió");
		        		$('#mensajeError').text("Error al enviar el correo: error con los datos de envió.");
		        	}
		        },
		        done: function() {
		        	console.log("Done");
		        }
		    });			
		}
);
</script>
        
        
</body>
</html>
