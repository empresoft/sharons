<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="mx.sharons.dao.PastelPredisenadoDao" %>
<%@ page import="mx.sharons.beans.PastelPredisenado" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Formulario Bodas y XV</title>
<link href="css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/reticula.css" rel="stylesheet" type="text/css">
<link href="css/estilos.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" href="imagenes/icono.ico">

<script src="js/respond.min.js"></script>

<!-- number with commas -->
<script src="js/precios-util.js"></script>
<!--COLOR-->
<script src="js/jquery-1.9.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/color.css" />
<script src="js/jquery.dd.js"></script>
   
       <!-- Menu -->
<link rel="stylesheet" href="css/responsive-nav.css">
<link rel="stylesheet" href="css/styles.css">
<script src="js/responsive-nav.js"></script>
<script> $("body select").msDropDown();</script>

<style>
#lista_color {
	width: 5%;
}
#previsualizacion_pastel { text-align:center; max-height: 672px;}
form {
	clear: both;
	float: left;
	margin-left: 0%;
	width: 100%;
	display: block;
}

@media only screen and (min-width: 481px) {
form {
	margin-left: 10.204%;
	width: 79.5918%;
}
.previsualizacion_pastel { text-align:center;}
}
.error_requerido {
	color: red;
}
</style>
   
</head>
<body>
<!--facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--end facebook-->

<div class="gridContainer clearfix">

<a href="login.html"><div id="sesion">
<span class="icon-head"></span>
</div></a> 

<a href="resumen-pedido.html"><div id="bag">
 <span class="icon-bag"></span></div></a>
 
  <div id="header">
    <div id="logo"><a href="index.html"><img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_01.png" class="big">
    <img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_02.png" class="small"></a></div>
    
    <div id="menu">
  <nav class="nav-collapse">
  <ul>
       <li ><a href="index.html">INICIO</a></li>
       <li><a href="pasteleria.html">PASTELERÍA</a></li>
       <li ><a href="cafeteria.html">CAFETERÍA</a></li>
       <li><a href="contacto.html">CONTACTO</a></li>
      </ul>
  </nav></div>
  </div><!--END HEADER-->
  <div id="titulo-centro"><span class="secciones">Edición de datos pastel</span><br>
    <span class="elige">prediseñado</span><br>
  <hr></div>

<%
if ( request.getParameter("idPastel") != null ) { 
	PastelPredisenadoDao pastelPredisenadoDao = new PastelPredisenadoDao();
	int idPastel = Integer.parseInt(request.getParameter("idPastel"));
	PastelPredisenado pastel = pastelPredisenadoDao.buscarPorId(idPastel);
	
	if ( pastel.getNombre() != null ) {
%>
 <form action="" method="post">
  <div id="form_arma_tu_pastel">
 <span id="idPastelPredisenado" hidden><%= idPastel %></span>
  <br><span class="titulos">Nombre</span>
  <input id="nombre" type="text" value="<%= pastel.getNombre() %>">
  <br>
 <span class="predis">Descripción</span><br>
 <textarea id="descripcion" rows="2" class="mensaje" maxlength="255">
 <%= pastel.getDescripcion() %>
 </textarea>
 <br><br>
 
<!-- Sabores -->
<span class="opciones">Sabores</span><br>
  <textarea id="sabores" rows="2" class="mensaje" maxlength="255">
  <%= pastel.getSabor() %>
  </textarea>
  <br>
    
  <!-- Observaciones -->
  <br><span class="opciones">Observaciones</span><br>
  <textarea id="observaciones" rows="2" class="mensaje" maxlength="255">
  <%= pastel.getObservaciones() %>
  </textarea>
  <br>
  
  <br><span class="titulos">Imagen</span><br>
  <input id="imagen" type="text" value="<%= pastel.getImagen() %>">
  </div>
  
  <div id="previsualizacion">
  
  <div id="precio_total">
	<span class="total">Precio base </span>
	$ <input id="precio_base" type="number" value="<%= pastel.getAdicional() %>">		
	<br>
	<span class="total">Precio porción </span>
	$ <input id="precio_porcion" type="number" value="<%= pastel.getPrecioPorcion() %>">
	<br>
	<input type="button" class="button" id="botonEditar" value="editar">		
  </div>
  
  <div id="error_continuar" align="right" class="error_requerido"></div>
      
  <div id="previsualizacion_pastel">
  <img src="imagenes/predisenados/<%= pastel.getImagen() %>">
  </div><!--Termina previsualizacion-->
  </div>
    </form>
  <% } else { %>
  	<p> No hay pasteles con ese ID </p>
  <% } %>
  
  <% } else { %>
  	<p> No se encontro un ID de pastel para buscar </p>
  <% } %>
  
<div id="footer">
<hr color="#e28f26">
<span class="map"><a href="index.html">inicio</a></span> | <span class="map"><a href="pasteleria.html">pastelería</a></span> | <span class="map"><a href="cafeteria.html">cafetería</a></span> | <span class="map"><a href="contacto.html"> contacto</a></span><br><span class="pie">
Todos los derechos reservados Sharon’s ® Sitio web desarrollado por <a href="http://omedg.com">Ome Diseño</a> y Empresoft <br><span style="font-weight:300"><a href="aviso-privacidad.html">Aviso de privacidad</a></span></span></div>

</div>

  
    <!-- Menu -->
<script>
      var navigation = responsiveNav(".nav-collapse", {
        animate: true,                    // Boolean: Use CSS3 transitions, true or false
        transition: 284,                  // Integer: Speed of the transition, in milliseconds
        label: "Menu",                    // String: Label for the navigation toggle
        insert: "after",                  // String: Insert the toggle before or after the navigation
        customToggle: "",                 // Selector: Specify the ID of a custom toggle
        closeOnNavClick: false,           // Boolean: Close the navigation when one of the links are clicked
        openPos: "relative",              // String: Position of the opened nav, relative or static
        navClass: "nav-collapse",         // String: Default CSS class. If changed, you need to edit the CSS too!
        navActiveClass: "js-nav-active",  // String: Class that is added to <html> element when nav is active
        jsClass: "js",                    // String: 'JS enabled' class which is added to <html> element
        init: function(){},               // Function: Init callback
        open: function(){},               // Function: Open callback
        close: function(){}               // Function: Close callback
      });
    </script>
    
<script language="javascript">
	
$(document).ready(function(e) {
	try {
		$("body select").msDropDown();
	} catch(e) {
	alert(e.message);
	}
});

$('#botonEditar').click(
	function(){
		// obtener valores del formulario
		var idPastel = $('#idPastelPredisenado').text().trim();
		var nombre = $('#nombre').val().trim();
		var descripcion = $('#descripcion').val().trim();
		var precioBase = $('#precio_base').val().trim();
		var precioPorcion = $('#precio_porcion').val().trim();
		var sabores = $('#sabores').val().trim();
		var observaciones = $('#observaciones').val().trim();
		var imagen = $('#imagen').val().trim();
		
		console.log('Id pastel: '+ idPastel);
		console.log('Nombre: '+ nombre);
		console.log('Descripción: '+ descripcion);
		console.log('Precio base: '+ precioBase);
		console.log('Precio porcion: '+ precioPorcion);
		console.log('Sabores: '+ sabores);
		console.log('Observaciones: '+ observaciones);
		console.log('Imagen: '+ imagen);
		
		callEditarServlet(idPastel, nombre, descripcion, precioBase, precioPorcion, sabores, observaciones, imagen);
	}
);

function callEditarServlet(idPastel, nombre, descripcion, precioBase, precioPorcion, sabores, observaciones, imagen){
	// AJAX call to Servlet
	$.ajax({
	    type: "POST",
	    url: "EditarPastelPredisenadoServlet",
	    data: {
	    	idPastel: idPastel,
	    	nombre: nombre,
	    	descripcion: descripcion,
	    	precioBase: precioBase,
	    	precioPorcion: precioPorcion,
	    	sabores: sabores,
	    	observaciones: observaciones,
	    	imagen: imagen
	    },
	    success: function(data){
	    	if (data == 1){
	    		alert("Editado con éxito");
	    	} else {
	    		alert("Hubó un error al editar el registro");
	    	}
	    	location.reload();	    	
	    },
	    done: function() {
	    	console.log("Done");
	    }                
	});	
}
</script>
</body>
</html>