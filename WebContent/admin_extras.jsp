<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="mx.sharons.html.ExtraHtml"  %>
    <%@ page import="mx.sharons.html.UnidadesHtml"  %>
    <%@ page import="mx.sharons.html.TipoExtrasHtml"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="ie8 oldie">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Pastelerías Sharons</title>
<link href="css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/reticula.css" rel="stylesheet" type="text/css">
<link href="css/estilos.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="imagenes/icono.ico">

<script src="js/respond.min.js"></script>

<!-- MENU -->
<link rel="stylesheet" href="css/responsive-nav.css">
<link rel="stylesheet" href="css/styles.css">
<script src="js/responsive-nav.js"></script>

<!-- DECORADO -->
<link rel="stylesheet" type="text/css" href="css/component-tabs.css" />
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<link rel="stylesheet" type="text/css" href="css/color_extras.css" />
<style>
select {
	display: inline-block;
    height: 35px;
    padding: 4px 8px;
    margin-bottom: 10px;
    margin-top: 10px;
    margin-left: 0px;
    line-height: 20pt;
    vertical-align: middle;
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    border: solid 1px #e39513;
    font-family: 'Montserrat', Arial, Helvetica, sans-serif;
    outline: none;
    font-size: 12pt;
    font-weight: 300;
    color: #763523;
}
</style>
</head>
<body>
<div class="gridContainer clearfix">
<a href="login.html"><div id="sesion">
<span class="icon-head"></span>
</div></a> 

<a href="resumen-pedido.html"><div id="bag">
 <span class="icon-bag"></span></div></a>
 
  <div id="header">
    <div id="logo"><a href="index.html"><img src="../imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_01.png" class="big">
    <img src="../imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_02.png" class="small"></a></div>
    
    <div id="menu">
  <nav class="nav-collapse">
  <ul>
       <li ><a href="#">INICIO</a></li>
       <li><a href="pasteleria.html">EXTRAS</a></li>
       <li ><a href="cafeteria.html">CATALOGOS</a></li>
      </ul>
  </nav></div>
    
  </div><!--END HEADER-->
  
  <div id="titulo-centro"><span class="secciones">Alta de extras</span><br></div>
  
  <div id="decorado-content" class="content" style="text-align: left; !important">
	<hr>
    <span class="opciones">Captura los datos del extra. Todos los campos son requeridos.</span>
    
    <br>
    <br>
    <!-- Id extra -->
    <input id="id_extra" placeholder="id" type="number" disabled>
    
    <br>
    <!-- Titulo -->
  	<input id="titulo" placeholder="Título" type="text">
  	
  	<br>	
  	<!-- Costo -->
  	<input id="costo" type="number" placeholder="Costo">
  	
  	<br>
  	<!-- Tipos -->
  	<%= TipoExtrasHtml.getSelectTipos()  %>
			
	<br>
	<!-- Número de unidades -->
  	<input id="no_unidades" type="number" placeholder="Número de unidades">
  	
  	
  	<br>
  	<!-- Unidades -->
	<%= UnidadesHtml.getSelectUnidades() %>
	
	<br>
	<!-- Imagen -->
	<input id="imagen" placeholder="Imagen" type="text">
	
	<br><br>
	<div id="msjTexto" class="mensaje_error"></div>
	<br>
	<span class="button" id="btn_guardar">Guardar<span class="icon-chevron-right"></span></span>   		
	</div><!-- /tabs -->
  </div>

  <div id="titulo-centro"><span class="secciones">Modificación y eliminación</span><br>
</div>
  
  <div id="decorado-content" class="content">
  <div id="tabs" class="tabs">
		<nav>
			<ul>
		<li><a href="#section-1" class="icon-cake"><span>Flores Fondant</span></a></li>
		<li><a href="#section-2" class="icon-cog"><span>Flores Naturales</span></a></li>
		<li><a href="#section-3" class="icon-gift"><span>Adicionales</span></a></li>
					</ul>
				</nav>
				<div class="content">
                
<!--SECCION 1-->               
<section id="section-1">
<div id="decorado-title"><span class="subtitulo"><span
     style="color:#e39513">Agrega</span> flores de fondant o látex <span
     style="color:#e39513">a tu pastel</span></span></div>
  
	<%= ExtraHtml.getListaFloresFondant() %>        
</section>
    
<!--SECCION 2-->              
<section id="section-2">
	<div id="decorado-title"><span class="subtitulo"><span
     style="color:#e39513">Agrega</span> flores naturales <span
     style="color:#e39513">a tu pastel</span></span></div>
     
    <%= ExtraHtml.getListaFloresNaturales() %>    
</section>
                    
                    
	<section id="section-3"><!--SECCION 3-->
<div id="decorado-title"><span class="subtitulo"><span
     style="color:#e39513">Productos</span> adicionales <span
     style="color:#e39513">para tu pastel</span></span></div>
     <%= ExtraHtml.getListaAdicionales() %>                    
</section>

<div id="footer">
<hr color="#e28f26">
<span class="map"><a href="index.html">inicio</a></span> | <span class="map"><a href="pasteleria.html">pastelería</a></span> | <span class="map"><a href="cafeteria.html">cafetería</a></span> | <span class="map"><a href="contacto.html"> contacto</a></span><br><span class="pie">
Todos los derechos reservados Sharon’s ® Sitio web desarrollado por <a href="http://omedg.com">Ome Diseño</a> y Empresoft <br><span style="font-weight:300"><a href="aviso-privacidad.html">Aviso de privacidad</a></span></span></div>

</div>

<script src="js/cbpFWTabs.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<script src="js/admin_extras.js"></script>
        
</body>
</html>
