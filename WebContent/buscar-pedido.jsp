<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="mx.sharons.beans.CompraPastelArmado" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="mx.sharons.utils.PayUSignGenerator" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="ie8 oldie">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Pastelerías Sharons</title>
<script src="js/jquery-1.9.0.min.js"></script>
<link href="css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/reticula.css" rel="stylesheet" type="text/css">
<link href="css/estilos.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" href="imagenes/icono.ico">

<script src="js/respond.min.js"></script>
       <!-- Menu -->
<link rel="stylesheet" href="css/responsive-nav.css">
<link rel="stylesheet" href="css/styles.css">
<script src="js/responsive-nav.js"></script>
<link rel="stylesheet" type="text/css" href="css/color.css" />
   
</head>
<body>
<!--facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--end facebook-->

<div class="gridContainer clearfix">
  <div id="header">
    <div id="logo"><a href="index.html"><img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_01.png" class="big">
    <img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_02.png" class="small"></a></div>
    
    <div id="menu">
  <nav class="nav-collapse">
  <ul>
  	<li ><a href="index.html">INICIO</a></li>
    <li><a href="cotizador.jsp">COTIZADOR</a></li>
    <li ><a href="buscar-pedido.jsp">CONSULTA</a></li>
    <li><a href="ayuda.html">AYUDA</a></li>
      </ul>
  </nav></div>
    
  </div><!--END HEADER-->
  
  <%
  	CompraPastelArmado compra  = new CompraPastelArmado();
 	List<String> coloresCobertura = Arrays.asList("");
 	String descSucursalEntrega = "";
 	DecimalFormat formatter = new DecimalFormat("#,###");
 	int precio = 0;
 	String precioStr = "";
 	int precioExtras = 0; 
 	String fechaEntrega = "";
 	String horaEntrega = "";
 	int precioTotal = 0;
 	String precioTotalStr = "";
 	String extrasDesc = "";
 	
  	if ( session.getAttribute("compraBean") != null ){
    	compra = (CompraPastelArmado) session.getAttribute("compraBean");
    		
    	coloresCobertura = compra.getColoresCobertura();
    	descSucursalEntrega = compra.getDescSucursalEntrega();
    		
    		
    	precio = compra.getPrecioPastel();
    	precioStr = formatter.format(precio);
    		
    	precioExtras = compra.getPrecioExtras();
    	fechaEntrega = compra.getFechaEntrega();
    	horaEntrega = compra.getHorario();
    	precioTotal = precio + precioExtras;
    	precioTotalStr =  formatter.format(precioTotal);
    		
    	extrasDesc = compra.getDescExtras() != null && 
    				!compra.getDescExtras().equals("") ? compra.getDescExtras() : "Sin extras";
    				
  	} 
  		
  %>
  <div id="titulo-centro"><span class="secciones">Número de pedido: </span>
  <input name="num_pedido" id="id_pedido" type="number" required  min="1" max="3000"  value="<%= compra.getIdCompraPastelArmado() %>"  maxlength="3"> 
  
  <button id="buscar_pedido" class="button" > Buscar</button>
  <hr><br></div>
  
  <div id="resumen">
    <div id="resumen-descripcion">
  
  	
  	<div class="map" id="resumen-titulo1">DESCRIPCIÓN</div>
	  <div id="resumen-descripcion-info">
	   <span class="pedido">
	    Forma de pastel: <b><%= compra.getDescForma() != null ? compra.getDescForma() : "" %></b><br><br>
	    Tamaño: <b><%= compra.getNumPersonas() > 0 ? compra.getNumPersonas() + " personas" : "" %> </b><br><br>
	    Pisos: <b><%= compra.getIdPiso() > 0 ? compra.getIdPiso() : ""%></b><br><br>
	    Sabor: <b><%= compra.getDescSabor() != null ? compra.getDescSabor() : ""%></b><br><br>
	    Relleno: <b><%=  compra.getDescRelleno() != null ? compra.getDescRelleno() : ""%></b><br><br>
	    Cobertura: <b><%= compra.getDescCobertura() != null ? compra.getDescCobertura() : ""%></b><br><br>
	    Textura: <b><%= compra.getDescTextura() != null ? compra.getDescTextura() : ""%></b><br><br>
	    <%
	    	for ( int i = 0; i < coloresCobertura.size(); i++ ) {
	    	%>
		    Color piso <%= i+1 %>: <b><%= coloresCobertura.get(i) %></b><br><br>
	    	<%
	    	}
	    %>
	    Mensaje: <b><%= compra.getMensaje() != null ? compra.getMensaje() : ""%></b>
	    </span>
  	  </div>
    </div>
    
    <div id="resumen-entrega">
    <div class="map" id="resumen-titulo2">ENTREGA</div>
    
    <div id="resumen-entrega-info">
    <span class="pedido">
     Sucursal: <b><%= descSucursalEntrega %></b><br><br>
     Día: <b><%= fechaEntrega %></b><br><br>
     Hora:  <b><%= horaEntrega %></b><br><br>
     Observaciones:  <b><%= compra.getObservaciones() != null ? compra.getObservaciones() : ""%></b><br><br>
     </span>
    </div>
    </div>
    
    <div id="resumen-precio">
     <div class="map" id="resumen-titulo2">EXTRAS</div>
     <span class="pedido"><b><%= extrasDesc %></b></span><br><br>
	</div>    
    
    <div id="resumen-precio-info">
	    <div id="resumen-extras"><span class="map">PRECIO</span><br></div>
	    
	    <div id="extras-precio"><span class="nombre">$<%= precioStr %> pesos </span></div>
	    <div id="resumen-extras"><span class="map">EXTRAS</span><br></div>
	    
	    <div id="extras-precio"><span class="nombre">$<%= precioExtras %> pesos </span></div>
	    
	    <div class="map" id="resumen-total">TOTAL</div>
	    <div id="total-precio"><span class="titulos">$<%= precioTotalStr %> pesos </span></div>
	</div>

    <br>
  
  </div><!--END RESUMEN-->
  
  
<div class="pie" id="footer">
  <hr color="#e28f26">
<span class="map">inicio</span> | <span class="map">pastelería</span> | <span class="map">cafetería</span> | <span class="map">eventos</span> | <span class="map">contacto</span><br>
Todos los derechos reservados Sharon's ® <br>Sitio web desarrollado por Ome Diseño y Empresoft
</div>

</div>

    <!-- Menu -->
<script>
      var navigation = responsiveNav(".nav-collapse", {
        animate: true,                    // Boolean: Use CSS3 transitions, true or false
        transition: 284,                  // Integer: Speed of the transition, in milliseconds
        label: "Menu",                    // String: Label for the navigation toggle
        insert: "after",                  // String: Insert the toggle before or after the navigation
        customToggle: "",                 // Selector: Specify the ID of a custom toggle
        closeOnNavClick: false,           // Boolean: Close the navigation when one of the links are clicked
        openPos: "relative",              // String: Position of the opened nav, relative or static
        navClass: "nav-collapse",         // String: Default CSS class. If changed, you need to edit the CSS too!
        navActiveClass: "js-nav-active",  // String: Class that is added to <html> element when nav is active
        jsClass: "js",                    // String: 'JS enabled' class which is added to <html> element
        init: function(){},               // Function: Init callback
        open: function(){},               // Function: Open callback
        close: function(){}               // Function: Close callback
      });
    </script>
    
     <!--EFFECT -->
 <script>
			// For Demo purposes only (show hover effect on mobile devices)
			[].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
				el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			} );

$('#buscar_pedido').click ( 
	function() {
		var idPedido  = $("#id_pedido").val();
		if (idPedido && idPedido > 0 ) {
			console.log('idPedido:' + idPedido);
		    $.ajax({
		        type: "POST",
		        url: "BuscarPastelArmadoServlet",
		        data: {
		        	idPedido:idPedido
		        },
		        success: function(data){
		        	if (data > 0 ){
		        		alert("Encontrado");
		        		location.reload();
		        	} else {
		        		alert("Fallo al guardar");
		        	}
		        },
		        done: function() {
		        	console.log("Don");
		        }
		    });
		} else {
			alert('Capturar "Número de Pedido"');
		}
	}
);
			
</script>
        
        
</body>
</html>
