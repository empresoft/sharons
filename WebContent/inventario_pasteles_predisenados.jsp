<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="mx.sharons.dao.PastelPredisenadoDao" %>
<%@ page import="mx.sharons.dao.TipoPastelDao" %>
<%@ page import="mx.sharons.beans.PastelPredisenado" %>
<%@ page import="java.util.ArrayList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Compra Pasteles Prediseñados</title>

<link rel="shortcut icon" href="imagenes/icono.ico">
<link href="css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/reticula.css" rel="stylesheet" type="text/css">
<link href="css/estilos.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">

<script src="js/respond.min.js"></script>
   
<!-- Menu -->
<link rel="stylesheet" href="css/responsive-nav.css">
<link rel="stylesheet" href="css/styles.css">
<script src="js/responsive-nav.js"></script>
</head>

<body>
<div class="gridContainer clearfix">
    <a href="login.html">
		<div id="sesion"><span class="icon-head"></span>
		</div>
	</a> 

 	<a href="resumen-pedido.html">
		<div id="bag"><span class="icon-bag"></span>
		</div>
 	</a>
 
  <div id="header">
    <div id="logo"><a href="index.html"><img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_01.png" class="big">
    <img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_02.png" class="small"></a></div>
    
    <div id="menu">
  <nav class="nav-collapse">
  <ul>
       <li ><a href="index.html">INICIO</a></li>
       <li><a href="pasteleria.html">PASTELERÍA</a></li>
       <li ><a href="cafeteria.html">CAFETERÍA</a></li>
       <li><a href="contacto.html">CONTACTO</a></li>
      </ul>
  </nav></div>
  </div><!--END HEADER-->
  
  <%
  // TO-DO Extract to a constant class
  int ID_TIPO_BAUTIZO_DESPEDIDAS = 1;
  int ID_TIPO_BODAS_XV = 2;
  int ID_TIPO_CUMPLEANOS = 3;
  int ID_TIPO_INFANTIL = 4;
  int ID_TODA_OCASION = 5;
  
  int idTipoPastel = ID_TIPO_BODAS_XV; // Bodas por default
  String descripcionTipoPastel = "";
  
  if ( request.getParameter("idTipoPastel") != null ){
	  idTipoPastel = Integer.parseInt( request.getParameter("idTipoPastel") );
	  TipoPastelDao tipoPastelDao = new TipoPastelDao();
	  descripcionTipoPastel = tipoPastelDao.getDescPorIdTipo(idTipoPastel);
  }  
  
  
  %>
  <div id="titulo-centro"><span class="secciones"><%= descripcionTipoPastel %></span><br>
    <span class="elige">selecciona uno de nuestros diseños y personalízalo</span>
    <hr></div>
    
	  <!--INICIA PASTEL-->
	  <%
	  PastelPredisenadoDao pastelPredisenadoDao = new PastelPredisenadoDao();	  
	   
	  ArrayList<PastelPredisenado> pastelesEncontrados = pastelPredisenadoDao.buscarTodosPorTipo(idTipoPastel);
	  int i = 1;
	  for ( PastelPredisenado pastel:  pastelesEncontrados) {
		  
	  %>
	  <div id="pastel<%= i %>">
		  <a href="compra_pasteles_predisenados.jsp?idPastel=<%= pastel.getIdPastelPredisenado() %>">
		  <img src="imagenes/predisenados/thumb/<%= pastel.getImagen() %>">
		  </a>
		  <br><br>
		  <div id="boton-seleccion" align="center">
		  <a href="compra_pasteles_predisenados.jsp?idPastel=<%= pastel.getIdPastelPredisenado() %>">
		    <input name="submit" type="submit" class="botonseleccionar" id="boton_comprar" value="comprar"></a>
		  </div>
	  </div>
	  <%
	  i++;
	  } %>
	  <!--TERMINA PASTEL-->
</div>
 
<div id="footer">
<hr color="#e28f26">
<span class="map"><a href="index.html">inicio</a></span> | <span class="map"><a href="pasteleria.html">pastelería</a></span> | <span class="map"><a href="cafeteria.html">cafetería</a></span> | <span class="map"><a href="contacto.html"> contacto</a></span><br><span class="pie">
Todos los derechos reservados Sharon’s ® Sitio web desarrollado por <a href="http://omedg.com">Ome Diseño</a> y Empresoft <br><span style="font-weight:300"><a href="aviso-privacidad.html">Aviso de privacidad</a></span></span></div>

</body>
  
    <!-- Menu -->
<script>
      var navigation = responsiveNav(".nav-collapse", {
        animate: true,                    // Boolean: Use CSS3 transitions, true or false
        transition: 284,                  // Integer: Speed of the transition, in milliseconds
        label: "Menu",                    // String: Label for the navigation toggle
        insert: "after",                  // String: Insert the toggle before or after the navigation
        customToggle: "",                 // Selector: Specify the ID of a custom toggle
        closeOnNavClick: false,           // Boolean: Close the navigation when one of the links are clicked
        openPos: "relative",              // String: Position of the opened nav, relative or static
        navClass: "nav-collapse",         // String: Default CSS class. If changed, you need to edit the CSS too!
        navActiveClass: "js-nav-active",  // String: Class that is added to <html> element when nav is active
        jsClass: "js",                    // String: 'JS enabled' class which is added to <html> element
        init: function(){},               // Function: Init callback
        open: function(){},               // Function: Open callback
        close: function(){}               // Function: Close callback
      });
    </script>
    
     <!--EFFECT -->
  <script>
			// For Demo purposes only (show hover effect on mobile devices)
			[].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
				el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			} );
		</script>
        
</html>