<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="mx.sharons.html.ExtraHtml"  %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="mx.sharons.beans.CompraPastelArmado" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="ie8 oldie">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Pastelerías Sharons</title>
<link href="css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/reticula.css" rel="stylesheet" type="text/css">
<link href="css/estilos.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" href="imagenes/icono.ico">
<script src="js/precios-util.js"></script>
<script src="js/respond.min.js"></script>

<!-- MENU -->
<link rel="stylesheet" href="css/responsive-nav.css">
<link rel="stylesheet" href="css/styles.css">
<script src="js/responsive-nav.js"></script>

<!-- DECORADO -->
<link rel="stylesheet" type="text/css" href="css/component-tabs.css" />
<!-- 
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 -->
<link rel="stylesheet" type="text/css" href="css/color_extras.css" />

</head>
<body>
<div class="gridContainer clearfix">
<a href="login.html"><div id="sesion">
<span class="icon-head"></span>
</div></a> 

<a href="resumen-pedido.html"><div id="bag">
 <span class="icon-bag"></span></div></a>
 
  <div id="header">
	 <div id="logo"><a href="index.html"><img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_01.png" class="big">
	 <img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_02.png" class="small"></a></div>
	    
	 <div id="menu">
		 <nav class="nav-collapse">
		  <ul>
		  	<li ><a href="index.html">INICIO</a></li>
		    <li><a href="cotizador.jsp">COTIZADOR</a></li>
		    <li ><a href="buscar-pedido.jsp">CONSULTA</a></li>
		    <li><a href="ayuda.html">AYUDA</a></li>
		  </ul>
		  </nav>
	  </div>
    
  </div><!--END HEADER-->
  
  <div id="titulo-centro"><span class="secciones">Elige el decorado de tu pastel</span><br>
</div>
  
  <div id="decorado-content" class="content">
    <%
    	if ( session.getAttribute("pastelArmadoBean") != null ){
      		CompraPastelArmado pastel = (CompraPastelArmado) session.getAttribute("pastelArmadoBean");
      		DecimalFormat formatter = new DecimalFormat("#,###");
      		String precio = formatter.format(pastel.getPrecioPastel());
    %>
  	
  	<div id="precio_total">
	<span class="total">Precio Pastel:</span> 
	<span class="precio_total">$</span>
	<span id="precioTotal" class="precio_total">
	<%= precio %>
	</span>
	<span class="button" id="boton_continuar">continuar<span class="icon-chevron-right"></span></span>   
  <div id="tabs" class="tabs">
		<nav>
			<ul>
			<li><a href="#section-1" class="icon-cake"><span>Flores Fondant</span></a></li>
			<li><a href="#section-2" class="icon-cog"><span>Flores Naturales</span></a></li>
			<li><a href="#section-3" class="icon-gift"><span>Adicionales</span></a></li>
			</ul>
		</nav>
	<div class="content">
                
<!--SECCION 1-->               
<section id="section-1">
<div id="decorado-title"><span class="subtitulo"><span
     style="color:#e39513">Agrega</span> flores de fondant o látex <span
     style="color:#e39513">a tu pastel</span></span></div>
  
	<%= ExtraHtml.getListaFloresFondantConUnidades() %>        
</section>
    
<!--SECCION 2-->              
<section id="section-2">
	<div id="decorado-title"><span class="subtitulo"><span
     style="color:#e39513">Agrega</span> flores naturales <span
     style="color:#e39513">a tu pastel</span></span></div>
     
    <%= ExtraHtml.getListaFloresNaturalesConUnidades() %>    
</section>
                    
                    
	<section id="section-3"><!--SECCION 3-->
<div id="decorado-title"><span class="subtitulo"><span
     style="color:#e39513">Productos</span> adicionales <span
     style="color:#e39513">para tu pastel</span></span></div>
     <%= ExtraHtml.getListaAdicionalesConUnidades() %>                    
</section>
				</div><!-- /content -->
			</div><!-- /tabs -->
  </div>
<% } else {%>
	<p class="error_requerido">Se venció el tiempo de su sesión.</p>
<% } %>

<div id="footer">
<hr color="#e28f26">
<span class="map"><a href="index.html">inicio</a></span> | <span class="map"><a href="pasteleria.html">pastelería</a></span> | <span class="map"><a href="cafeteria.html">cafetería</a></span> | <span class="map"><a href="contacto.html"> contacto</a></span><br><span class="pie">
Todos los derechos reservados Sharon’s ® Sitio web desarrollado por <a href="http://omedg.com">Ome Diseño</a> y Empresoft <br><span style="font-weight:300"><a href="aviso-privacidad.html">Aviso de privacidad</a></span></span></div>

</div>

<!--DECORADO -->
<script src="js/cbpFWTabs.js"></script>
<script src="js/jquery-1.9.0.min.js"></script>
<script>
new CBPFWTabs( document.getElementById( 'tabs' ) );
</script>

<script>
$('#boton_continuar').click ( 
	    function() {
	    	var extraList = [];
	    	console.log("+ Extras +");
			for ( i = 1; i <= 100; i++ ){ // TO-DO: Cambiar por un límite definido de acuerdo a la bd.
				if ( $("#idExtra_" + i ).length != 0 && $("#idExtra_" + i )[0].selectedIndex > 0) {
					var extra = new Object();
					var cantidad =  $("#idExtra_" + i ).val(); // valor select
					var color = $("#idColor_" + i ).val();
					var descUnidad = $("#idExtra_" + i + " option:first").text();
					
					// create Json object
					extra.idExtra = i;
					extra.cantidad = cantidad;
					extra.descUnidad = descUnidad;
					extra.color = color;
					
					// add the extra to the list 
					extraList.push(extra);
				}
			}

			var extraListJson = JSON.stringify(extraList);	
			console.log( extraListJson );
	    	
			var precioExtras = removeCommas($('#precioExtras').text());
			
	        $.ajax({
	            type: "POST",
	            url: "ExtrasPastelServlet",
	            data: {
					extras: extraListJson,
					precioExtras: precioExtras
	            },
	            success: function(data){
	            	window.location.href = "resumen-pedido.jsp"; 
	            },
	            done: function() {
	            	console.log("Done");
	            }                
	        });
		}
	);

</script>
        
</body>
</html>
