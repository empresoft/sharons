<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="mx.sharons.beans.CompraPastelPredisenado" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.DecimalFormat" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="ie8 oldie">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Compra exitosa</title>
<script src="js/jquery-1.9.0.min.js"></script>
<link href="css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/reticula.css" rel="stylesheet" type="text/css">
<link href="css/estilos.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" href="imagenes/icono.ico">

<script src="js/respond.min.js"></script>
       <!-- Menu -->
<link rel="stylesheet" href="css/responsive-nav.css">
<link rel="stylesheet" href="css/styles.css">
<script src="js/responsive-nav.js"></script>
   
</head>
<body>
<!--facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--end facebook-->

<div class="gridContainer clearfix">
  <div id="header">
    <div id="logo"><a href="index.html"><img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_01.png" class="big">
    <img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_02.png" class="small"></a></div>
    
    <div id="menu">
  <nav class="nav-collapse">
  <ul>
       <li ><a href="#">INICIO</a></li>
       <li><a href="pasteleria.html">PASTELERÍA</a></li>
       <li ><a href="cafeteria.html">CAFETERÍA</a></li>
       <li ><a href="eventos.html">EVENTOS</a></li>
       <li><a href="contacto.html">CONTACTO</a></li>
      </ul>
  </nav></div>
    
  </div><!--END HEADER-->
  
  <div id="titulo-centro"><span class="secciones" style="color:green;">Compra exitosa</span><br>
  <hr><br></div>
  
  <div id="resumen">
  <%
  	if ( session.getAttribute("compraPPBean") != null ){
  		CompraPastelPredisenado compra = (CompraPastelPredisenado) session.getAttribute("compraPPBean");
  		DecimalFormat formatter = new DecimalFormat("#,###");
  		String precio = formatter.format(compra.getPrecio());
  %>
    <div id="resumen-imagen"><img src="imagenes/predisenados/<%= compra.getImagen() %>"></div>
    <div id="resumen-descripcion">
  <div class="map" id="resumen-titulo1">DESCRIPCIÓN</div>
	  <div id="resumen-descripcion-info">
	  <span class="pedido">
	    Número de personas: <b><%= compra.getNumPersonas() %> </b>
	    <br><br>
	    Mensaje: <br><b><%= compra.getMensaje() %></b>
	    <br><br>
	    Observaciones: <br><b><%= compra.getObservaciones() %></b>
	    </span>
  	  </div>
    </div>
    
    <div id="resumen-entrega">
    <div class="map" id="resumen-titulo2">ENTREGA</div>
    <div id="resumen-entrega-info">
     <span class="pedido">Sucursal: <br><b><%= compra.getSucursalEntrega() %></b></span>
     <br><br>
     <span class="pedido">Día:</span> <span class="texto"><b><%= compra.getFechaEntrega() %></b>
     <br><br>
     <span class="pedido">Hora:</span> <b><%= compra.getHoraEntrega() %></b></span></div>
    </div>

    <div id="resumen-precio">
    <div class="map" id="resumen-titulo3">PRECIO</div>
    <div id="resumen-precio-info"> <span class="nombre">$<%= precio %> pesos </span></div>
    <br>
    
    </div>
  <%
  	} else {
  		%>
  		<p>Se venció el tiempo de tu sesión.</p>
  		</div>
  		<%
  	}
  %>    
  </div><!--END RESUMEN-->
  
  
<div class="pie" id="footer">
  <hr color="#e28f26">
<span class="map">inicio</span> | <span class="map">pastelería</span> | <span class="map">cafetería</span> | <span class="map">eventos</span> | <span class="map">contacto</span><br>
Todos los derechos reservados Sharon's ® <br>Sitio web desarrollado por Ome Diseño y Empresoft
</div>

</div>

    <!-- Menu -->
<script>
      var navigation = responsiveNav(".nav-collapse", {
        animate: true,                    // Boolean: Use CSS3 transitions, true or false
        transition: 284,                  // Integer: Speed of the transition, in milliseconds
        label: "Menu",                    // String: Label for the navigation toggle
        insert: "after",                  // String: Insert the toggle before or after the navigation
        customToggle: "",                 // Selector: Specify the ID of a custom toggle
        closeOnNavClick: false,           // Boolean: Close the navigation when one of the links are clicked
        openPos: "relative",              // String: Position of the opened nav, relative or static
        navClass: "nav-collapse",         // String: Default CSS class. If changed, you need to edit the CSS too!
        navActiveClass: "js-nav-active",  // String: Class that is added to <html> element when nav is active
        jsClass: "js",                    // String: 'JS enabled' class which is added to <html> element
        init: function(){},               // Function: Init callback
        open: function(){},               // Function: Open callback
        close: function(){}               // Function: Close callback
      });
    </script>
    
     <!--EFFECT -->
 <script>
			// For Demo purposes only (show hover effect on mobile devices)
			[].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
				el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			} );

$('#guardarPastel').click ( 
	function() {
		var precio = 450;
		var extras = 110;
		var total = precio + extras;
	    $.ajax({
	        type: "POST",
	        url: "PastelPredisenadoServlet",
	        data: {
	        	total:total
	        },
	        success: function(data){
	        	if (data > 0 ){
	        		alert("Guardado");
	        	} else {
	        		alert("Fallo al guardar");
	        	}
	        },
	        done: function() {
	        	console.log("Don");
	        }
	    });
	}
);
			
</script>
        
        
</body>
</html>
