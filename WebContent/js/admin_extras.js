new CBPFWTabs( document.getElementById( 'tabs' ) );

$('#btn_guardar').click(
	function(){
		var idExtra = $('#id_extra').val();
		var titulo = $('#titulo').val();
		var idTipo = $('#idTipo')[0].selectedIndex;
		var noUnidades = $('#no_unidades').val();
		var idUnidad = $('#idUnidad')[0].selectedIndex;
		var imagen = $('#imagen').val();
		var costo = $('#costo').val();
		
		if ( validarDatos(idExtra, titulo, idTipo, noUnidades, idUnidad, imagen, costo) ) {
			callSaveOrUpdateExtra(idExtra, titulo, idTipo, noUnidades, idUnidad, imagen, costo);	 
		} else {
			concatenateMensajeDeError("Llenar los campos requeridos:");
		}
	}
);

function callSaveOrUpdateExtra(idExtra, titulo, idTipo, noUnidades, idUnidad, imagen, costo){
	$.ajax({
	    type: "POST",
	    url: "SaveOrUpdateExtraServlet",
	    data: {
	    	idExtra: idExtra,
			titulo: titulo,
			idTipo: idTipo,
			noUnidades: noUnidades,
			idUnidad: idUnidad,
			imagen: imagen,
			costo: costo
	    },
	    success: function(data){
	    	alert("Guardado con éxito");
	    	location.reload();	    	
	    },
	    done: function() {
	    	console.log("Done");
	    }                
	});
}


function validarDatos(idExtra, titulo, idTipo, noUnidades, idUnidad, imagen, costo){
	var canContinue = true;
	
	console.log("Validando Datos...");
	console.log("idExtra " + idExtra );
	console.log("titulo " + titulo );
	console.log("idTipo " + idTipo);
	console.log("noUnidades " + noUnidades);
	console.log("idUnidad " + idUnidad);
	console.log("imagen " + imagen);
	console.log("costo " + costo);
	
	// limpia mensaje de error:
		
	addMensajeDeError('');
	
	if ( titulo == '' ){
		concatenateMensajeDeError("- Titulo");
		canContinue = false;
	} 
	if ( idTipo == 0 ) {
		concatenateMensajeDeError("- Tipos");
		canContinue = false;
	} 
	if ( noUnidades == '' ) {
		concatenateMensajeDeError("- No. Unidades");
		canContinue = false;
	} 
	if ( idUnidad == 0 ) {
		concatenateMensajeDeError("- Unidades");
		canContinue = false;
	} 
	if ( imagen == '' ) {
		concatenateMensajeDeError("- Imagen");
		canContinue = false;
	} 
	if ( costo == '' ) {
		concatenateMensajeDeError("- Costo");
		canContinue = false;
	}
	
	return canContinue;
}
function concatenateMensajeDeError(val){
	var oldValue = $('#msjTexto').text(); 
	var newValue = val;
	addMensajeDeError( newValue + ' <br> ' +  oldValue);
}

function addMensajeDeError(val){
	$('#msjTexto').html(val);
}

function editar(idExtra){
	var titulo = $('#titulo_' + idExtra).text();
	var costo = $('#costo_' + idExtra).text();
	var unidad = $('#unidad_' + idExtra).text();	
	var noUnidades = $('#no_unidades_' + idExtra).text();
	var image = $('.img_' + idExtra +  ' img').attr('src');
	var image = image.replace('decorado/','');
	var idTipo = $('#idTipo_' + idExtra).text();
	var idUnidad = $('#idUnidad_' + idExtra).text();
	
	console.log("titulo: " + titulo);
	console.log("costo: " + costo);
	console.log("unidad: " + unidad );
	console.log("noUnidades: " + noUnidades);
	console.log("image: " + image);
	console.log("idTipo: " + idTipo);
	console.log("idUnidad: " + idUnidad);
	
	$('#id_extra').val( idExtra );
	$('#titulo').val( titulo );
	$('#costo').val( costo );
	$('#unidad').val( unidad );
	$('#no_unidades').val( noUnidades );
	$('#imagen').val( image );
	$('#idTipo').val( idTipo );
	$('#idUnidad').val( idUnidad );	
	
	$('#titulo').focus();
	
}

function eliminar( idExtra ){
	console.log("idExtra: " + idExtra);
	$.ajax({
	    type: "POST",
	    url: "EliminarExtraServlet",
	    data: {
	    	idExtra: idExtra
	    },
	    success: function(data){
	    	alert("Eliminado con éxito");
	    	 location.reload();
	    },
	    done: function() {
	    	console.log("Done");
	    }                
	});	
}
