/**
 * Los precios de Sharons estÃ¡n en los siguientes arreglos, se ingresa por index -1
 * ya que son basados en cero.
 */
// Piso 1, piso 2, piso 3 // Actualizar por catálogo 

function getPrecioPiso( indexPrecio ){
	return indexPrecio > 0 ? parseInt(indexPrecio) : 0;
}

function getPrecioSabor( indexPrecio, descCobertura ) {
	var precioSabor = 0;
	if (indexPrecio > 0 && !descCobertura.includes("Fondant")){
		var numPersonas = $("#num_personas").val();
		precioSabor = indexPrecio * numPersonas;		
	} 
	return precioSabor;
}

function getPrecioRelleno( indexPrecio, descCobertura ) {
	var precioRelleno = 0;
	if (indexPrecio > 0 && !descCobertura.includes("Fondant")){
		var numPersonas = $("#num_personas").val();
		precioRelleno = indexPrecio * numPersonas;
	} 	
	return precioRelleno;
}

function getPrecioCobertura( indexPrecio ){
	var precioCobertura = 0;
	// no se esta usando indexSabor
	numPersonas = $("#num_personas").val();
	if (indexPrecio > 0  ){	
		precioCobertura = numPersonas * indexPrecio;		
	}
	return precioCobertura;
}

function getPrecioTextura( indexPrecio ){
	var numPersonas = $("#num_personas").val();
	return indexPrecio > 0 ? indexPrecio * numPersonas : 0;	
}

function getPrecioTotal(){
	var precioPisos = $("#lista_pisos").val(); 
	var precioSabor = $("#lista_sabor").val();
	var precioRelleno = $("#lista_relleno").val();
	// console.log('IndexRelleno' + indexRelleno);
	var precioCobertura = $("#lista_cobertura").val();
	var descCobertura = $("#lista_cobertura :selected").text();	
	var precioTextura = $("#lista_textura").val();
	
	
	 //console.log("---");
	 //console.log("Precio Piso = " + getPrecioPiso( precioPisos ) );
	 //console.log("Precio Sabor = " + getPrecioSabor( precioSabor ) );
	 //console.log("Precio Relleno = " + getPrecioRelleno( precioRelleno, descCobertura ));
	 //console.log("Precio Cobertura = " + getPrecioCobertura( precioCobertura ));
	 //console.log("Precio Textura = " + getPrecioTextura( precioTextura ) ); // La textura no se toma en cuenta para el calculo del precio
	
	return getPrecioPiso( precioPisos ) + getPrecioSabor( precioSabor, descCobertura ) 
	+ getPrecioRelleno( precioRelleno, descCobertura ) + getPrecioCobertura( precioCobertura );
}

function numberWithCommas( number ) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function removeCommas( numberWithCommas ){
	return numberWithCommas.replace(/,/g, "");
}