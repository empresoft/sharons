// return a yyyy-mm-dd fecha de entrega más los días especificados 
function getFechaEntrega(tiempoEntrega){
	// Agregar 15 días a la fecha actual para pasteles de XV años
	var today = addDays(new Date(), tiempoEntrega);
	
	var dd = ("0" + today.getDate()).slice(-2);
	var mm = ("0" + (today.getMonth() + 1)).slice(-2);
	var yyyy = today.getFullYear();
	today = yyyy + '-' + mm  + '-'+ dd;
	//today = mm + '-' + mm  + '-'+ dd;
	return today;
}

function getFechaFormatoMX( fecha ){
	//split string and store it into array
	array = fecha.split('-');
	//from array concatenate into new date string format: "DD.MM.YYYY"
	return  array[2] + "/" + array[1] + "/" + array[0];	
}

function addDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}