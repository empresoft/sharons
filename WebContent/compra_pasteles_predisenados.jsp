<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="mx.sharons.dao.PastelPredisenadoDao" %>
<%@ page import="mx.sharons.beans.PastelPredisenado" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Formulario Bodas y XV</title>
<link href="css/boilerplate.css" rel="stylesheet" type="text/css">
<link href="css/reticula.css" rel="stylesheet" type="text/css">
<link href="css/estilos.css" rel="stylesheet" type="text/css">

<link rel="shortcut icon" href="imagenes/icono.ico">

<!-- Fecha -->
<script src="js/fecha.js"></script>

<script src="js/respond.min.js"></script>

<!-- number with commas -->
<script src="js/precios-util.js"></script>
<!--COLOR-->
<script src="js/jquery-1.9.0.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/color.css" />
<script src="js/jquery.dd.js"></script>
   
       <!-- Menu -->
<link rel="stylesheet" href="css/responsive-nav.css">
<link rel="stylesheet" href="css/styles.css">
<script src="js/responsive-nav.js"></script>
<script> $("body select").msDropDown();</script>

<style>
#lista_color {
	width: 5%;
}
#previsualizacion_pastel { text-align:center; max-height: 672px;}
form {
	clear: both;
	float: left;
	margin-left: 0%;
	width: 100%;
	display: block;
}

@media only screen and (min-width: 481px) {
form {
	margin-left: 10.204%;
	width: 79.5918%;
}
.previsualizacion_pastel { text-align:center;}
}
.error_requerido {
	color: red;
	font-size: 16px;
	padding-top: 5px;
	padding-bottom: 7px;
	font-family: 'Montserrat', sans-serif;
	color: #a94442;
}
</style>
   
</head>
<body>
<!--facebook-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!--end facebook-->

<div class="gridContainer clearfix">

<a href="login.html"><div id="sesion">
<span class="icon-head"></span>
</div></a> 

<a href="resumen-pedido.html"><div id="bag">
 <span class="icon-bag"></span></div></a>
 
  <div id="header">
    <div id="logo"><a href="index.html"><img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_01.png" class="big">
    <img src="imagenes/sharons-pasteleria-panaderia-cafeteria-acapulco_02.png" class="small"></a></div>
    
    <div id="menu">
  <nav class="nav-collapse">
  <ul>
       <li ><a href="index.html">INICIO</a></li>
       <li><a href="pasteleria.html">PASTELERÍA</a></li>
       <li ><a href="cafeteria.html">CAFETERÍA</a></li>
       <li><a href="contacto.html">CONTACTO</a></li>
      </ul>
  </nav></div>
  </div><!--END HEADER-->
  <div id="titulo-centro"><span class="secciones">Personaliza tu pastel</span><br>
    <span class="elige">prediseñado</span><br>
<span class="map">*No se hacen entregas a domicilio, las entregas son en nuestras sucursales</span>
  <hr></div>

<%
if ( request.getParameter("idPastel") != null ) { 
	PastelPredisenadoDao pastelPredisenadoDao = new PastelPredisenadoDao();
	int idPastel = Integer.parseInt(request.getParameter("idPastel"));
	PastelPredisenado pastel = pastelPredisenadoDao.buscarPorId(idPastel);
	
	if ( pastel.getNombre() != null ) {
%>
 <form action="" method="post">
  <div id="form_arma_tu_pastel">
  <br><span class="titulos"><%= pastel.getNombre() %></span><br>
 <span class="predis"><%= pastel.getDescripcion() %></span><br>
 <span class="predis">Sabor <%= pastel.getSabor() %></span><br><br>
 <%
 	if ( pastel.getObservaciones() != null && !pastel.getObservaciones().equals("") && !pastel.getObservaciones().equals(" ")){
 		%>
 		<span class="opciones">*</span>
 		<span class="total"><%= pastel.getObservaciones() %></span><br>
 		<%
 	}
 %>
 <br>
 
 <!-- Num. de personas -->
<span class="opciones">Selecciona el número de personas</span><br>
<span class="mensaje">*De 20 a 999 personas</span><br>
<input id="num_personas" type="text" required id="num_personas" max="999" min="20" value="0"  maxlength="3"> 
<br><span class="mensaje">*Minimo 80 personas para pasteles de más de 1 piso</span><br>
<div id="error_num_personas" class="error_requerido"></div>
 
<!-- Regla #6.- Mensaje -->
<span class="opciones">Mensaje</span> <span class="spe">*Impreso</span><br>
  <textarea name="mensaje" rows="2" class="mensaje" id="mensaje_pastel" placeholder="Escribe tu mensaje de hasta 100 caracteres" maxlength="100"></textarea>
  <br>
  
 <!-- Entrega -->
 <span class="opciones">Entrega</span><br>
  <select id="lista_sucursal"name="lista_sucursal" required>
	<option value="">Elige la sucursal de tu preferencia</option>
	<option value="1">ACA - Martín Alonso Pinzón, Col. Magallanes No. 28, Local 6, CP 39670</option>
	<option value="2">ACA - Boulevard Vicente Guerrero Saldaña, Manzana 1, Lote 3, Local B2, Cd. Renacimiento, CP 39715</option>
	<option value="3">ACA - Av. Constituyentes No. 90, Interior B, Col. Vista Alegre, CP 39560</option>
	<option value="4">ACA - Cd. Luis Donaldo Colosio, Calle Obsidiana #1, CP 39907</option>
	<option value="5">ACA - Av. Costera Miguel Alemán 239, Col. Centro, CP 39300</option>
	<option value="6">CDMX - Av. Sinatel No. 72, Ampliación Sinatel, CP 09470, Iztapalapa</option>
  </select>
 <div id="error_entrega_sucursal" class="error_requerido"></div>
  
  <input id="fecha_entrega" type="date" class="decorado" value="Fecha">
  <div id="error_entrega_fecha" class="error_requerido"></div>
  
    <select id="lista_horario" name="lista_horario" required>
	  <option value="">Hora</option>
	  <option value="7:30 am">7:30 am</option>
	  <option value="8:30 am">8:30 am</option>
	  <option value="9:30 am">9:30 am</option>
	  <option value="10:30 am">10:30 am</option>
	  <option value="11:30 am">11:30 am</option>
	  <option value="12:30 pm">12:30 pm</option>
	  <option value="1:30 pm">1:30 pm</option>
	  <option value="2:30 pm">2:30 pm</option>
	  <option value="3:30 pm">3:30 pm</option>
	  <option value="4:30 pm">4:30 pm</option>
	  <option value="5:30 pm">5:30 pm</option>
	  <option value="6:30 pm">6:30 pm</option>
	  <option value="7:30 pm">7:30 pm</option>
  </select>
  <div id="error_entrega_horario" class="error_requerido"></div>
  
  <!-- Observaciones -->
  
  <br><span class="opciones">Observaciones</span><br>
  <textarea name="mensaje" rows="2" class="mensaje" id="observaciones_pastel" placeholder="Escribe tus comentarios" maxlength="100"></textarea>
  <br>

  </div>
  
  <div id="previsualizacion">
  
  <div id="precio_total">
	<span class="total">Precio total </span>
	<span class="precio_total">$</span>
	<span id="precioTotal" class="precio_total">
		0
	</span>
	
	<!-- campos ocultos -->
	<span id="idPastelPredisenado" hidden > 
		<%= idPastel %>
	</span>
		
	<span id="precioBase" hidden > 
		<%= pastel.getAdicional() %>
	</span>
	
	<span id="precioPorcion" hidden > 
		<%= pastel.getPrecioPorcion() %>
	</span>
	
	
  	
  	<span id="imgPastel" hidden><%= pastel.getImagen() %></span>
	
	<input  type="button" class="button" id="boton_continuar" value="Continuar">
  </div>
  
  <div id="error_continuar" align="right" class="error_requerido"></div>
      
  <div id="previsualizacion_pastel">
  <img src="imagenes/predisenados/<%= pastel.getImagen() %>">
  </div><!--Termina previsualizacion-->
  </div>
    </form>
  <% } else { %>
  	<p> No hay pasteles con ese ID </p>
  <% } %>
  
  <% } else { %>
  	<p> No se encontro un ID de pastel para buscar </p>
  <% } %>
  
<div id="footer">
<hr color="#e28f26">
<span class="map"><a href="index.html">inicio</a></span> | <span class="map"><a href="pasteleria.html">pastelería</a></span> | <span class="map"><a href="cafeteria.html">cafetería</a></span> | <span class="map"><a href="contacto.html"> contacto</a></span><br><span class="pie">
Todos los derechos reservados Sharon’s ® Sitio web desarrollado por <a href="http://omedg.com">Ome Diseño</a> y Empresoft <br><span style="font-weight:300"><a href="aviso-privacidad.html">Aviso de privacidad</a></span></span></div>

</div>

  
    <!-- Menu -->
<script>
      var navigation = responsiveNav(".nav-collapse", {
        animate: true,                    // Boolean: Use CSS3 transitions, true or false
        transition: 284,                  // Integer: Speed of the transition, in milliseconds
        label: "Menu",                    // String: Label for the navigation toggle
        insert: "after",                  // String: Insert the toggle before or after the navigation
        customToggle: "",                 // Selector: Specify the ID of a custom toggle
        closeOnNavClick: false,           // Boolean: Close the navigation when one of the links are clicked
        openPos: "relative",              // String: Position of the opened nav, relative or static
        navClass: "nav-collapse",         // String: Default CSS class. If changed, you need to edit the CSS too!
        navActiveClass: "js-nav-active",  // String: Class that is added to <html> element when nav is active
        jsClass: "js",                    // String: 'JS enabled' class which is added to <html> element
        init: function(){},               // Function: Init callback
        open: function(){},               // Function: Open callback
        close: function(){}               // Function: Close callback
      });
    </script>
    
<script language="javascript">
	
$(document).ready(function(e) {
	try {
		$("body select").msDropDown();
	} catch(e) {
	alert(e.message);
	}
});

$(document).ready(function(e) {
	try {
		$("body select").msDropDown();
		$('#fecha_entrega').val( getFecha(15) );
	} catch(e) {
	 alert(e.message);
	}
});

// Evento para capturar cuando el usuario escribe en número de personas
$( "#num_personas" ).keyup(function() {
	numPersonas = Number($("#num_personas").val());
	if ( numPersonas < 20 ){
		$('#error_num_personas').text("Dato invalido, debe ser min. 20. ");
		$('#precioTotal').text('0');
		//$('#boton_pagar').prop( "disabled", true );
	} else if ( numPersonas > 999 ){
		$('#error_num_personas').text("Dato invalido, debe ser máx. 999. ");
		$('#precioTotal').text('0');
		//$('#boton_pagar').prop( "disabled", true );
	} else {
		if ( isNaN(numPersonas) ){
			$('#error_num_personas').text("Dato invalido, solo números. ");
			$('#precioTotal').text('0');
			//$('#boton_pagar').prop( "disabled", true );			
		} else {
			$('#error_num_personas').text("");
			//$('#boton_pagar').prop( "disabled", false );
			actualizaPrecioTotal(numPersonas);			
		}
	}
});

function actualizaPrecioTotal(numPersonas){
	var precioTotal = numberWithCommas( getPrecioTotal(numPersonas) );
	$('#precioTotal').text( precioTotal );
}

function getPrecioTotal(numPersonas){
	var precioBase = Number($('#precioBase').text().trim());
	// console.log("Precio base: " + precioBase );
	var precioPorcion = Number($('#precioPorcion').text().trim());
	// console.log("Precio porción: " + precioPorcion );
	var precioTotal = (numPersonas * precioPorcion) + precioBase;
	// console.log("Costo total: " + precioTotal );
	return precioTotal;
}

$('#boton_continuar').click(
	function(){
		if (validaNumPersonas() & validaSucrusal() & validaFecha() & validaHorario()){
			guardarCompra();
		}		
	}
);

function guardarCompra(){
	var idPastelPredisenado = $('#idPastelPredisenado').text().trim();
	var numPersonas = $("#num_personas").val();
	var mensaje = $("#mensaje_pastel").val();
	var idSucursalEntrega = $("#lista_sucursal").val();
	var descSucursalEntrega = $('#lista_sucursal option:selected').text();
	var descFechaEntrega = getFechaFormatoMX($("#fecha_entrega").val());
	var descListaHorario = $("#lista_horario").val();
	var observaciones = $("#observaciones_pastel").val();
	var precioTotal = removeCommas($('#precioTotal').text());
	var imgPastel = $('#imgPastel').text().trim();
	
	console.log("numPersonas: " + numPersonas);
	console.log("mensaje: " + mensaje);
	console.log("idSucursalEntrega: " + idSucursalEntrega + " descSucursalEntrega:" + descSucursalEntrega);
	console.log("descFechaEntrega: " + descFechaEntrega);
	console.log("descListaHorario: " + descListaHorario);
	console.log("observaciones: " + observaciones);
	console.log("precioTotal: " + precioTotal);
	console.log("imgPastel: " + imgPastel);
	
	console.log("Llamada al Back-end")	
    $.ajax({
        type: "POST",
        url: "PastelPredisenadoServlet",
        data: {
        	idPastelPredisenado: idPastelPredisenado, 
        	numPersonas: numPersonas,
        	mensaje: mensaje,
        	idSucursalEntrega: idSucursalEntrega,
        	descSucursalEntrega: descSucursalEntrega,
        	descFechaEntrega: descFechaEntrega,
        	descListaHorario: descListaHorario,
        	observaciones: observaciones,
        	precioTotal: precioTotal,
        	imgPastel: imgPastel
        },
        success: function(data){
        	window.location.href = "resumen_pedido_prediseno.jsp";
        },
        done: function() {
        	// console.log("Done");
        }                
    });
}
function validaNumPersonas(){
	// validar número de personas
	var numPersonas = Number($("#num_personas").val());
	if ( numPersonas == '' ||  numPersonas == 0 ){
		$('#error_num_personas').text("Selecciona el número de personas.");
		//$('#boton_pagar').prop( "disabled", true );		
		return false;
	} else {
		return true;
	}
}

function validaSucrusal(){
	// validar entrega
	if ( $('#lista_sucursal')[0].selectedIndex == '' ) {
		$('#error_entrega_sucursal').text("Selecciona una sucursal de entrega");
		return false;
	} else {
		$('#error_entrega_sucursal').text('');
		return true;
	} 
}

function validaFecha(){
	// validar fecha
	var strFechaEntrega = $("#fecha_entrega").val();
	if ( fechaEntrega == ''  ){
		$("#error_fecha").html("Selecciona una fecha de entrega");
		return false;
	} else {
		var diasEntrega = 15;
		var strFechaDiasEntrega = getFecha(diasEntrega);
		var fechaDiasEntrega = new Date( strFechaDiasEntrega );
		var fechaEntrega = new Date( strFechaEntrega );
		console.log("Fecha Días Entrega: " + fechaDiasEntrega );
		console.log("Fecha Entrega: " + fechaEntrega );
		
		if ( fechaEntrega.getTime() < fechaDiasEntrega.getTime() ){
			$("#error_entrega_fecha").html('La fecha de entrega no puede ser menor a ' + getFechaFormatoMX(strFechaDiasEntrega));
			return false;
		} else {
			$("#error_entrega_fecha").html('');
			return true;
		}
		
	}
}

function validaHorario(){
	if ( $('#lista_horario')[0].selectedIndex == '') {
		$('#error_entrega_horario').text("Selecciona un Horario de entrega");
		return false;
	} else {
		$('#error_entrega_horario').text('');
		return true;
	}	
}
</script>
</body>
</html>